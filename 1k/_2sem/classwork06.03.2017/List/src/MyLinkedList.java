/**
 * Created by Роберт on 13.02.2017.
 */
public class MyLinkedList<T> {
    Node head;
    Node tail;
    MyLinkedList(Node head){
        this.head = head;
        Node f;
        for (f = head; f.getNext() == null ; f = f.getNext()) {
        }
        this.tail = f;
    }
    MyLinkedList(){
        head = null;
        tail = null;
    }
    public void input(T y){
        Node x = new Node(y,null);
        tail.setNext(x);
        tail = x;
        if (head == null){
            head = x;
        }
    }
    public String toString(){
        String s = "";
        for (Node f = head; f.getNext() != null ; f = f.getNext()) {
            s +=f.getValue() +"/n";
        }
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public void add(MyLinkedList x){
        tail.setNext(x.getHead());
        tail = x.getTail();
    }
    public void addHead(int x){
        Node p = new Node(x,null);
        p.setNext(head);
        this.head = p;
    }
    public void addTail(T x){
        Node p = new Node(x,null);
        tail.setNext(p);
        this.tail = p;

    }
    public void deleteHead(){
        Node p = head.getNext();
        head.setNext(null);
        head = p;
    }
    public void deleteTail(){
        Node p;
        for ( p = head; p.getNext().getNext() == null ; p = p.getNext()) {
        }
        p.setNext(null);
        tail = p;
    }
}
