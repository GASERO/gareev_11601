import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Роберт on 24.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> x = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            x.add(i);
        }
        x.stream()
                .filter(z -> z > 1)
                .sorted((o1, o2) -> {
                    return -o1 + o2;
                })
                .map(z -> z *z - 2*z + 1)
                .forEach(System.out::println);

    }
}
