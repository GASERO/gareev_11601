package sample;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.nio.channels.Pipe;

/**
 * Created by Роберт on 16.05.2017.
 */
public class MyStage extends Stage {
    private BufferedReader reader;
    private PrintWriter writer;


    public MyStage(PipedInputStream pis, PipedOutputStream pos) {
        super();
        this.reader = new BufferedReader(new InputStreamReader(pis));
        this.writer = new PrintWriter(pos,true);
        initialization();
    }

    public void initialization(){

        Group root = new Group();
        this.requestFocus();

        TextArea textArea = new TextArea();
        textArea.setPrefSize(Main.WIDTH,Main.HEIGHT/2);
        textArea.setEditable(false);

        ChatListener c1 = new ChatListener(textArea,reader);
        c1.setDaemon(true);
        c1.start();

        TextField textField = new TextField();
        textField.setLayoutY(Main.HEIGHT / 2 + 10);
        textField.setPrefSize(Main.WIDTH, Main.HEIGHT / 10);


        Button button = new Button();
        button.setLayoutY(Main.HEIGHT /2 + Main.HEIGHT / 10 + 10);
        button.setLayoutX(Main.WIDTH / 1.5);
        button.setText("SEND MESSAGE");
        button.setOnAction(x -> {
            if (!textField.getText().equals("")){
            // display
            textArea.setText(textArea.getText() +"YOU: "+ textField.getText() + "\n");
            //send
            writer.println("GUEST: " +textField.getText());
            textField.setText("");}
            else {
                return;
            }
        });

        this.setTitle("Chat");

        this.setScene(new Scene(root,Main.WIDTH, Main.HEIGHT));
        root.getChildren().addAll(textArea,textField,button);
        textField.requestFocus();





    }
}
