package sample;

import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;

/**
 * Created by Роберт on 16.05.2017.
 */
public class ChatListener extends Thread {
    private TextArea textArea;
    private BufferedReader reader;
    @Override
    public void run() {
        while(true){
            try {
                String line = reader.readLine();
                textArea.setText(textArea.getText() + line +"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    ChatListener(TextArea t, BufferedReader reader){
        textArea = t;
        this.reader= reader;
    }
}
