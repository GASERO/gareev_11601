package sample;

import com.sun.webkit.graphics.WCImageFrame;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Main extends Application {
    public static final int HEIGHT = 500;
    public static final int WIDTH = 500;



    @Override
    public void start(Stage primaryStage) throws Exception{
        PipedOutputStream pos1 = new PipedOutputStream();
        PipedInputStream pis1 = new PipedInputStream(pos1);
        PipedOutputStream pos2 = new PipedOutputStream();
        PipedInputStream pis2 = new PipedInputStream(pos2);

        MyStage stage1 = new MyStage(pis1,pos2);
        MyStage stage2 = new MyStage(pis2,pos1);
        stage1.show();
        stage2.show();
        stage1.setX(stage1.getX() - WIDTH);
        stage2.setX(stage2.getX() + WIDTH);

    }


    public static void main(String[] args) {
        launch(args);
    }
}
