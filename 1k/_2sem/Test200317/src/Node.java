/**
 * Created by Роберт on 20.03.2017.
 */
public class Node {
    private String value;
    private Node next;
    Node(String a){
        value = a;
    }

    Node(String value, Node next) {
        this.value = value;
        this.next = next;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public String getValue() {

        return value;
    }

    public Node getNext() {
        return next;
    }
}
