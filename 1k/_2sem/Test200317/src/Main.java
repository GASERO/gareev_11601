import java.util.Scanner;

/**
 * Created by Роберт on 20.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Node head = null,p;
        Scanner sc = new Scanner(System.in);
        System.out.print("n = ");
        int n = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < n; i++) {
            p = new Node(sc.nextLine());
            p.setNext(head);
            head = p;
        }
        n = -1;
        int k;
        Node a = null, b = null;
        for (p = head; p.getNext() != null ; p = p.getNext()) {
            k = Math.abs(p.getValue().length() - p.getNext().getValue().length());
            if (k > n){
                n = k;
                a = p;
                b = p.getNext();
            }
        }
        System.out.println("Max raznica ravna = " + n + "\nMezhdu: ");
        System.out.println(a.getValue());
        System.out.println(b.getValue());
        System.out.println();
        char[] c = {'a','b','c'};
        Node prev = null;
        for (p = head; p.getNext() != null ; p = p.getNext()) {
            if (test(p.getValue(),c)){
                if (prev == null){
                    head = head.getNext();
                }else{
                    prev.setNext(p.getNext());
                }
            }else{
                prev = p;
            }
        }
        for (p = head; p.getNext() != null ; p = p.getNext()) {
            System.out.println(p.getValue());
        }




    }
    public static boolean test(String a, char[] c){
        int k = 0;
        for (char x: c
             ) {
            for (int i = 0; i < a.length(); i++) {
                if (x == a.charAt(i)){
                    k++;
                }
                if (k > 4) return true;
            }
        }
        return false;
    }
}
