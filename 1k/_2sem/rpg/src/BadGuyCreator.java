/**
 * Created by Роберт on 21.02.2017.
 */
public abstract class BadGuyCreator {
    public abstract BadGuy createEnemy();
}
