/**
 * Created by Роберт on 20.02.2017.
 */
public class Reaper extends BadGuy {

    public Reaper() {
        this.name = "Reaper";
    }

    public BadGuy createEnemy() {
        return new Reaper();
    }
}