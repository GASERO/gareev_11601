/**
 * Created by Роберт on 20.02.2017.
 */
public abstract class BadGuy extends GameCharacter{
    protected String name;
    public abstract BadGuy createEnemy();
}