/**
 * Created by Роберт on 21.02.2017.
 */
public class Game {
    public void startGame(){
        Player player = new Player();
        OrcCreator oc = new OrcCreator();
        BadGuy orc = oc.createEnemy();
        ReaperCreator rp = new ReaperCreator();
        BadGuy reaper =rp.createEnemy();
        player.attack(orc);
        player.attack(reaper);
    }
}
