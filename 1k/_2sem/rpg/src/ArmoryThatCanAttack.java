/**
 * Created by Роберт on 21.02.2017.
 */
public interface ArmoryThatCanAttack {
   void attack (GameCharacter enemy);
}
