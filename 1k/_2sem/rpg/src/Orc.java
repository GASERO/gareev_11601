/**
 * Created by Роберт on 20.02.2017.
 */
public class Orc extends BadGuy {

    public Orc() {
        this.name = "Orc";
    }

    public BadGuy createEnemy() {
        return new Orc();
    }
}