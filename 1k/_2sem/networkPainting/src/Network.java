import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Роберт on 07.05.2017.
 */
public class Network {
    private ArrayList<Node> nodes;

    public Network() {
        this.nodes = new ArrayList<>();
    }

    public void addConnection(String a){
        String[] s = a.split(" ");
        Node a1 = nodes.stream()
                .filter(x -> x.getNumber() == Integer.parseInt(s[0]))
                .findFirst()
                .orElse(null);
        Node a2 = nodes.stream()
                .filter(x -> x.getNumber() == Integer.parseInt(s[1]))
                .findFirst()
                .orElse(null);
        if (a1 == null){
            a1 = new Node(Integer.parseInt(s[0]));
            nodes.add(a1);
        }
        if (a2 == null){
            a2 = new Node(Integer.parseInt(s[1]));
            nodes.add(a2);
        }
        a1.addNeighbor(a2);
        a2.addNeighbor(a1);
    }
    public String toString(){
        String s = "";
        for (Node x :
                nodes) {
            s += x.getColor() + "\n";
        }
        return s;
    }
    public void paint(){
        paint(1);
    }
    private void paint(int n){
        nodes.get(0).setColor(n);
        nodes.get(0).getNeighbors().stream()
                .forEach(x -> x.setColor(n+1));
        nodes.stream()
                .skip(1)
                .forEach(x -> {
                    for (Node y :
                            x.getNeighbors()) {
                        if (y.getColor() == x.getColor()) {
                            y.setColor(y.getColor() + 1);
                        }
                    }
                });
        if (!check()){
            System.out.println("watafak");
            paint(n+1);
        }
    }
    public boolean check(){
        for (Node x :
                nodes) {
            for (Node y :
                    x.getNeighbors()) {
                if (x.getColor() == y.getColor()) {
                    return false;
                }
                }
        }
        return true;
    }

}
