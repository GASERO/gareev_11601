import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Роберт on 07.05.2017.
 */
public class Node {
    private int color;
    private int number;
    private ArrayList<Node> neighbors;

    public Node(int number) {
        this.number = number;
        neighbors = new ArrayList<>();
    }

    public void addNeighbor(Node n) {
        neighbors.add(n);
    }

    public ArrayList<Node> getNeighbors() {
        return neighbors;
    }

    public int getNumber() {

        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getColor() {

        return color;
    }

    public void setColor(int n) {
        if (n >4){
            n = n % 4;
        }
        this.color = n;
    }
}
