import java.util.Scanner;

/**
 * Created by Роберт on 07.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Network nw = new Network();
        String s = sc.nextLine();
        while (!s.equals("0")){
            nw.addConnection(s);
            s = sc.nextLine();
        }
        nw.paint();
        System.out.println(nw);
    }
}
