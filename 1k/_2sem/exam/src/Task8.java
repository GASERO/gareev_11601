import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Роберт on 17.06.2017.
 */
public class Task8 {
    public static void main(String[] args) throws IOException {
        File file = new File("text.txt");
        BufferedReader bufferedReader = new BufferedReader( new FileReader(file));
        HashMap<Character, Integer> map = new HashMap<>();
        int symbol = bufferedReader.read();
        for (char c = 'a'; c <= 'z'; c++) {
            map.put(c,0);
        }
        while (symbol != -1) {
            char c = (char) symbol;
            String s = c +"";
            s = s.toLowerCase();
            c = s.charAt(0);
            if (c <= 'z' && c >= 'a'){
                    map.put(c,map.get(c)+1);
            }
            symbol = bufferedReader.read();
        }
        System.out.println(map.toString());

    }
}
