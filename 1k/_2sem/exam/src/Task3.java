import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by Роберт on 17.06.2017.
 */
public class Task3 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("in.txt");
        Scanner sc = new Scanner(file);
        ArrayList<Integer> list = new ArrayList<>();
        while(sc.hasNext()){
            list.add(sc.nextInt());
        }
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return numberOfFives(o1) - numberOfFives(o2);
            }

            public int numberOfFives(int x){
                int k = 0;
                int digit;
                while (x > 0){
                    digit = x % 10;
                    if (digit ==5) k++;
                    x = x / 10;
                }
                return k;
            }
        });
        PrintWriter pw = new PrintWriter("out.txt");
        pw.write(list.toString());
        pw.close();
    }
}
