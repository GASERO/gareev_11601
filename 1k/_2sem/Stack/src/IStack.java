import java.util.List;

/**
 * Created by Роберт on 16.02.2017.
 */
public interface IStack {
    public void push(char x);
    public Character pop();
    public boolean isEmpty();
}
