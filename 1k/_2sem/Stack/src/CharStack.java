/**
 * Created by Роберт on 16.02.2017.
 */
public class CharStack implements IStack {
    private class CharElem{
        private Character value;
        private CharElem next;

        public CharElem(char value, CharElem next) {
            this.value = value;
            this.next = next;
        }

        public void setValue(char value) {
            this.value = value;
        }

        public void setNext(CharElem next) {
            this.next = next;
        }

        public char getValue() {
            return value;
        }

        public CharElem getNext() {
            return next;
        }
    }


    private CharElem head;

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public Character pop() {
        if (head != null){
        char a = head.getValue();
        head = head.getNext();
        return a;}
        else return null;


    }

    @Override
    public void push(char x) {
        head = new CharElem(x,head);
    }
}
