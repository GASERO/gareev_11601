/**
 * Created by Роберт on 13.04.2017.
 */
public class CharAndStates {
    private Character c;
    private States states;

    @Override
    public String toString() {
        return "CharAndStates{" +
                "c=" + c +
                ", states=" + states +
                '}';
    }

    public Character getC() {
        return c;
    }

    public void setC(Character c) {
        this.c = c;
    }

    public States getStates() {
        return states;
    }

    public void setStates(States states) {
        this.states = states;
    }

    public CharAndStates(Character c, States states) {

        this.c = c;
        this.states = states;
    }
    @Override
    public int compareTo(CharAndStates o) {
        if( c != o.getC()){
            return c.compareTo(o.getC());
        }
        else{
            return states.compareTo(o.getStates())
        }
    }
}
