import Grammar.RegGrammar;
import Grammar.RightSideElem;

import java.util.*;

/**
 * Created by NVYas on 11.04.2017.
 */
public class NFA {
    private Map<CharAndState,List<State>> transitions = new TreeMap<>();
    private State startState;
    private State finalState = new State("$@@$$#%&$*$".hashCode() + "");

    public Map<CharAndState, List<State>> getTransitions() {
        return transitions;
    }

    public void setTransitions(Map<CharAndState, List<State>> transitions) {
        this.transitions = transitions;
    }

    public State getStartState() {
        return startState;
    }

    public void setStartState(State startState) {
        this.startState = startState;
    }

    public State getFinalState() {
        return finalState;
    }

    public void setFinalState(State finalState) {
        this.finalState = finalState;
    }

    public NFA(RegGrammar grammar){
        startState =new State(grammar.getStartNonterminal());
        for (String leftSide : grammar.getProductions().keySet()){
            State from = new State(leftSide);
            for(RightSideElem rse : grammar.getProductions().get(leftSide)){
                //terminal always in right side
                Character c = rse.getTerminal();
                CharAndState cas = new CharAndState(c,from);
                State to;
                if (rse.getNonterminal().equals("")){
                    to = new State(rse.getNonterminal());
                }else{
                    to = new State(rse.getNonterminal());

                }
                if (transitions.containsKey(cas)){
                    List<State> lst = transitions.get(cas);

                    lst.add(to);

                }else{
                    List<State> lst  = new ArrayList<>();
                    lst.add(to);
                    transitions.put(cas,lst);
                }
            }
        }
    }
}