package Grammar;

/**
 * Created by NVYas on 11.04.2017.
 */
public class RightSideElem {
    private Character terminal;
    private String nonterminal;

    public Character getTerminal() {
        return terminal;
    }

    public void setTerminal(Character terminal) {
        this.terminal = terminal;
    }

    public String getNonterminal() {
        return nonterminal;
    }

    public void setNonterminal(String nonterminal) {
        this.nonterminal = nonterminal;
    }

    public RightSideElem(Character terminal) {
        this.terminal = terminal;
    }

    public RightSideElem(Character terminal, String nonterminal) {
        this.terminal = terminal;
        this.nonterminal = nonterminal;
    }

    @Override
    public String toString() {
        return "Grammar.RightSideElem{" +
                "terminal=" + terminal +
                ", nonterminal='" + nonterminal + '\'' +
                '}';
    }
}
