package Grammar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by NVYas on 11.04.2017.
 */
public class RegGrammar {
    private String defaultFilename = "grammar.txt";
    private String terminals;
    private List<String> nonterminals;
    private String startNonterminal;
    private Map<String, List<RightSideElem>> productions;

    public RegGrammar() {
        readGrammar(defaultFilename);
    }

    public void readGrammar(String filename) {
        try {
            Scanner scanner = new Scanner(new File(filename));
            terminals = scanner.nextLine();
            nonterminals = Arrays.asList(scanner.nextLine().split(" "));
            startNonterminal = scanner.nextLine();
            productions = new HashMap<>();
            while (scanner.hasNextLine()) {
                String[] args = scanner.nextLine().split(" -> ");
                String[] rightSides = args[1].split("\\|");
                List<RightSideElem> lst = Arrays.stream(rightSides)
                        .map((x) -> new RightSideElem(
                                        x.charAt(0),
                                        x.substring(1)
                        ))
                        .collect(Collectors.toList());
                productions.put(args[0],lst);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Grammar.RegGrammar{" +
                "terminals='" + terminals + '\'' +
                ", nonterminals=" + nonterminals +
                ", startNonterminal='" + startNonterminal + '\'' +
                ", productions=" + productions +
                '}';
    }

    public String getStartNonterminal() {
        return startNonterminal;
    }

    public Map<String, List<RightSideElem>> getProductions() {
        return productions;
    }
}
