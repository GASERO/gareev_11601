/**
 * Created by Роберт on 13.04.2017.
 */
public class CharAndState implements  Comparable<CharAndState>{
    private Character c;
    private State state;

    public CharAndState(Character c, State state) {
        this.c = c;
        this.state = state;
    }

    public Character getC() {
        return c;
    }

    public void setC(Character c) {
        this.c = c;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CharAndState){
            CharAndState cas = (CharAndState)obj;
            return (cas.getC() == c && cas.getState().equals(state));
        }
        else{
            return false;
        }
    }

    @Override
    public int compareTo(CharAndState o) {
        if( c != o.getC()){
            return c.compareTo(o.getC());
        }
        else{
            return state.getState().compareTo(o.getState().getState());
        }
    }
}
