import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Роберт on 13.04.2017.
 */
public class States extends ArrayList<State> implements Comparable<States>{
    @Override
    public int compareTo(States o) {
        if (Arrays.equals(this.toArray(), o.toArray())){
            return 0;
        }
        else{
            return this.size()-o.size();
        }
    }
}