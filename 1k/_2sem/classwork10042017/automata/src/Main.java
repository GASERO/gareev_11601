import Grammar.RegGrammar;

/**
 * Created by NVYas on 11.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        RegGrammar grammar = new RegGrammar();
        System.out.println(grammar);
        NFA automata = new NFA(grammar);
        System.out.println(automata);
    }
}
