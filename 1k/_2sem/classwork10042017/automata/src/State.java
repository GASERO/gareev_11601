/**
 * Created by Роберт on 13.04.2017.
 */
public class State {
    private  String state;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof State){
            return state.equals(((State) obj).getState());
        }
        else{
            return false;
        }
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public State(String state) {
        this.state = state;

    }

    @Override
    public String toString() {
        return super.toString();
    }
}
