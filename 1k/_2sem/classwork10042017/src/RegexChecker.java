import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Роберт on 10.04.2017.
 */
public class RegexChecker {
    public boolean checkByRegex(String regex, String input){
        Pattern p1 = Pattern.compile(regex);
        Matcher matcher = p1.matcher(input);
        return matcher.matches();
    }
}
