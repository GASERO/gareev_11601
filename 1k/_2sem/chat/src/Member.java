import java.io.*;
import java.nio.BufferOverflowException;
import java.time.LocalDate;

/**
 * Created by Роберт on 15.05.2017.
 */
public class Member extends Thread {

    private PipedOutputStream outputStream = new PipedOutputStream();
    private String filename = "buffer";

    @Override
    public void run(){
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filename));
            int x;
            if (!(new File(filename)).exists()) {
                System.out.println("Chat created. Waiting for member to chat...");
                FileOutputStream fos = new FileOutputStream(filename);
                fos.write(0);
                fos.close();



                x = bis.read();
                while (x == 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    bis.close();
                    bis = new BufferedInputStream(new FileInputStream(filename));
                    x = bis.read();
                }
                System.out.println("Found member. Starting the chat.");
            }
            else{
                System.out.println("Joining existing chat");
            }
        }
         catch(FileNotFoundException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        (new Member()).start();
    }
}
