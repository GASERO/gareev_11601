package entities;

/**
 * Created by NVYas on 18.04.2017.
 */
public class CarMaker {
    private int id;
    private String maker;
    private String fullName;
    private int countryId;
    private Country country;

    public CarMaker(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "CarMaker{" +
                "id =" + id +
                ", maker ='" + maker + '\'' +
                ", fullName ='" + fullName + '\'' +
                ", country =" + country.getCountryName() +
                '}';
    }
}
