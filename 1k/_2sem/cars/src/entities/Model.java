package entities;

/**
 * Created by NVYas on 19.04.2017.
 */
public class Model {
    private int modelId;
    private String model;
    private int carMakerId;
    private CarMaker carMaker;

    public Model(){}

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCarMakerId() {
        return carMakerId;
    }

    public void setCarMakerId(int carMakerId) {
        this.carMakerId = carMakerId;
    }

    public CarMaker getCarMaker() {
        return carMaker;
    }

    public void setCarMaker(CarMaker carMaker) {
        this.carMaker = carMaker;
    }

    @Override
    public String toString() {
        return "Model{" +
                "modelId=" + modelId +
                ", model='" + model + '\'' +
                ", carMakerId=" + carMakerId +
                ", carMaker=" + carMaker.getMaker() +
                '}';
    }
}
