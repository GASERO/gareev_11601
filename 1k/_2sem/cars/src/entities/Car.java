package entities;

/**
 * Created by NVYas on 20.04.2017.
 */
public class Car implements Comparable<Car> {
    private int id;

    @Override
    public int compareTo(Car o) {
        return this.getId() - o.getId();
    }

    private Name name;
    private int mpg;
    private int cylinders;
    private int edispl;
    private int horsepower;
    private int weight;
    private int accelerate;
    private int year;

    public Car(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public int getMpg() {
        return mpg;
    }

    public void setMpg(int mpg) {
        this.mpg = mpg;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public int getEdispl() {
        return edispl;
    }

    public void setEdispl(int edispl) {
        this.edispl = edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(int accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
//                  ", name=" + name.getMake() +
                ", mpg=" + mpg +
                ", cylinders=" + cylinders +
                ", edispl=" + edispl +
                ", horsepower=" + horsepower +
                ", weight=" + weight +
                ", accelerate=" + accelerate +
                ", year=" + year +
                '}';
    }
}
