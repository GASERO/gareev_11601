package repositories;

import db.DataBaseConnection;
import entities.Car;

import java.util.List;

/**
 * Created by NVYas on 20.04.2017.
 */
public class CarRepository {
    public List<Car> getAll(){
        return DataBaseConnection.getDBConnection().getAllCars();
    }
}
