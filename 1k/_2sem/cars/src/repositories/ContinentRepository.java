package repositories;

import db.DataBaseConnection;
import entities.Continent;

import java.util.List;

/**
 * Created by ma on 23.03.2017.
 */
public class ContinentRepository {

    public List<Continent> getAll() {
        return DataBaseConnection.getDBConnection().getAllContinents();
    }

    public Continent getById(int id) {
        return DataBaseConnection.getDBConnection().getContinentById(id);
    }

    public Continent getByName(String name) {
        for (Continent continent : DataBaseConnection.getDBConnection().getAllContinents()) {
            if (continent.getContinent().equals(name))
                return continent;
        }
        return null;
    }
}
