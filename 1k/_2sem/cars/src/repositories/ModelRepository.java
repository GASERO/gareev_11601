package repositories;

import db.DataBaseConnection;
import entities.Model;

import java.util.List;

/**
 * Created by NVYas on 19.04.2017.
 */
public class ModelRepository {
    public List<Model>  getAll(){
        return DataBaseConnection.getDBConnection().getAllModels();
    }
}
