package repositories;

import db.DataBaseConnection;
import entities.Name;

import java.util.List;

/**
 * Created by NVYas on 19.04.2017.
 */
public class NameRepository {
    public List<Name> getAll(){
        return DataBaseConnection.getDBConnection().getAllNames();
    }
}
