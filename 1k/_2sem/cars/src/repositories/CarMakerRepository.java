package repositories;

import db.DataBaseConnection;
import entities.CarMaker;
import entities.Country;

import java.util.Arrays;
import java.util.List;

/**
 * Created by NVYas on 18.04.2017.
 */
public class CarMakerRepository {

    public List<CarMaker> getAll(){
        return DataBaseConnection.getDBConnection().getAllCarMakers();
    }

    /*public List<String> getFullNamesByContinentName(String continent){
        CountryRepository countryRepo = new CountryRepository();
        List<Country> countries = countryRepo.getByContinentNames(Arrays.asList(new String[]{continent}));
    }*/
}