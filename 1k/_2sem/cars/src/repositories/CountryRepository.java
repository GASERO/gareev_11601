package repositories;

import db.DataBaseConnection;
import entities.Country;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ma on 23.03.2017.
 */
public class CountryRepository {

    public List<Country> getAll() {
        return DataBaseConnection.getDBConnection().getAllCountries();
    }

    public Country getByName(String name) {
        for (Country country : DataBaseConnection.getDBConnection().getAllCountries()) {
            if (country.getCountryName().equals(name))
                return country;
        }
        return null;
    }

    public List<Country> getByContinentNames(List<String> namesOfContinents) {
        List<Country> result = new ArrayList<>();
        for (Country country : DataBaseConnection.getDBConnection().getAllCountries()) {
            if (namesOfContinents.contains(country.getContinent().getContinent())) {
                result.add(country);
            }
        }
        return result;
    }

}
