import entities.Car;
import entities.CarMaker;
import entities.Continent;
import entities.Country;
import repositories.CarMakerRepository;
import repositories.ContinentRepository;
import repositories.CountryRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class Main {
    private static ContinentRepository continentRepo;
    private static CountryRepository countryRepo;
    private static CarMakerRepository carMakerRepo;

    public static void init() {
        continentRepo = new ContinentRepository();
        countryRepo = new CountryRepository();
        carMakerRepo = new CarMakerRepository();
    }

    public static void main(String[] args) {
        init();
//        System.out.println(continentRepo.getAll());
  //      System.out.println(countryRepo.getAll());
 //       Continent continent = continentRepo.getByName("africa");
   //     Country country = countryRepo.getByName("usa");
       // country.setContinent(continent);
     //   System.out.println(country);
        //country.save()

        //List<Country> countryList = countryRepo.getByContinentNames(Arrays.asList(new String []{"europe","asia"}));


        List<Car> cars = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File("datasets/cars-data.csv"));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Car car = new Car();
                car.setId(Integer.parseInt(strData[0]));
              // car.setName(getMakeByCarId(car.getId()));
               // car.setMpg(Integer.parseInt(strData[1]));
                car.setCylinders(Integer.parseInt(strData[2]));
              //  car.setEdispl(Integer.parseInt(strData[3]));
                car.setHorsepower(Integer.parseInt(strData[4]));
                car.setWeight(Integer.parseInt(strData[5]));
               // car.setAccelerate(Integer.parseInt(strData[6]));
                car.setYear(Integer.parseInt(strData[7]));
                cars.add(car);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Collections.sort(cars);
        System.out.println(cars);
        cars.sort(((o1, o2) -> o1.getId() - o2.getId()));
        cars.sort(new compar("weight"));
        System.out.println(cars);





    }
}
