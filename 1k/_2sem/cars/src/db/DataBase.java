package db;

import entities.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ma on 23.03.2017.
 */
public class DataBase {
    private List<Country> countries;
    private List<Continent> continents;
    private List<CarMaker> carMakers;
    private List<Model> models;
    private List<Name> names;
    private List<Car> cars;

    public void init() {
        prepareContinents("datasets/continents.csv");
        prepareCountries("datasets/countries.csv");
        prepareMakers("datasets/car-maker.csv");
        prepareModels("datasets/model-list.csv");
        prepareNames("datasets/car-names.csv");
        prepareCars("datasets/cars-data.csv");
    }

    private void prepareCountries(String filename) {
        countries = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Country country = new Country();
                country.setCountryId(Integer.parseInt(strData[0]));
                country.setCountryName(strData[1].substring(1, strData[1].length() - 1));
                country.setContinentId(Integer.parseInt(strData[2]));
                country.setContinent(getContinentById(country.getContinentId()));
                countries.add(country);
            }

        } catch (IOException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepareContinents(String filename) {
        continents = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Continent c = new Continent();
                c.setContId(Integer.parseInt(strData[0]));
                c.setContinent(strData[1].substring(1, strData[1].length() - 1));
                continents.add(c);
            }
            scanner.close();
            System.out.println("Continents Data was loaded.");
        } catch (IOException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepareMakers(String filename){
        carMakers = new LinkedList<>();
        try{
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while(scanner.hasNext()){
                String[] strData = scanner.nextLine().split(",");
                CarMaker cm = new CarMaker();
                cm.setId(Integer.parseInt(strData[0]));
                cm.setMaker(strData[1].substring(1,strData[1].length() - 1));
                cm.setFullName(strData[2].substring(1,strData[1].length() - 1));
                cm.setCountryId(Integer.parseInt(strData[3]));
                cm.setCountry(getCountryById(cm.getCountryId()));
                carMakers.add(cm);
            }
            scanner.close();
            System.out.println("Car Makers Data was loaded.");
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepareModels(String filename){
        models = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()){
                String[] strData = scanner.nextLine().split(",");
                Model model = new Model();
                model.setModelId(Integer.parseInt(strData[0]));
                model.setCarMakerId(Integer.parseInt(strData[1]));
                model.setCarMaker(getCarMakerById(model.getCarMakerId()));
                model.setModel(strData[2].substring(1,strData[2].length() - 1));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepareNames(String filename){
        names = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while(scanner.hasNext()){
                String[] strData = scanner.nextLine().split(",");
                Name name = new Name();
                name.setId(Integer.parseInt(strData[0]));
                name.setModelName(strData[1].substring(1,strData[1].length() - 1));
                name.setModel(getModelByModelName(name.getModelName()));
                name.setMake(strData[2].substring(1,strData[2].length() - 1));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepareCars(String filename){
        cars = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Car car = new Car();
                car.setId(Integer.parseInt(strData[0]));
                car.setName(getMakeByCarId(car.getId()));
                car.setMpg(Integer.parseInt(strData[1]));
                car.setCylinders(Integer.parseInt(strData[2]));
                car.setEdispl(Integer.parseInt(strData[3]));
                car.setHorsepower(Integer.parseInt(strData[4]));
                car.setWeight(Integer.parseInt(strData[5]));
                car.setAccelerate(Integer.parseInt(strData[6]));
                car.setYear(Integer.parseInt(strData[7]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public Continent getContinentById(int id) {
        for (Continent continent : continents) {
            if (continent.getContId() == id)
                return continent;
        }
        return null;
    }

    public Country getCountryById(int id){
        for(Country country : countries){
            if(country.getCountryId() == id) return country;
        }
        return null;
    }

    public CarMaker getCarMakerById(int id){
        for(CarMaker carMaker : carMakers){
            if(carMaker.getId() == id) return carMaker;
        }
        return null;
    }

    public Model getModelByModelName(String modelName){
        for(Model model : models){
            if(model.getModel().equals(modelName)) return model;
        }
        return null;
    }

    public Name getMakeByCarId(int id){
        for(Name name : names){
            if(name.getId() == id) return name;
        }
        return null;
    }


    public List<Continent> getAllContinents() {
        return continents;
    }

    public List<Country> getAllCountries() {
        return countries;
    }

    public List<CarMaker> getAllCarMakers() {return carMakers;}

    public List<Model> getAllModels(){return models;}

    public List<Name> getAllNames() {return names;}

    public List<Car> getAllCars() {return cars;}
}
