import java.util.ArrayList;

/**
 * Created by Роберт on 04.05.2017.
 */
public class Main {
    static ArrayList results = new ArrayList();
    static int[] components = {0,1,2};
    public static void main(String[] args) {

        int n = 5;
        backTracking(n);
        results.stream()
                .forEach(System.out::println);

    }

    public static void backTracking(int n) {
        ArrayList<Integer> list = new ArrayList<>();
        Integer y;
        for (int x :
                components) {
            list.add(x);
        }
        list.remove(0);
        while (!list.isEmpty()) {
            y = list.get(0);
            if (length(y) >= n) {
                results.add(y);
            } else {
                for (int g : components
                        ) {
                    y = y * 10 + g;
                    if (check(y) && length(y) <= n) {
                        list.add(y);

                    }
                    y /=10;
                }

            }
            list.remove(y);

        }

    }
    public static boolean check(int x){
        int digit;
        int k =0;
        if (x == 0) return false;
        while (x > 0){
            digit = x % 10;
            if (digit == 1) k++;
            if (k > 2) return  false;
            x /= 10;
        }
        return true;
    }
    public static int length(int x){
        int k = 0;
        do{
            k++;
            x /=10;
        }while(x>0);
        return k;
    }
}
