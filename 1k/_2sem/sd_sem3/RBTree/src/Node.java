/**
 * Created by Роберт on 11.04.2017.
 */
public class Node {
    private int value;
    private Node leftChild,rightChild;
    private boolean red; // true = red; false = black;

    public Node(int value, boolean red) {
        this.value = value;
        this.red = red;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public boolean isRed() {
        return red;
    }

    public void paintRed() {
        this.red = true;
    }
    public void paintBlack(){
        this.red = false;
    }
}
