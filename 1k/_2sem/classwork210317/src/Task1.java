import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by Роберт on 21.03.2017.
 */
public class Task1 {
    public static void main(String[] args) throws FileNotFoundException{
        File file = new File("en_v1.dic");
        Scanner sc = new Scanner(file);
        TreeMap map = new TreeMap();
        String[] a;
        while (sc.hasNextLine()){
            a = sc.nextLine().split(" ");
            map.put(a[0],a[1]);
        }
        System.out.println(map);

    }
}
