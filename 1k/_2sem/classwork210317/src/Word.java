/**
 * Created by Роберт on 22.03.2017.
 */
public class Word implements Comparable<Word> {
    @Override
    public String toString() {
        return value + " " + frequency;
    }

    @Override
    public int compareTo(Word o) {
        return -value.compareTo(o.value);
    }

    private String value;
    private int frequency;

    public String getValue() {
        return value;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public Word(String value, int frequency) {
        this.value = value;
        this.frequency = frequency;
    }
}
