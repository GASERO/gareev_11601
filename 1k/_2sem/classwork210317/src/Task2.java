import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Array;
import java.util.*;

/**
 * Created by Роберт on 21.03.2017.
 */
public class Task2 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("en_v1.dic");
        Scanner sc = new Scanner(file);
        TreeMap<Integer, Set<String>> map = new TreeMap();
        String[] a;
        while (sc.hasNextLine()){
            a = sc.nextLine().split(" ");
            if (map.containsKey(Integer.parseInt(a[1]))){
                map.get(Integer.parseInt(a[1])).add(a[0]);
            }else{
                HashSet b = new HashSet();
                map.put(Integer.parseInt(a[1]),b);
                b.add(a[0]);
            }
        }
        System.out.println(map);
        for (Map.Entry<Integer, Set<String>> e : map.entrySet()) {
            System.out.print(e.getKey() + " has values : ");
            for (Iterator<String> it = e.getValue().iterator()
                 ; it.hasNext(); ) {
                String s = it.next();
                System.out.print(s + " ");
            }
            System.out.println();
        }
    }
}
