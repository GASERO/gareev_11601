import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Роберт on 22.03.2017.
 */
public class Task3 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("en_v1.dic");
        Scanner sc = new Scanner(file);
        ArrayList<Word> list = new ArrayList<>();
        String[] a;
        while (sc.hasNextLine()){
            a = sc.nextLine().split(" ");
            list.add(new Word(a[0],Integer.parseInt(a[1])));
        }
        Collections.sort(list);
        System.out.println(list);

        Collections.sort(list, (o1, o2) -> -o1.getFrequency() + o2.getFrequency());
        System.out.println(list);
        Collections.sort(list, (o1, o2) -> -o1.getFrequency() + o2.getFrequency() +
                (-o1.getFrequency() + o2.getFrequency()) == 0 ?
                o1.getValue().compareTo(o2.getValue()) :
                0
        );
        System.out.println(list);
    }

}
