import java.util.Arrays;

/**
 * Created by Роберт on 11.05.2017.
 */
public class Task2 {
    public static void main(String[] args) {
        int[] a = {9,8,7,6,5,4,3,2,1};
        MyThread2 t1 = new MyThread2(a,0,a.length / 2);
        MyThread2 t2 = new MyThread2(a,a.length / 2,a.length);
        t1.start();
        t2.start();
        try{
            t1.join();
            t2.join();
        }catch (Exception e){}
        int x = 0, y = a.length /2;
        int[] b = new int[a.length];
        int k = 0;
        while (1 == 1){
                if (a[x] < a[y]) {
                    b[k] = a[x];
                    x++;
                } else {
                    b[k] = a[y];
                    y++;
                }
                k++;
            if (x >=  a.length/2){
                for (int i = y; i < a.length; i++) {
                    b[k] = a[i];
                    k++;
                }
                break;
            }
            if (y >= a.length){
                for (int i = x; i < a.length / 2; i++) {
                    b[k] = a[i];
                    k++;
                }
                break;
            }

        }
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));

    }
}

class MyThread2 extends Thread {
    int[] array;
    int begin, end;

    public MyThread2(int[] array, int begin, int end) {
        this.array = array;
        this.begin = begin;
        this.end = end;
    }

    @Override
    public void run() {


        for (int i = end - 1; i > begin; i--) {
            for (int j = begin; j < i; j++) {

                if (array[j] > array[j + 1]) {
                    int t = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = t;
                }
            }
        }

    }

}
