/**
 * Created by Роберт on 11.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        int[] a = {1,1,1,1,1,1,1,1,1,1};
        MyThread t1 = new MyThread(a,0,a.length / 2);
        MyThread t2 = new MyThread(a,a.length / 2 +1,a.length - 1);
        t1.start();
        t2.start();
        try{
            t1.join();
            t2.join();
        }catch (Exception e){

        }
        System.out.println(t1.getSum() + t2.getSum());

    }
}
class MyThread extends Thread{
    int[] array;
    int begin,end,sum;

    public MyThread(int[] array, int begin, int end) {
        this.array = array;
        this.begin = begin;
        this.end = end;
        sum = 0;
    }

    @Override
    public void run() {
        for (int i = begin; i <= end ; i++) {
            sum +=array[i];
        }
    }

    public int getSum() {
        return sum;
    }
}
