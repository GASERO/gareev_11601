import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class Task08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }
        int s = 0;
        Point zero = new Point(0,0);
        for (int i = 0; i < n - 1; i++) {
           s =s + (new Vector(zero,points[i]).pseudoscalarProduct(new Vector(zero,points[i+1])) / 2);
        }
        s =s + (new Vector(zero,points[n-1]).pseudoscalarProduct(new Vector(zero,points[0])) / 2);
        s = Math.abs(s);
        System.out.println("s = "+ s);
    }
}
