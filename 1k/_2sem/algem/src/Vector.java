/**
 * Created by Роберт on 24.05.2017.
 */
public class Vector {
    private int x,y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Point a, Point b) {
        this.x = b.getX()-a.getX();
        this.y = b.getY()-a.getY();
    }

    public int pseudoscalarProduct(Vector v){
        return v.getY()*getX() - v.getX() * getY();
    }

    public int scalarProduct(Vector v){
        return v.getX()*getX() + v.getY() * getY();
    }
}
