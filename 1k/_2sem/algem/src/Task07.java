import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Point[] points = new Point[4];
        for (int i = 0; i < 4; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }
        int a = check(points[0],points[1],points[2],points[3]);
        int b = check(points[2],points[0],points[1],points[3]);
        int c = check(points[1],points[2],points[0],points[3]);
        if (a < 0| b < 0 | c < 0){
            System.out.println("Лежит вне");
        }else{
            if (a ==0 |b==0|c==0){
                System.out.println("Лежит на стороне");
            }else{
                System.out.println("Лежит внутри");
            }
        }


    }
    public static int check(Point a, Point b, Point c, Point x){
        Vector v1 = new Vector(a,b);
        Vector v2 = new Vector(a,c);
        Vector v3 = new Vector(a,x);
        return v1.pseudoscalarProduct(v2) * v1.pseudoscalarProduct(v3);
    }
}
