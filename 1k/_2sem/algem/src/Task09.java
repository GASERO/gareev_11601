import java.util.Scanner;

/**
 * Created by Роберт on 25.05.2017.
 */
public class Task09 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }
        boolean k = true;
        for (int i = 0; i < n - 2; i++) {
            if (new Vector(points[i],points[i+1]).pseudoscalarProduct(new Vector(points[i+1],points[i+2])) > 0){
                k = false;
                break;
            }

        }
        if (k) System.out.println("Выпуклый");
        else System.out.println("Не выпуклый");
    }
}
