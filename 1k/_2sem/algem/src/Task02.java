

import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class Task02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Point[] points = new Point[3];
        for (int i = 0; i < 3; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }
        Vector v1 = new Vector(points[0],points[1]);
        Vector v2 = new Vector(points[0],points[2]);
        if (v1.pseudoscalarProduct(v2) == 0){
            System.out.println("It isn't a triangle");
        }else{
            System.out.println("It's a triangle");
        }

    }
}
