import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class Task06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Point[] points = new Point[3];
        for (int i = 0; i < 3; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }
        Vector v1 = new Vector(points[0],points[1]);
        Vector v2 = new Vector(points[0],points[2]);

        System.out.println("s = " + Math.abs(v1.pseudoscalarProduct(v2) / 2));
    }
}
