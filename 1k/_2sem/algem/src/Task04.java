import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class Task04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Point[] points = new Point[3];
        for (int i = 0; i < 3; i++) {
            points[i] = new Point(sc.nextInt(),sc.nextInt());
        }

        int k = new Vector(points[0],points[1]).scalarProduct(new Vector(points[0],points[2]))*
                new Vector(points[2],points[0]).scalarProduct(new Vector(points[2],points[1]))*
                new Vector(points[1],points[0]).scalarProduct(new Vector(points[1],points[2]));
        if (k > 0){
            System.out.println("Остроугольный");
        }else{
            if (k < 0){
                System.out.println("Тупоугольный");
            }
            else{
                System.out.println("Прямоугольный");
            }
        }
    }
}
