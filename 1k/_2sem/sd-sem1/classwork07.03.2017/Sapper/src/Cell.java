/**
 * Created by Роберт on 07.03.2017.
 */
public class Cell {
    private int value;
    private boolean mined;
    private boolean opened;
    Cell(){
        this.value = 0;
        this.opened = false;
        mined = false;
    }
    Cell(int value,boolean mined){
        opened = false;
        this.mined = mined;
        this.value = value;
    }

    public boolean isMined() {
        return mined;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public int getValue() {
        return value;
    }

    public boolean isOpened() {
        return opened;
    }
    public void setMined(boolean x){
        mined = x;
    }
}
