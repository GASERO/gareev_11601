import java.util.Scanner;

/**
 * Created by Роберт on 07.03.2017.
 */
public class Main {
        public final static int n = 10;
    public static void main(String[] args) {

        Cell[][] map = new Cell[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                map[i][j] = new Cell();
            }
        }
        int x,y;
        for (int i = 0; i < n; i++){
            x = (int) (n * Math.random());
            y = (int)(n * Math.random());
            map[x][y].setMined(true);
            for (int j = -1; j < 2; j++) {
                for (int k = -1; k < 2; k++) {
                    if ((j + x >= 0) &&(j + x < n)&& (k+y >= 0)&& (k+y< n)){
                        map[j+x][k+y].setValue(map[j+x][k+y].getValue() + 1);
                    }
                }
            }
        }
        LinkedQueue<Cell> queue = new LinkedQueue();
        Scanner sc = new Scanner(System.in);
        while(1 > 0){
            print(map);
            y = sc.nextInt();
            x = sc.nextInt();
            if (map[y][x].isOpened()){
                System.out.println("Cell is already opened");
                continue;
            }
            else{
                if (map[y][x].isMined()){
                    System.out.println("gg");
                    break;
                }
                else{
                    if (map[y][x].getValue() != 0){
                        map[y][x].setOpened(true);
                    }
                    else{
                        queue.add(map[y][x]);
                        while(!queue.isEmpty()){
                            Cell p = queue.poll();
                            p.setOpened(true);
                            for (int j = -1; j < 2; j++) {
                                for (int k = -1; k < 2; k++) {
                                    if ((j + x >= 0) &&(j + x < n)&& (k+y >= 0)&& (k+y< n)){
                                        queue.add(map[j+x][k+y]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
    public static void print(Cell[][] map){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (map[i][j].isOpened()){
                    System.out.print(map[i][j].getValue());
                }
                else{
                    System.out.println('Ш');
                }
            }
        }
    }
}
