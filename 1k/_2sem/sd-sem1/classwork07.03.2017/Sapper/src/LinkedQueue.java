/**
 * Created by Роберт on 07.03.2017.
 */
public class LinkedQueue<T> implements Queue {
    Node head;
    Node tail;

    @Override
    public void add(Object value) {
        if (head == null){
            head = new Node(value,null);
            tail = head;
            return;
        }
        Node x = new Node(value,null);
        tail.setNext(x);
        tail = x;
    }

    @Override
    public boolean isEmpty() {
        if (head == null) return true;
        return false;
    }

    @Override
    public T poll() {
        Node x = head;
        head = head.getNext();
        return x.getValue();
    }

    @Override
    public Object getNode() {
        return head;
    }
}
