/**
 * Created by Роберт on 07.03.2017.
 */
public class Node<T> {
    T value;
    Node next;
    Node(T value, Node next){
        this.value = value;
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
