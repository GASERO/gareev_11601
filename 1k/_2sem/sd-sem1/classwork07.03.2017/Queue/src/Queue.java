/**
 * Created by Роберт on 07.03.2017.
 */
public interface Queue<T> {
    void add(T value);
    boolean isEmpty();
    T poll();
    T getNode();
}
