import java.util.List;

/**
 * Created by Роберт on 16.02.2017.
 */
public interface IStack<T>{
     void push(T x);
     Object pop();
     boolean isEmpty();
}
