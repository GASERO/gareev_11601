import java.util.Objects;

/**
 * Created by Роберт on 07.03.2017.
 */
public class ArrayQueue<T> implements Queue {
    private int capacity = 100;
    private Object[] a = new Object[capacity];
    private int size;
    ArrayQueue(){
        size = 0;
    }

    @Override
    public void add(Object value) {
        a[size++] = value;
        if (size >= (capacity * 0.66)){
            capacity = (int) 1.5 * capacity;
            Object[] b = new Object[capacity];
            for (int i = 0; i < size; i++) {
                b[i] = a[i];

            }
            a = b;
        }
    }

    @Override
    public boolean isEmpty() {
        if (size > 0) return false;
        return true;
    }

    @Override
    public Object poll() {
        Object b = a[0];
        for (int i = 1; i < size; i++) {
            a[i-1] = a[i];
        }
        size--;
        return b;
    }

    @Override
    public Object getNode() {
        return a[0];
    }
}
