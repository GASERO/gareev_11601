/**
 * Created by Роберт on 16.02.2017.
 */
public class Stack<T> implements IStack {



    private Node head;

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public Object pop() {
        if (head != null){
        Object a = head.getValue();
        head = head.getNext();
        return a;}
        else return null;


    }
    public Object get(){
        return head;
    }

    @Override
    public void push(Object x) {
        head = new Node(x,head);
    }
}
