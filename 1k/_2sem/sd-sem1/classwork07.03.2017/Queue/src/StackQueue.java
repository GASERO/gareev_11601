/**
 * Created by Роберт on 07.03.2017.
 */
public class StackQueue<T> implements Queue {
    private Stack a,b;
    StackQueue(){
        a = new Stack();
        b = new Stack();
    }

    @Override
    public void add(Object value) {
        a.push(new Node(value,null));
    }

    @Override
    public boolean isEmpty() {
        if (a.isEmpty() && b.isEmpty()) return true;
        return false;
    }

    @Override
    public Object poll() {
        Object p = b.pop();
        if (b.isEmpty()){
            while (!a.isEmpty()){
                b.push(a.pop());
            }
        }
        return p;

    }

    @Override
    public Object getNode() {
        return b.get();
    }
}
