import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Роберт on 25.02.2017.
 */
public class QuickSort {
    public static int sort(int[] a, int l, int r){
        int t;
        int v = a[(l+r)/2];
        int i = l;
        int iter = 0;
        int j = r;
        while(i <= j){
            while (a[i] < v) i++;
            while (a[j] > v) j--;
            if (i <= j) {
                t = a[i];
                a[i] = a[j];
                a[j] = t;
                i++;
                j--;
                iter++;
            }
        }
        if (l < j) iter+=sort(a,l,j);
        if (j > 1) iter+=sort(a,l,j);
        return iter;
    }
    public static int sort(List<Integer> a, int l, int r){
        int t;
        int iter = 0;
        int v = a.get((l+r)/2);
        int i = l;
        int j = r;
        while(i <= j){
            while (a.get(i) < v) i++;
            while (a.get(j) > v) j--;
            if (i <= j) {
                iter++;
                t = a.get(i);
                a.set(i,a.get(j));
                a.set(j,t);
                i++;
                j--;
            }
        }
        if (l < j) iter += sort(a,l,j);
        if (j > 1) iter += sort(a,l,j);
        return iter;
    }
}
