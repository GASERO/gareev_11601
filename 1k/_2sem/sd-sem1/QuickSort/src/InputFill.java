import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by Роберт on 02.04.2017.
 */
public class InputFill  {
    public static void main(String[] args) throws FileNotFoundException{
        File file = new File("input.txt");
        PrintWriter pw = new PrintWriter(file);
        for (int i = 100; i <=10000 ; i+=990) {
            for (int j = 0; j < 10; j++) {
                for (int k = 0; k < i-1; k++) {
                    pw.print((int)(Math.random() * 1000) +",");
                }
                pw.print((int)(Math.random() * 1000));
                pw.println();
            }
        }
        pw.close();


    }

}
