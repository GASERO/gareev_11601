import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Роберт on 25.02.2017.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        Scanner sc = new Scanner(new File("input.txt"));
        /*LinkedList<Integer> a;
        String[] s;
        PrintWriter stats = new PrintWriter(new File("statsForLinkedList.txt"));
        long start, finish;
        for (int i = 100; i <= 10000; i += 990) {
            long iter = 0;
            long time = 0;
            for (int j = 0; j < 10; j++) {


                s = sc.nextLine().split(",");
                a = new LinkedList<>();
                pw.print(i + 1 + " : ");
                for (String x : s
                        ) {
                    a.add(Integer.parseInt(x));
                }
                start = System.nanoTime();
                int x = QuickSort.sort(a, 0, a.size() - 1);
                finish = System.nanoTime();
                pw.println("iter = " + x);
                pw.println("time " + (finish - start) + " ns ");
                pw.println(a);
                pw.println();
                iter +=x;
                time += finish - start;

            }
            stats.println(i +" : \n iter = " + iter / 10 + "\n time in ns = " + time / 10 + "\n");

        }*/

        int[] a;
        String[] s;
        PrintWriter stats = new PrintWriter(new File("statsForArray.txt"));
        long start, finish;
        for (int i = 100; i <= 10000; i += 990) {
            long iter = 0;
            long time = 0;
            for (int j = 0; j < 10; j++) {


                s = sc.nextLine().split(",");
                a = new int[i];
                pw.print(i + 1 + " : ");
                int k = 0;
                for (String x : s
                        ) {
                    a[k] = Integer.parseInt(x);
                    k++;
                }
                start = System.nanoTime();
                int x = QuickSort.sort(a, 0, a.length - 1);
                finish = System.nanoTime();
                pw.println("iter = " + x);
                pw.println("time " + (finish - start) + " ns ");
                pw.println(a);
                pw.println();
                iter +=x;
                time += finish - start;

            }
            stats.println(i +" : \n iter = " + iter / 10 + "\n time in ns = " + time / 10 + "\n");

        }


        pw.close();
        stats.close();
    }
}
