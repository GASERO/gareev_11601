/**
 * Created by Роберт on 04.03.2017.
 */
public class Node {
    private int value;
    private int i;
    private int j;
    private Node next;

    public Node( int i, int j, int value) {
        this.value = value;
        this.i = i;
        this.j = j;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
