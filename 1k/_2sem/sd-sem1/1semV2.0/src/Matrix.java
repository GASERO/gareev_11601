import java.util.ArrayList;

/**
 * Created by Роберт on 04.03.2017.
 */
public class Matrix {
    private Node head;
    private int order;
    Matrix(int[][] a){
        order = a.length;
        Node x;
        Node prev = new Node(0,0,a[0][0]);
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                x = new Node(i,j,a[i][j]);
                if (i == 0 && j == 0){
                    head = x;
                }
                prev.setNext(x);
                prev = x;
            }
        }
    }
    public void write(){
        Node p = head;
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (p != null) {
                    System.out.print(p.getValue() + " ");
                    p = p.getNext();
                }
            }
            System.out.println();
        }
        System.out.println();
    }
    public int[][] decode(){
        int[][] a = new int[order][order];
        Node p = head;
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (p != null) {
                    a[i][j] = p.getValue();
                    p = p.getNext();
                }
            }
        }
        return  a;
    }
    public void insert(int i, int j, int value){
        Node p = head;
        for (int k = 0; k < order; k++) {
            for (int l = 0; l < order; l++) {
                if(p.getI() == i & p.getJ() == j){
                    p.setValue(value);
                    return;
                }
                p = p.getNext();
            }
        }
    }
    public void delete(int i, int j){
        insert(i,j,0);
    }
    public ArrayList<Integer> minList(){
        ArrayList<Integer> list = new ArrayList<>();
        Node p = head;
        for (int i = 0; i < order; i++) {
            list.add(p.getValue());
            p = p.getNext();
        }
        for (int i = 1; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (p.getValue() < list.get(j)){
                    list.set(j,p.getValue());
                }
                p = p.getNext();
            }
        }
        return list;
    }
    public int sumDiag(){
        int sum = 0;
        Node p = head;
        for (int i = 0; i < order * order; i++) {
            if (i % (order + 1) == 0){
                sum += p.getValue();
            }
            p = p.getNext();
        }
        return sum;
    }
    public void transp(){
        Node p = head;
        int a;
        Node x;
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (p.getI() < p.getJ()){
                    x = p.getNext();
                    while(!(x.getI() == p.getJ() && x.getJ() == p.getI())){
                        x = x.getNext();
                    }
                    a = p.getValue();
                    p.setValue(x.getValue());
                    x.setValue(a);
                }
                p = p.getNext();
            }
        }
    }
    public void sumCols(int j1, int j2){
        Node p = head;
        Node s1 =null,s2 = null;
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (j == j1) s1 = p;
                if (j == j2) s2 = p;
                p = p.getNext();
            }
            s1.setValue(s1.getValue() + s2.getValue());

        }
    }
}
