import javafx.scene.control.ComboBox;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by Роберт on 16.03.2017.
 */
public class BinaryTreeIntCollection<T extends Comparable<T>> implements MyCollection<T> , Iterable<T> {

    class Node<T> {
        private T value;
        private Node left;
        private Node right;

        public Node(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }
    }


    private Node root = null;
    private int size = 0;

    @Override
    public Iterator iterator() {
        return null;
    }
    

    @Override
    public boolean add(T value) {
        if (root == null){
           root = new Node(value); 
        }else{
            Node p = root;
            boolean isLeft = true;
            boolean go = true;
            while (go){
                if (value.compareTo((T) p.getValue()) < 0){
                    isLeft = true;
                    if (p.getLeft() != null){
                        p = p.getLeft();
                    }
                    else{
                        go = false;
                    }
                }else{
                    isLeft = false;
                    if (p.getRight() != null){
                        p = p.getRight();
                    }
                    else{
                        go = false;
                    }
                }
            }
            if(isLeft){
                p.setLeft(new Node(value));
            }else{
                p.setRight(new Node(value));
            }
        }
        return true;
        
    }

    @Override
    public void remove() {
        
    }

    public void printTree(){
       printNode(root,0);
    }

    private void printNode(Node<T> p, int h){
        if (p!= null){
            for (int i = 0; i < h; i++) {
                System.out.print(" ");
            }
            printNode(p.getLeft(),h+1);
            System.out.println(p.getValue());
            printNode(p.getRight(),h+1);
        }
    }

    @Override
    public boolean contains() {
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public Iterator createIterator() {
        return null;
    }
}
