import java.util.Iterator;

/**
 * Created by Роберт on 16.03.2017.
 */
public interface MyCollection<T> {
    boolean add(T value);
    void remove();
    boolean contains();
    int size();
    void clear();
    Iterator createIterator();

}
