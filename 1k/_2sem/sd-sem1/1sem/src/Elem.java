/**
 * Created by Роберт on 23.02.2017.
 */
public class Elem {
    private int value;
    private int column;
    private int row;
    private Elem up ;
    private Elem down;
    private Elem right;
    private Elem left;
    Elem(int row, int column, int value){
        this.column = column;
        this.row = row;
        this.value = value;
        this.up  = null;
        this.down = null;
        this.right = null;
        this.left = null;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setUp(Elem up) {
        this.up = up;
    }

    public void setDown(Elem down) {
        this.down = down;
    }

    public void setRight(Elem right) {
        this.right = right;
    }

    public void setLeft(Elem left) {
        this.left = left;
    }

    public int getValue() {

        return value;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public Elem getUp() {
        return up;
    }

    public Elem getDown() {
        return down;
    }

    public Elem getRight() {
        return right;
    }

    public Elem getLeft() {
        return left;
    }
}
