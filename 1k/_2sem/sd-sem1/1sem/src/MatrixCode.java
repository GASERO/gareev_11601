import java.util.ArrayList;

/**
 * Created by Роберт on 23.02.2017.
 */
public class MatrixCode {
    private int size;
    private Elem head;
    MatrixCode(int [][] a){
        this.size = a.length;
        Elem x,prev;
        ArrayList<Elem> z = new ArrayList<>();
        head = new Elem(0,0,a[0][0]);
        prev = head;
        z.add(head);
        for (int i = 1; i < size ; i++) {
            x = new Elem(0,i,a[0][i]);
            prev.setRight(x);
            x.setLeft(prev);
            prev = x;
            z.add(x);
        }
        for (int i = 1; i < size ; i++) {
            prev = new Elem(i,0,a[i][0]);
            prev.setUp(z.get(0));
            z.get(0).setDown(prev);
            z.set(0,prev);
            for (int j = 1; j < size ; j++) {
                x = new Elem(i,j,a[i][j]);
                prev.setRight(x);
                x.setLeft(prev);
                x.setUp(z.get(j));
                z.get(j).setDown(x);
                z.set(j,x);
            }

        }
    }

    public Elem getElem(int y, int x){
        Elem i;
        for (i = head; i.getRow() < y; i = i.getDown());
        for (; i.getColumn() < x; i = i.getRight());
        return i;

    }

    public int[][] decode(){
        int[][] a = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                a[i][j] = getElem(i,j).getValue();
            }

        }
        return a;
    }
    public void insert(int i,int j,int value){
        getElem(i,j).setValue(value);
    }
    public void delete(int i,int j){
        getElem(i,j).setValue(0);
    }
    public ArrayList minList(){
        ArrayList<Integer> a = new ArrayList<>();
        Elem x = head;
        for (int i = 0; i < size ; i++) {
            a.add(x.getValue());
            if (i < size - 1)x = x.getRight();
        }
        for (int i = 1; i < size; i++) {
            x = getElem(i,0);
            for (int j = 0; j < size ; j++) {
                if (x.getValue() < a.get(j)) a.set(j,x.getValue());
                if (i < size - 1) x = x.getRight();
            }
        }

        return a;

    }
    public int sumDiag(){
        int a = 0;
        Elem x = head;
        for (int i = 0; i < size; i++) {
            a += x.getValue();
            if (i < size - 1)x = x.getRight().getDown();
        }
        return a;
    }
    public void transp(){
        int a;
        for (int i = 1; i < size; i++) {
            for (int j = 0; j < i; j++) {
                a = getElem(i,j).getValue();
                getElem(i,j).setValue(getElem(j,i).getValue());
                getElem(j,i).setValue(a);
                
            }            
        }
    }
    public void sumCols(int j1, int j2){
        for (int i = 0; i < size; i++) {
            getElem(i,j1).setValue(getElem(i,j1).getValue() + getElem(i,j2).getValue());
        }
    }


    public int getSize() {
        return size;
    }

    public Elem getHead() {
        return head;
    }
}
