/**
 * Created by Роберт on 16.02.2017.
 */
public class IntStack implements IStack {
    private class intElem{
        private Integer value;
        private intElem next;

        public intElem(
                int value,
                intElem next) {
            this.value = value;
            this.next = next;
        }

        public void setValue(
                int value) {
            this.value = value;
        }

        public void setNext(
                intElem next) {
            this.next = next;
        }

        public
        int getValue() {
            return value;
        }

        public

        intElem getNext() {
            return next;
        }
    }


    private
    intElem head;

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public
    Integer pop() {
        if (head != null){


            int a = head.getValue();
        head = head.getNext();
        return a;}
        else return null;


    }

    @Override
    public void push(
            int x) {
        head = new


                intElem(x,head);
    }
}
