import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Роберт on 27.02.2017.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException{
        int[][] table = new int[6][7];
        Scanner sc = new Scanner(new File("table.txt"));
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                table[i][j] = sc.nextInt();
            }
        }
        System.out.println(InfToPost("1 + ( 2 * ( 12 - 2 ) - 10 ) / 5",table));
        /*for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(table[i][j] + "");
            }
            System.out.println();
        }*/
        String s = InfToPost("1 + ( 2 * ( 12 - 2 ) - 10 ) / 5",table);
        s = s.substring(0,s.length()-1);
        System.out.println(s);
        String[] x = s.split(",");
        IntStack is = new IntStack();
        int a;
        int b;
        for (String z : x
                ) {
            if (z.equals("-") || z.equals("+") ||z.equals("*") ||z.equals("/") ){
                a = is.pop();
                b = is.pop();
                switch (z){
                    case "-" : is.push(b-a);
                        break;
                    case "+" : is.push(a+b);
                        break;
                    case "*" : is.push(a*b);
                        break;
                    case "/" : is.push(b / a);
                        break;
                }
            }
            else{
                is.push(Integer.parseInt(z));
            }
        }
        System.out.println(is.pop());
    }
    public static String InfToPost(String a, int[][] table){
        CharStack cs = new CharStack();
        String result = "";
        a += " #";
        String[] b = a.split(" ");
        boolean flag = false;
        int x1 = 0,y1 = 0;
        char z;
        cs.push('#');
        String x = b[0];
        int k = 0;
        while(k < b.length && !cs.isEmpty()){
            switch(z = cs.pop()){
                case '-':
                    y1 = 2;
                    break;
                case '+':
                    y1 = 1;
                    break;
                case '*':
                    y1 = 3;
                    break;
                case '/':
                    y1 = 4;
                    break;
                case '(':
                    y1 = 5;
                    break;
                case '#':
                    y1 = 0;
                    break;
            }
            cs.push(z);
            switch (x){
                case "-":
                    x1 = 2;
                    flag = true;
                    break;
                case "+":
                    x1 = 1;
                    flag = true;
                    break;
                case "*":
                    x1 = 3;
                    flag = true;
                    break;
                case "/":
                    x1 = 4;
                    flag = true;
                    break;
                case "(":
                    x1 = 5;
                    flag = true;
                    break;
                case ")":
                    x1 = 6;
                    flag = true;
                    break;
                case "#":
                    x1 = 0;
                    flag = true;
                    break;
                default:
                    result+=x + ',';
                    k++;
                    x = b[k];
                    break;
            }
            if (flag){
                int mem = table[y1][x1];
                switch (mem){
                    case 1:
                        cs.push(x.charAt(0));
                        k++;
                        x = b[k];
                        break;
                    case 2:
                        result += cs.pop();
                        result +=',';
                        break;
                    case 3:
                        cs.pop();
                        k++;
                        x = b[k];
                        break;
                    case 4:
                        return result;
                    case 5:
                        System.out.println("error");
                        return null;

                }
            }
            flag = false;
        }
        return "nothing";
    }
}
