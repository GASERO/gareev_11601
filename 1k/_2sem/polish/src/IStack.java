/**
 * Created by Роберт on 16.02.2017.
 */
public interface IStack {
    void push(int x);
    Integer pop();
    boolean isEmpty();
}
