import java.util.Arrays;

public class Main{
    public static void main(String[] args) throws InterruptedException {
        int[] array = new int[]{12, 15, 3, 4, 7, 8, 9, 10, 47, 11, 28, 17, 90};



        int new_arrays_length = array.length / 4;
        int thread_number = 0;
        int[] maxes = new int[5];
        for (int i = 0; i < 4; i++) {
            int[] arr = new int[new_arrays_length];
            for (int j = 0; j < new_arrays_length; j++) {
                arr[j] = array[j + thread_number * new_arrays_length];
            }
            thread_number++;
            MyThread t1 = new MyThread(arr);
            t1.start();
            Thread.sleep(1000);
            maxes[i] = t1.getMax();
        }
        System.out.println(Arrays.toString(maxes));

    }
}

class MyThread extends Thread {
    int[] array;
    int max;

    public synchronized int getMax() {
        return max;
    }

    public MyThread(int[] array) {
        this.array = array;
    }

    public void run() {
        Arrays.sort(array); //Предварительная сортировка по возрастанию
        max = array[array.length - 1];
    }
}