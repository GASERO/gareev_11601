/**
 * Created by Роберт on 13.02.2017.
 */
public class MyLinkedList {
    Elem head;
    Elem tail;
    MyLinkedList(Elem head){
        this.head = head;
        Elem f;
        for (f = head; f.getNext() == null ; f = f.getNext()) {
        }
        this.tail = f;
    }
    MyLinkedList(){
        head = null;
        tail = null;
    }
    public void input(int y){
        Elem x = new Elem();
        x.setValue(y);
        tail.setNext(x);
        tail = x;
        if (head == null){
            head = x;
        }
    }
    public String toString(){
        String s = "";
        for (Elem f = head; f.getNext() != null ; f = f.getNext()) {
            s +=f.getValue() +"/n";
        }
    }

    public Elem getHead() {
        return head;
    }

    public Elem getTail() {
        return tail;
    }

    public void add(MyLinkedList x){
        tail.setNext(x.getHead());
        tail = x.getTail();
    }
    public void addHead(int x){
        Elem p = new Elem(x,null);
        p.setNext(head);
        this.head = p;
    }
    public void addTail(int x){
        Elem p = new Elem(x,null);
        tail.setNext(p);
        this.tail = p;

    }
    public void deleteHead(){
        Elem p = head.getNext();
        head.setNext(null);
        head = p;
    }
    public void deleteTail(){
        Elem p;
        for ( p = head; p.getNext().getNext() == null ; p = p.getNext()) {
        }
        p.setNext(null);
        tail = p;
    }
}
