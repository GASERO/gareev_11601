import java.util.Scanner;

/**
 * Created by Роберт on 13.02.2017.
 */
public class Task03 {
    public static void main(String[] args) {
        Elem p = fill();
        Elem k = fill();
        add(k,p);


    }
    public static Elem fill(){
        Scanner sc = new Scanner(System.in);
        Elem p;
        Elem head = null;
        int n = 5;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }
        return head;

    }
    public static void add(Elem h1, Elem h2){
        Elem f;
        for (f = h1; f.getNext() != null ; f = f.getNext());
        f.setNext(h2);
        Elem p = h1;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }

}

