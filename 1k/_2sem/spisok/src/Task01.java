import java.util.Scanner;

/**
 * Created by Роберт on 13.02.2017.
 */
public class Task01 {
    public static void main(String[] args) {
        Elem head = null;
        Elem p;
        int n = 10;
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }
        p = head;
        boolean hasMinus = false;
        for (p = head; p != null ; p = p.getNext()) {
            if (p.getValue() < 0) {
                hasMinus = true;
            }
            if (hasMinus) break;
            System.out.println("*");
        }
        if (hasMinus) System.out.println("Exists");
        else System.out.println("Does not exist");
    }
}
