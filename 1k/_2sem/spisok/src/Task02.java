import java.util.Scanner;

/**
 * Created by Роберт on 13.02.2017.
 */
public class Task02 {
    public static void main(String[] args) {
        int n = 5;
        Scanner sc = new Scanner(System.in);

        Elem k = fill();
        Elem p = fill();
        Elem f;
        for (f = k; f.getNext() !=null ; f = f.getNext()) {
        }
        f.setNext(p);
        p = k;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }

    }
    public static Elem fill(){
        Scanner sc = new Scanner(System.in);
        Elem p;
        Elem head = null;
        int n = 5;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.setValue(sc.nextInt());
            p.setNext(head);
            head = p;
        }
        return head;

    }

}
