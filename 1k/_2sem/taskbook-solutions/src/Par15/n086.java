package Par15;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n086 {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        TimeThread[] t = new TimeThread[3];
        for (int i = 0; i < 3; i++) {
            t[i] = new TimeThread("city"+i, (int)Math.pow(-1,i)*i,c);
        }
        for (TimeThread x :
                t) {
            x.start();
        }

    }
    private static class TimeThread extends Thread{
        private String city;
        private int difference;
        private Calendar c;
        public TimeThread(String city, int difference, Calendar c) {
            this.city = city;
            this.difference = difference;
            this.c = c;
        }

        @Override
        public void run() {
            int t = c.getTime().getHours() + difference;
            while (t > 23){
               t -=24;
            }
            while (t < 0){
                t +=24;
            }
            while(true) {
                System.out.println(city + ": " + t + ":" + c.getTime().getMinutes() + ":" + c.getTime().getSeconds());
            }
        }
    }
}
