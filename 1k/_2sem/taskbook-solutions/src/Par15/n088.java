package Par15;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n088 {


    public static void main(String[] args) throws InterruptedException {
        int[] array = new int[]{12, 15, 3, 4, 7, 8, 9, 10, 47, 11, 28, 17, 90};
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = 0;
        MyThread2[] t = new MyThread2[n];
        for (int i = 0; i < n; i++) {
            t[i] = new MyThread2(array,k,k+array.length / n);
            k+=array.length / n;
        }
        for (MyThread2 x :
                t) {
            x.start();
            x.join();
        }
        int sum = 0;
        for (MyThread2 x :
                t) {
            sum += x.getSum();
        }
        System.out.println(sum);
    }

}

class MyThread2 extends Thread {
    int[] array;
    int sum;
    int start, finish;

    public int getSum() {
        return sum;
    }

    public MyThread2(int[] array, int start, int finish) {
        this.array = array;
        this.start = start;
        this.finish = finish;
    }

    public void run() {
        this.sum = 0;
        for (int i = start; i < finish; i++) {
            this.sum += array[i];
        }
    }
}

