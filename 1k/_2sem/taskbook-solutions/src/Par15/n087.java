package Par15;

import java.util.Arrays;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n087 {


    public static void main(String[] args) throws InterruptedException {
        int[] array = new int[]{12, 15, 3, 4, 7, 8, 9, 10, 47, 11, 28, 17, 90};
        MyThread t1 = new MyThread(array, 0, array.length / 2);
        MyThread t2 = new MyThread(array, array.length / 2, array.length);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(t1.getSum() + t2.getSum());
    }

}

class MyThread extends Thread {
    int[] array;
    int sum;
    int start, finish;

    public int getSum() {
        return sum;
    }

    public MyThread(int[] array, int start, int finish) {
        this.array = array;
        this.start = start;
        this.finish = finish;
    }

    public void run() {
        this.sum = 0;
        for (int i = start; i < finish; i++) {
            this.sum += array[i];
        }
    }
}

