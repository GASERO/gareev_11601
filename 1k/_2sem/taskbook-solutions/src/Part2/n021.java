package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n021 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        fill(ll,sc);
        int z = sc.nextInt();

        Node prev = ll.getHead();
        for (Node x = prev.getNext(); x != null; ) {
            if (x.getValue() ==z){
                prev.setNext(x.getNext());
                x =prev.getNext();
                ll.setSize(ll.getSize()-1);

            }else{
                prev = x;
                x = x.getNext();
            }

        }
        if (ll.getHead().getValue() == z){
            ll.setHead(ll.getHead().getNext());
            ll.setSize(ll.getSize()-1);
        }

        System.out.println(ll.toString());

    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
    }
}
