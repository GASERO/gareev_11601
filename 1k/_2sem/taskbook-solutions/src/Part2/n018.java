package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n018 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        fill(ll,sc);
        paste(ll,sc);
        Node z = ll.getHead();
        System.out.println(ll.toString());
    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
    }
    public static void paste(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.paste(sc.nextInt(),sc.nextInt());
        }
    }
}
