package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n024 {
    public static void main(String[] args) {
        LinkedIntList ll1 = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        fill(ll1,sc);
        LinkedIntList ll2 = new LinkedIntList();
        Node x = ll1.getHead().getNext();
        Node prev = ll1.getHead();
        int k = ll1.getSize();
        for (int i = 1; i < k; i++) {
            if (x.getValue() % 2 ==1){
                ll2.add(x.getValue());
                prev.setNext(x.getNext());
                x = x.getNext();
                ll1.setSize(ll1.getSize()-1);
            }else{
                prev = x;
                x = x.getNext();
            }
        }
        if (ll1.getHead().getValue() % 2 ==1){
            ll2.add(ll1.getHead().getValue());
            ll1.setSize(ll1.getSize()-1);
            ll1.deleteHead();
        }

        System.out.println(ll1.toString());
        System.out.println(ll2.toString());





    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
    }
}
