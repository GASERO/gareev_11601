package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n017 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        Node z = ll.getHead().getNext();
        Node prev = ll.getHead();
        for (int i = 1; i < n; i++) {
            if (isPrime(z.getValue())){
                prev.setNext(new Node(firstDigit(z.getValue()),z));
                Node y = new Node(lastDigit(z.getValue()),z.getNext());
                z.setNext(y);
                prev = y;
                if (i != n-1) {
                    z = y.getNext();
                }
            }else {
                prev = z;
                z = z.getNext();
            }
        }
       if (isPrime(ll.getHead().getValue())){
            Node l = ll.getHead();
            ll.setHead(new Node(firstDigit(l.getValue()),l));
            l.setNext(new Node(lastDigit(l.getValue()),l.getNext()));
       }
       System.out.println(ll.toString());
    }
    public static boolean isPrime(int x){
        if (x<2) return false;
        for (int i = 2; i <= Math.sqrt(x); i++) {
            if (x % i == 0) return false;

        }
        return true;
    }
    public static int firstDigit(int x){
        String s = Integer.toString(x);
        return s.charAt(0)- '0';
    }
    public static int lastDigit(int x){
        return x % 10;
    }
}
