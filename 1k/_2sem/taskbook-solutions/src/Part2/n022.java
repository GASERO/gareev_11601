package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n022 {
    public static void main(String[] args) {
        LinkedIntList ll1 = new LinkedIntList();
        LinkedIntList ll2 = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        fill(ll1,sc);
        fill(ll2,sc);
        LinkedIntList result = new LinkedIntList();

        for (int i = 0; i < ll1.getSize() + ll2.getSize(); i++) {
            if (ll1.getHead() == null){
                for (Node j = ll2.getHead(); j !=null    ; j = j.getNext()) {
                    result.add(j.getValue());
                    ll2.deleteHead();
                }
                break;
            }
            if (ll2.getHead() == null){
                for (Node j = ll1.getHead(); j !=null    ; j = j.getNext()) {
                    result.add(j.getValue());
                    ll1.deleteHead();
                }
                break;
            }
            if (ll1.getHead().getValue() < ll2.getHead().getValue()){
                result.add(ll1.getHead().getValue());
                ll1.deleteHead();
            }else{

                result.add(ll2.getHead().getValue());
                ll2.deleteHead();
            }
        }
        System.out.println(result.toString());




    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
    }
}
