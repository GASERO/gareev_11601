package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n023 {

    public static void main(String[] args) {
        LinkedIntList ll1 = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        fill(ll1,sc);
        deleteByPositions(ll1,sc);
        System.out.println(ll1.toString());

        



    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
    }
    public static void deleteByPositions(LinkedIntList ll1, Scanner sc){
        int k;
        TreeSet<Integer> ts = new TreeSet<>();
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ts.add(sc.nextInt());
        }
        Node x = ll1.getHead().getNext();
        Node prev = ll1.getHead();
        k = ll1.getSize();
        for (int i = 1; i  < k; i++) {
           if (ts.contains(i)){
               prev.setNext(x.getNext());
               x = x.getNext();
               ll1.setSize(ll1.getSize()-1);
           }else{
               prev = x;
               x = x.getNext();
           }

        }
        if (ts.contains(0)){
            ll1.deleteHead();
        }
    }
}
