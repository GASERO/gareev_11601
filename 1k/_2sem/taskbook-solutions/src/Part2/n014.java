package Part2;

/**
 * Created by Роберт on 29.05.2017.
 */
import Part1.*;

import java.util.Scanner;

public class n014 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        int x = sc.nextInt();
        Node z = ll.getHead();
        for (int i = 0; i < n; i++) {
            if (z.getValue() % 2 == 0){
                Node next = z.getNext();
                z.setNext(new Node(x,next));
                z = next;
            }else {
                z = z.getNext();
            }
        }
        System.out.println(ll.toString());
    }
}
