package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n025 {
    public static void main(String[] args) {
            LinkedIntList ll1 = new LinkedIntList();
            Scanner sc = new Scanner(System.in);
            fill(ll1,sc);
            int x = convert(ll1);
            LinkedIntList ll2 = new LinkedIntList();
            do{
                ll2.add(x % 10);
                x /= 10;
            }while(x > 0);
            ll2.reverse();
            System.out.println(ll2);





    }
    public static void fill(LinkedIntList ll, Scanner sc){
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }

    }
    public static int convert(LinkedIntList ll1){
        int x = pow(ll1.getSize()-1);
        int result = 0;
        for (Node y = ll1.getHead(); y != null; y = y.getNext()) {
            result += x * y.getValue();
            x /= 2;
        }
        return result;
    }
    public static int pow(int x){
        int result = 1;
        for (int i = 0; i < x; i++) {
            result *=2;
        }
        return result;
    }
}


