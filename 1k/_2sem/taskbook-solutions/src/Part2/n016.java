package Part2;

import Part1.LinkedIntList;
import Part1.Node;

import java.util.Scanner;

/**
 * Created by Роберт on 29.05.2017.
 */
public class n016 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        int x = sc.nextInt();
        Node z = ll.getHead();
        for (int i = 0; i < n-1; i++) {
            if (z.getNext().getValue() % 2 == 0){
                Node next = z.getNext();
                z.setNext(new Node(x,next));
                z = next;
            }else {
                z = z.getNext();
            }
        }
        if (ll.getHead().getValue() % 2 == 0){
            Node l = ll.getHead();
            ll.setHead(new Node(x,l));
        }
        System.out.println(ll.toString());
    }
}
