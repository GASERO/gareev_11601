package Part1;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n002 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        int[] a = {1, 2, 3, 4, 5};
        for (int x :
                a) {
            ll.add(x);
        }
        int x = ll.getHead().getValue();
        for (Node i = ll.getHead(); i != null ; i = i.getNext()) {
            if (i.getValue() > x) x = i.getValue();
        }
        System.out.println(x);
    }
}
