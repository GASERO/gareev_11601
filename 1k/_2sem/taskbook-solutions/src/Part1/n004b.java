package Part1;

import java.util.Scanner;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n004b {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        int pr = 1;
        int k = 1;
        for (Node i = ll.getHead(); i != null ; i = i.getNext()) {
            if (k % 2 != 0) pr *= i.getValue();
            k++;
        }
        System.out.println(pr);
    }
}
