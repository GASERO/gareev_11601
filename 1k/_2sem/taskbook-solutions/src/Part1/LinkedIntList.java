/**
 * Created by Роберт on 15.03.2017.
 */
package Part1;
public class LinkedIntList {
    private Node head;
    private Node tail;
    private int size = 0;

    public Node getHead() {
        return head;
    }

    public int getSize() {
        return size;
    }

    public void setHead(Node head) {

        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void add(int x){

        Node p = new Node(x,null);
        if (head == null){
            head = p;
            tail = p;
        }
        else{
            tail.setNext(p);
            tail = p;
        }
        size++;
    }
    public void addList(LinkedIntList x){
        tail.setNext(x.getHead());
        tail = x.getTail();
    }
    public String toString(){
        if (size==0) return "[]";
        String s = "[";
        Node i;
        for (i = head; i.hasNext() ; i = i.getNext()) {
           s = s + i.getValue() + ", ";
        }

        s = s+i.getValue() +"]";
        return s;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void reverse(){
        Node i;
        Node last;
        int k = 0;
        for (int j = 0; j < size / 2; j++) {
            i = head;
            last = head;
            for (int l = 0; l < k; l++) {
               i = i.getNext();
            }
            for (int l = 0; l < size - k -1; l++) {
                last = last.getNext();
            }
            swap(i,last);
            k++;
        }

    }
    private void swap(Node a, Node b){
        int t = a.getValue();
        a.setValue(b.getValue());
        b.setValue(t);
    }
    private Node getByNumber(int k){
        if (k > size) return null;
        Node j = head;
        for (int i = 0; i < k; i++) {
            j = j.getNext();
        }
        return j;
    }
    public void bubbleSort(){
        if (size > 1) {
            Node prev, cur;
            for (int i = 0; i < size; i++) {
                prev = head;
                cur = head.getNext();
                for (int j = 1; j < size - i; j++) {
                    if (prev.getValue() > cur.getValue()){
                        swap(prev,cur);
                    }
                    prev = prev.getNext();
                    cur = cur.getNext();
                }
            }
        }
    }

    public void selectionSort(){
        Node k;
        Node l;
        for (int i = size -1; i >0; i--) {
            for (int j = 0; j < i; j++) {
                k = findMax(i);
                l =getByNumber(i);
                if (k.getValue() > l.getValue()){
                    swap(k,l);
                }
            }
        }
    }
    private Node findMax(int finish){
        Node p = head;
        Node max = p;
        for (int i = 0; i < finish; i++) {
            if (max.getValue() < p.getValue()){
                max = p;
            }
            p = p.getNext();
        }
        return max;
    }
    public void shiftOnKPositionR(int k){
        for (int i = 0; i < k; i++) {
            shiftOn1PositionR();
        }
    }
    private void shiftOn1PositionR(){
        if (size > 1) {
            Node cur = head.getNext();
            int val2,val1 = head.getValue();
            for (int i = 1; i < size - 1; i++) {
                val2 = cur.getValue();
                cur.setValue(val1);
                cur = cur.getNext();
                val1 = val2;
            }
            val2 = cur.getValue();
            cur.setValue(val1);
            head.setValue(val2);

        }
    }
    public void shitOnKPositionL(int k){
        for (int i = 0; i < k; i++) {
            shiftOn1PositionL();
        }
    }

    private void shiftOn1PositionL() {
        Node node = head;
        tail.setNext(node);
        head = head.getNext();
        node.setNext(null);
        tail = node;
    }

    public void paste(int i, int k) {
        Node x = getHead();
        if (i == 0) {
            head = new Node(k, head);
            return;
        }
        if (i > size) {
            Node y = new Node(k,null);
            tail.setNext(y);
            tail = y;
            return;
        }
        for (int j = 0; j < i-1; j++) {
            x = x.getNext();
        }
        x.setNext(new Node(k,x.getNext()));

    }

    public void deleteLastThreeElements() {
        if (size < 4){
            head = null;
            size = 0;
            return;
        }
        Node x;
        for ( x = head;  x.getNext().getNext().getNext().getNext() != null ; x = x.getNext()) {

        }
        size-=3;
        x.setNext(null);
    }

    public void deleteHead() {
        head = head.getNext();
    }
}
