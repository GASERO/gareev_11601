package Part1;

import java.util.Scanner;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n004 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        int sum = 0;
        for (Node i = ll.getHead(); i != null ; i = i.getNext()) {
            if (i.getValue() % 2 == 0) sum += i.getValue();
        }
        System.out.println(sum);
    }
}
