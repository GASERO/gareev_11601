package Part1;

import Part1.LinkedIntList;

import java.util.Scanner;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n004a {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        boolean nullIsExists = false;
        for (Node i = ll.getHead(); i != null && !nullIsExists; i = i.getNext()) {
            if (i.getValue() == 0) nullIsExists = true;
        }
        System.out.println(nullIsExists);
    }
}
