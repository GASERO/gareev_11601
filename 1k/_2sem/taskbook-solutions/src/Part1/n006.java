import Part1.LinkedIntList;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n006 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        int[] a = {1, 2, 3, 4, 5};
        for (int x :
                a) {
            ll.add(x);
        }
        int x = ll.getHead().getValue();
        ll.getHead().setValue(ll.getTail().getValue());
        ll.getTail().setValue(x);
    }
}
