package Part1;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n003 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        int[] a = {1, 2, 3, 4, 5};
        for (int x :
                a) {
            ll.add(x);
        }
        int sum = 0;
        int pr = 1;
        for (Node i = ll.getHead(); i != null ; i = i.getNext()) {
            sum += i.getValue();
            pr *= i.getValue();
        }
        System.out.println(sum);
        System.out.println(pr);
    }
}
