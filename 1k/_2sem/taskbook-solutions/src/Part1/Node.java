/**
 * Created by Роберт on 15.03.2017.
 */
package Part1;
public class Node {
    private int value;
    private Node next;

    public Node(int value, Node next) {
        this.value = value;
        this.next = next;
    }

    public boolean hasNext(){
        if (next == null) return false;
        return true;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public int getValue() {

        return value;
    }

    public Node getNext() {
        return next;
    }
}
