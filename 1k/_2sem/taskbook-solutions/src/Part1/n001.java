package Part1;

/**
 * Created by Роберт on 15.03.2017.
 */
public class n001 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        int[] a = {1, 2, 3, 4, 5};
        for (int x :
                a) {
            ll.add(x);
        }
        for (Node i = ll.getHead(); i != null ; i = i.getNext()) {
            System.out.println(i.getValue());
        }
    }
}
