package Part1;

import Part1.LinkedIntList;

import java.util.Scanner;

/**
 * Created by Роберт on 24.05.2017.
 */
public class n012 {
    public static void main(String[] args) {
        LinkedIntList ll = new LinkedIntList();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            ll.add(sc.nextInt());
        }
        n = sc.nextInt();
        System.out.println(ll.toString());
        ll.shiftOnKPositionR(n);
        System.out.println(ll.toString());
    }
}
