public class Task01{
	public static void main(String[] args){
		boolean k = false;
		int n;
		for (int i = 0; i < args.length && !k; i++){
			n = Integer.parseInt(args[i]);
			if (n  > 0){
				k = true;
			}
		}
		if (k) {
			System.out.print("+");
		} 
		else {
			System.out.print("-");
		}
	}
}