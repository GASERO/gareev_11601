public class Task02{
	public static void main(String[] args){
		boolean k = false;
		int n;
		int digit;
		for (int i = 0; i < args.length && !k; i++){
			n = Integer.parseInt(args[i]);
			while ((n > 0)&&!k) {
				digit = n % 10;
				if (digit % 2 != 0 ){
					k = true;
				}
				n = n / 10;
			}
		}
		if (k) {
			System.out.print("-");
		} 
		else {
			System.out.print("+");
		}
		
	}
}