/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 83 a 
*/
import  java.util.Scanner;
public class PS1n83a{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		double a = scan.nextDouble();
		double result = 1;
		int k = 2;
		while (result <= a){
			result +=(double) (1.0 / k);
			k++;
		}
		System.out.print(result);
	}
}