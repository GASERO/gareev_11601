/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 d
*/
import  java.util.Scanner;
public class PS1n77d{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		double result = 0,b =0;
		for (int i = 1; i <= n; i++){
			result = Math.sqrt(result + 2);
		}
		System.out.print(result);
	}
}