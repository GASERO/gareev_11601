/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 b
*/
import  java.util.Scanner;
public class PS1n77b{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int result = 1;
		for (int i = 1; i <= n; i++){
			result *= i;
		}
		System.out.print(result);
	}
}