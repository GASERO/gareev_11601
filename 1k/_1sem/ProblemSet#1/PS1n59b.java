/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 59 b
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n59b{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		if (( x * x + y * y <= 1) &&(x * x + y * y >= 0.5)){
			System.out.print("+");
		}
		else{
			System.out.print("-");
		}
		
		
		
		
	}
}