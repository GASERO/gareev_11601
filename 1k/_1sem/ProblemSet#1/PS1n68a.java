/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 68 a
*/
import  java.util.Scanner;
public class PS1n68a{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int a = n, b = 0,i = 1, digit;
		while (n > 0) {
			digit = n % 10;
			b += digit *i;
			i *= 10;
			n /= 10;
		}
		if (a==b) System.out.print("+");
		else System.out.print("-");
		
	}
}