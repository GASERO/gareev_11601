/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 16
*/
import java.util.Scanner;
public class PS1n16{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("length = ");
		double l = scan.nextDouble();
		double s = l * l / 4 / Math.PI;
		System.out.print("S = ");
		System.out.print(s);
	}
}