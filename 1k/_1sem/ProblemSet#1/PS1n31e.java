/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 31 e
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n31e{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("a = ");
		double a = scan.nextDouble();
		double b = a * a ;
		a = b * a;
		a = a * b ;
		a = a * a;
		System.out.print(a);
		
		
		
	}
}