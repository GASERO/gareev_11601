/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 84 b
*/
import  java.util.Scanner;
public class PS1n84b{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("n = ");
		double n = scan.nextDouble();
		x = Math.sin(x);
		double result = 0, b = x;
		for (int i = 1; i <= n; i++){
			result += Math.sin(b);
			b = b * x;
			
		}
		System.out.print(result);
	}
}