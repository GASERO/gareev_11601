/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 67 d
*/
import  java.util.Scanner;
public class PS1n67d{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int digit = n / 10;
		if (digit == 10) digit = 0;
		System.out.print("predposlednee chislo = "+ digit);
	}
}