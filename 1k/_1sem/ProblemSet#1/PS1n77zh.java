/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 zh
*/
import  java.util.Scanner;
public class PS1n77zh{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		double result = 0,b =0;
		for (int i = n; i >= 1; i--){
			result = Math.sqrt(result+3*i) ;
		}
		System.out.print(result);
	}
}