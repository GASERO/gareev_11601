/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 33 a
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n33a{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		double max = x > y ? x : y;
		System.out.print(max);
		
		
		
		
	}
}