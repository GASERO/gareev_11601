/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 68 v
*/
import  java.util.Scanner;
public class PS1n68v{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt(),digit;
		int [] a = new int[10];
		while (n > 0){
			digit = n % 10;
			a[digit]++;		
			n /= 10;			
		}
		boolean y = true;
		for (int i = 0; i < 10; i++){
			if ((a[i] != 1)&&(a[i] != 0)){
				y = false;
				break;
			}
		}
		if (y) System.out.print("+");
		else System.out.print("-");
	}
}