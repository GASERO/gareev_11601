/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 60 e
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n60e{
	public static void main(String[] args){
		double result = 1;
		for (int i = 1; i <= 52; i++){
			result = result * i * i / (i *i + 2 * i + 3);
		}
		System.out.print(result);
		
		
	}
}