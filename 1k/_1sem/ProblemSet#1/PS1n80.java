/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 80
*/
import  java.util.Scanner;
public class PS1n80{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		int fact = 6;
		int b = a * a * a;
		double result = a;
		byte k = -1;
		for (int i = 1; i <= 6; i++){
			result += (double) k * b / fact;
			fact = fact * (i+1) * (i+2);
			k *= -1;
			b = b *a *a;
		}
		System.out.print(result);
	}
}