/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 79
*/
import  java.util.Scanner;
public class PS1n79{
	public static void main(String[] args){
		double b = 0.1;
		double result =1;
		while (b < 10){
			result *= (1+ Math.sin(b * Math.PI / 180));
			b += 0.1;
		}
		System.out.print(result);
	}
}