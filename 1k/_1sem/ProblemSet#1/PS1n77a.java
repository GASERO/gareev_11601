/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 a
*/
import  java.util.Scanner;
public class PS1n77a{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int result = 1;
		for (int i = 1; i <= n; i++){
			result *= 2;
		}
		System.out.print(result);
	}
}