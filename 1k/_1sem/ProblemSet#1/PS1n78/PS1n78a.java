/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 78 a
*/
import  java.util.Scanner;
public class PS1n78a{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		System.out.print("n = ");
		int n = scan.nextInt();
		int result = 1;
		for (int i = 1; i <= n; i++){
			result *=a;
		}
		System.out.print(result);
	}
}