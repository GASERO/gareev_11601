/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 78 b
*/
import  java.util.Scanner;
public class PS1n78b{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		System.out.print("n = ");
		int n = scan.nextInt();
		int result = a;
		for (int i = 1; i < n; i++){
			result *=(a + i);
		}
		System.out.print(result);
	}
}