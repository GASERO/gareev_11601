/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 78 g
*/
import  java.util.Scanner;
public class PS1n78g{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		System.out.print("n = ");
		int n = scan.nextInt();
		double result = a;
		for (int i = 1; i <= n; i++){
			result *=(a - n * i);
		}
		System.out.print(result);
	}
}