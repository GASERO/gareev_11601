/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 59 g
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n59g{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		if (Math.abs(x)+ Math.abs(y) <= 1){
			System.out.print("+");
		}
		else{
			System.out.print("-");
		}
		
		
		
		
	}
}