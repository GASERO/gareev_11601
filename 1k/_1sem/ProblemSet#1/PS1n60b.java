/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 60 b
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n60b{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		double u;
		if ((y <= x/2.0) && ( x * x + y * y <= 1)){
			u = -3;
		} 
		else {
			u = y * y;
		}
		System.out.print(u);
		
		
		
	}
}