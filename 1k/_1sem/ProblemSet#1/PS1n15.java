/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 15
*/
import java.util.Scanner;
public class PS1n15{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("a = ");
		double a = scan.nextDouble();
		System.out.print("c = ");
		double c = scan.nextDouble();
		if (a > c) {
			System.out.print("Incorrect data");
			
		}
		else{
		double b = Math.sqrt(c * c - a * a);
		System.out.println("b = " + b);
		double r = (a + b - c) / 2;
		System.out.print("r = "+ r);
		}
	}
}