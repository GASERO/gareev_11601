/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 59 e
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n59e{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		if ((x <= 0) && (Math.abs(y)+ 0.5 * Math.abs(x) <= 1)){
			System.out.print("+");
		} 
		else {
			if ((x >= 0) && ( x * x + y * y <= 1)){
				System.out.print("+");
			}
			else System.out.print("-");
		}
		
		
		
		
	}
}