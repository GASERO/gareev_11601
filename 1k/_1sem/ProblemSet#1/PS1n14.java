/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 14
*/
import java.util.Scanner;
public class PS1n14{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Mass 1 =  ");
		double m1 = scan.nextDouble();
		System.out.print("Mass 2 =  ");
		double m2 =scan.nextDouble();
		System.out.print("R =  ");
		double r = scan.nextDouble();
		
		final double G = 6.67e-11;
		double f = G * m1 * m2 / r / r;
		System.out.print("F = ");
		System.out.print(f);
	}
}