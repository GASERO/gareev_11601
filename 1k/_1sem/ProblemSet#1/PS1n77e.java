/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 e
*/
import  java.util.Scanner;
public class PS1n77e{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		double result = 0,b =0,c=0;
		for (int i = 1; i <= n; i++){
			b += Math.sin(i * Math.PI / 180);
			c += Math.cos(i * Math.PI / 180);
			result += c/b;
		}
		System.out.print(result);
	}
}