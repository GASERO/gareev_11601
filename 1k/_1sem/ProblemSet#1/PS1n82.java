/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 81



ATTENTION previous PS1n81 = PS1n82; current number = PS1n81;



*/
import  java.util.Scanner;
public class PS1n82{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("a = ");
		int a = scan.nextInt();
		System.out.print("x = ");
		int x = scan.nextInt();
		System.out.print("n = ");
		int n = scan.nextInt();
		double result = x;
		for (int i = 1; i <= n; i++){
			result = (result + a) * (result + a);
		}
		System.out.print(result);
	}
}