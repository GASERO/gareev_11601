/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 114 z
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n114z{
	public static void main(String[] args){
		double result = 1;
		int fact = 2;
		for (int i = 2; i <= 10; i++){
			result = result * (1 - 1.0 / fact) *(1 - 1.0 / fact);
			fact *= i;
		}
		System.out.print(result);
		
		
	}
}