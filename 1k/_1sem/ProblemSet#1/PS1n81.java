/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 81
*/
import  java.util.Scanner;
public class PS1n81{
	public static void main(String[] args){
		System.out.print("a = ");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		double result = 1;
		for (int i = 1; i <=32 ; i++){
			result *= (a - 2 * i);
		}
		for (int i = 1; i <=32 ; i++){
			result = (double) result / (a - 2 * i + 1);
		}
		System.out.print(result);
	}
}