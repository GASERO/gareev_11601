/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 17
*/
import java.util.Scanner;
public class PS1n17{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("R = ");
		double r = scan.nextDouble();
		double s = 400 * Math.PI - r * r * Math.PI;
		System.out.print("S = "+s);
	}
}