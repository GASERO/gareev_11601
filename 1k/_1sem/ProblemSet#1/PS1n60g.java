/*
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 60 g
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n60g{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		double u = 0;
		if ((x <= 0) && (x * x + y * y <= 1)){
			u = x * x -1;
		} 
		else {
			if ((x >= 0) && ( x * x + y * y <= 1) && ( x * x + y * y >= 0.3)){
				u = x * x -1;
			}
			else u = Math.sqrt(Math.sqrt(x - 1));
		}
		System.out.print(u);
		
		
		
		
	}
}