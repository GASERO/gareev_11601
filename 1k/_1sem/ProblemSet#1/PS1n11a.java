/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 11 a
*/
import  java.util.Scanner;
import static java.lang.Math.*;
public class PS1n11a{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		System.out.print("y = ");
		double y = scan.nextDouble();
		System.out.print("z = ");
		double z = scan.nextDouble();
		double a = (sqrt(abs(x - 1))- pow(abs(y), 1.0/3 )) / (1 + x * x / 2 + y * y / 4);
		System.out.print("a = " + a);
		double b = x * (atan(z) + pow(exp(1), -x -3));
		System.out.print("b = " + b);
		
		
	}
}