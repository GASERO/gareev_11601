/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 13
*/
import java.util.Scanner;
public class PS1n13{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.print("Length = ");
		double l = scan.nextDouble();
		final double g = 9.8;
		double t = 2 * Math.PI * Math.sqrt(l / g);
		System.out.print("T = ");
		System.out.print(t);
	}
}