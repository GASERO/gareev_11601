/**
* @author Gareev Robert
* 11-601
* Problem Set # 1 number 77 v
*/
import  java.util.Scanner;
public class PS1n77v{
	public static void main(String[] args){
		System.out.print("n = ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		double result = 1;
		for (int i = 1; i <= n; i++){
			result *= (1 + 1.0 / i / i);
		}
		System.out.print(result);
	}
}