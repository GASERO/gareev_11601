public class exp{
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		final double EPS =1e-6;
		double xn = x;
		int n = 1;
		double y;
		double result = 1;
		long fact = 1;
		do{
			y = result;
			result += xn / fact;
			n += 1;
			xn *= x;
			fact *= n;
			System.out.println(result);
			
		}
		while (Math.abs(result-y)>EPS);
		System.out.print(result);
	}
}