public class ln{
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		final double EPS =1e-6;
		double xn = x * x;
		int n = 2;
		int k = -1;
		double y;
		double c = x;
		do{
			y = x;
			x += k * xn / n ;
			n += 1;
			xn *= c;
			k *= -1;
			System.out.println(x);
			
		}
		while (Math.abs(x-y)>EPS);
		System.out.print(x);
	}
}