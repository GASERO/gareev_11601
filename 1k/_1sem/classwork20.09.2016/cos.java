public class cos{
	public static void main (String[] args){
		double x = Double.parseDouble(args[0]);
		double c = x;
		final double EPS =1e-6;
		int n = 1;
		int k = -1;
		double y;
		double b = x * x ;
		double fact = 2;
		x = 1;
		do{
			y = x;
			x += k * b / fact;
			fact = fact * (n+1) * (n+2);
			n += 2;
			b *= c * c;
			k *= -1;
			System.out.println(x);
			
		}
		while (Math.abs(x-y)>EPS);
		System.out.print(x);
	}
}