import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x;
        boolean k = false;
        do{
            x = sc.nextInt();
            if (x == -1) break;
            k = check(x);
            if (!k) break;
        }while(k);
        if (k) System.out.println("+");
        else System.out.println("-");
    }
    public static boolean check(int x){
        while (x > 0){
            if ((x % 10) % 3 == 0) return  true;
            x /=10;
        }
        return false;
    }
}
