public class Task04{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		int k = n / 2;
		int z = n;
		for (int i = 1; i <= n / 2 + 1; i++){
			for (int j = 1; j <= k; j++){
				System.out.print("  ");
			}
			for (int j = k + 1; j <= z; j++ ){
				System.out.print("1 ");
			}
			for (int j = z + 1; j <= n; j++ ){
				System.out.print("  ");
			}
			System.out.println();
			z--;
			k--;
		}
	}
}