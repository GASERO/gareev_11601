import java.util.Scanner;

/**
 * Created by Роберт on 12.01.2017.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        int i = 0;
        int number1 = 0;
        int number2 = 0;
        while (a.charAt(i) != '+'){
            number1 = number1 * 10 + a.charAt(i) - '0';
            i++;
        }
        i++;
        while (i < a.length()){
            number2 = number2 * 10 + a.charAt(i) - '0';
            i++;
        }
        System.out.println(number1);
        System.out.println(number2);
        System.out.println("Summa:");
        System.out.println(number1+number2);
    }
}
