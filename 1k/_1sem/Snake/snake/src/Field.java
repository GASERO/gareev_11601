/**
 * Created by Роберт on 27.11.2016.
 */
import java.util.Random;
public class Field {
    Random rnd = new Random();
    final static int FSIZE = 20;
    Cell [][] a = new Cell[FSIZE][FSIZE];
    public void render(){
        for (int i = 0; i < FSIZE; i++){
            for (int j =0; j < FSIZE; j++){
                if (a[i][j] == null) System.out.println("..");
                else a[i][j].print();
            }
        }
    }
    public void spawn(){
        int i,j;
        do {
            i = rnd.nextInt(FSIZE);
            j = rnd.nextInt(FSIZE);
        } while (a[i][j] != null);
        a[i][j] = new Food(j,i);
    }
}
