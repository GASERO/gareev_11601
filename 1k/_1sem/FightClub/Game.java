import java.util.Scanner;
public class Game{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		player p1 = new player();
		player p2 = new player();
		int dmg;
		while (1 > 0){
			p1.printStats();
			p2.printStats();
			System.out.println(p1.name + " turns to attack");
			dmg = p1.punch(sc.nextInt());
			if (dmg == 0){
				System.out.println("missed");
			}
			else {
				p2.minusHP(dmg); //p2.minusHP(dmg)
				p2.printStats();
			}
			if (p2.getHP() < 1) {
				System.out.print(p1.name + "wins!");
				break;
			}
			System.out.println();
			System.out.println(p2.name + " turns to attack");
			dmg = p2.punch(sc.nextInt());
			if (dmg == 0){
				System.out.println("missed");
			}
			else {
				p1.minusHP(dmg);
				p1.printStats();
			}
			System.out.println("__________________");
			System.out.println();
			if (p1.getHP() < 1) {
				System.out.print(p1.name + "wins!");
				break;
			}
		}
	}
	//
}
