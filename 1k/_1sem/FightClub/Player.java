import java.util.Scanner;
public class Player{
	private int hp;
	String name;
	player(){
		hp = 20;
		setName();
	}
	public int punch(int x){
		double chance = 0.99 * Math.pow(2.0 / 2.5 , x - 1 );
		if (Math.random() <= chance){
			return x;
		}
		else {
			return 0;
		}
	}
	public int getHP(){
		return hp;
	}
	public void setName(){
		System.out.print("Type player's name ");
		Scanner sc = new Scanner(System.in);
		this.name = sc.nextLine();
	}
	public void printStats(){
		System.out.println(name + " "+ hp);
	}
	public void minusHP( int dmg){
		this.hp -= dmg;
		System.out.println("BAM!");
	}
	//
}