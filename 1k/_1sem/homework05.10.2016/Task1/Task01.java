import	java.io.File; 
import	java.util.Scanner;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
public class Task01{
	public static void main(String[] args) throws FileNotFoundException{
		Scanner scan = new Scanner(new File("input.txt"));
		PrintWriter pw = new PrintWriter("output.txt");
		int x,s = 0;
		do {
		x = scan.nextInt();
		s += x;
		}
		while (scan.hasNextInt());
		pw.print(s);
		pw.close();
		
	}
}