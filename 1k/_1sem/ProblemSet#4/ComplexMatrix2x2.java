public class ComplexMatrix2x2{
	public static void main(String[] args){
		ComplexNumber a1 = new ComplexNumber(2,3);
		ComplexNumber a2 = new ComplexNumber(5,3);
		ComplexNumber a3 = new ComplexNumber(7,13);
		ComplexNumber a4 = new ComplexNumber(7,3);
		ComplexMatrix2x2 mat1 = new ComplexMatrix2x2(a3,a4,a1,a2);
		ComplexMatrix2x2 mat2 = new ComplexMatrix2x2(a4,a1,a2,a3);
		ComplexMatrix2x2 mat0= new ComplexMatrix2x2();
		ComplexMatrix2x2 mat4;
		boolean x,y;
		System.out.println(mat1.toString());
		System.out.println(mat2.toString());
		System.out.println(mat0.toString());
		
		System.out.println("det mat1 " + mat1.det());
		System.out.println("mult vect \n"+ mat1.multVector(new ComplexVector2D(a1,a4)));
		
		mat0 = mat1.add(mat2);
		System.out.println("add \n" + mat0.toString() );
		mat0 = mat1.mult(mat2);
		System.out.println("mult \n" + mat0.toString());

		
		
		
		
		
	}
	private ComplexNumber [] [] a = new ComplexNumber [3] [3];
	public void assignment(ComplexNumber [] [] x){
		this.a[1][1] = x[1][1];
		this.a[1][2] = x[1][2];
		this.a[2][1] = x[2][1];
		this.a[2][2] = x[2][2];
	}
	public ComplexNumber [][] assignmentX(ComplexNumber x, ComplexNumber y, ComplexNumber z, ComplexNumber u){
		a[1][1] = x;
		a[1][2] = y;
		a[2][1] = z;
		a[2][2] = u;
		return a;
	}
	ComplexMatrix2x2(){
		ComplexNumber [][] x = new ComplexNumber [3][3];
		x = assignmentX( new ComplexNumber(),new ComplexNumber(),new ComplexNumber(),new ComplexNumber());
		assignment(x);
	}
	ComplexMatrix2x2(ComplexNumber a){
		ComplexNumber [][] x = new ComplexNumber [3][3];
		x = assignmentX(a,a,a,a);
		assignment(x);
	}
	ComplexMatrix2x2(ComplexNumber x, ComplexNumber y, ComplexNumber z, ComplexNumber u){
		ComplexNumber [][] r = new ComplexNumber [3][3];
		r = assignmentX(x,y,z,u);
		assignment(r);
	}
	
	public ComplexMatrix2x2 add(ComplexMatrix2x2 mat){
		ComplexMatrix2x2 mat2 = new ComplexMatrix2x2(this.a[1][1].add(mat.a[1][1]),this.a[1][2].add(mat.a[1][2]),this.a[2][1].add(mat.a[2][1]),this.a[2][2].add(mat.a[2][2]));
		return mat2;
	}
	

	public ComplexMatrix2x2 mult(ComplexMatrix2x2 mat){
		ComplexNumber a1,a2,a3,a4;
		a1 = this.a[1][1].mult(mat.a[1][1]).add(this.a[1][2].mult(mat.a[2][1]));
		a2 = this.a[1][2].mult(mat.a[1][2]).add(this.a[1][2].mult(mat.a[2][2]));
		a3 = this.a[2][1].mult(mat.a[1][1]).add(this.a[2][2].mult(mat.a[2][1]));
		a4 = this.a[2][2].mult(mat.a[1][2]).add(this.a[2][2].mult(mat.a[2][2]));
		ComplexMatrix2x2 mat2 = new ComplexMatrix2x2(a1,a2,a3,a4);
		return mat2;
	}

	public ComplexNumber det(){
		return (this.a[1][1].add(this.a[2][2])).sub(this.a[2][1].mult(this.a[1][2]));
	}
	

	
	public ComplexVector2D multVector(ComplexVector2D vect){
		ComplexVector2D vect2 = new ComplexVector2D(this.a[1][1].mult(vect.getX()).add(this.a[1][2].mult(vect.getY())),this.a[2][1].mult(vect.getX()).add(this.a[2][2].mult(vect.getY())));
		return vect2;
	}
	public String toString(){
		return a[1][1] + " " + a[1][2] + "\n" + a[2][1] + " " + a[2][2] + "\n" + "___________";
	}
}