public class Vector2D{
	public static void main(String[] args){
		Vector2D vect1 = new Vector2D(3,4);
		Vector2D vect2 = new Vector2D(4,3);
		Vector2D vect0= new Vector2D();
		boolean x,y;
		System.out.println(vect1.toString());
		System.out.println(vect2.toString());
		System.out.println(vect0.toString());
		
		vect0 = vect1.add(vect2);
		System.out.println("add " + vect0.toString() );
		vect0.add2(vect1);
		System.out.println("add2 " + vect0.toString() );
		vect0 = vect1.sub(vect2);
		System.out.println("sub " + vect0.toString());
		vect0.sub2(vect1);
		System.out.println("sub2 " + vect0.toString());
		vect0 = vect1.mult(2);
		System.out.println("mult " + vect0.toString());
		vect0.mult2(2);
		System.out.println("mult2 " + vect0.toString());
		System.out.println("length" + vect1.length());
		System.out.println("cos " + vect1.cos(vect2));
		System.out.println("scalarProduct" + vect1.scalarProduct(vect2));
		System.out.println( x = vect1.equals(vect2));
		System.out.println( y = vect1.equals(vect1));
		
		
		
		
		
	}
	private double x,y;
	public void assignment(double x, double y){
		this.x = x;
		this.y = y;
	}
	Vector2D(double x, double y){
		assignment(x,y);
	}
	Vector2D(){
		assignment(0,0);
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public void setX(double x){
		this.x = x;	
	}
	public void setY(double y){
		this.y = y;	
	}
	public Vector2D add(Vector2D vect){
		Vector2D sum = new Vector2D(this.x + vect.getX(), this.y + vect.getY());
		return sum;
	}
	public void add2(Vector2D vect){
		setX(this.x + vect.getX());
		setY(this.y + vect.getY());
	}
	public Vector2D sub(Vector2D vect){
		Vector2D sub = new Vector2D(this.x - vect.getX(), this.y - vect.getY());
		return sub;
	}
	public void sub2(Vector2D vect){
		setX(this.x - vect.getX());
		setY(this.y - vect.getY());
	}
	public Vector2D mult(double x){
		Vector2D sub = new Vector2D(this.x * x, this.y * x);
		return sub;
	}
	public void mult2(double x){
		setX(this.x * x);
		setY(this.y * x);
	}
	public String toString(){
		return "(" + this.x + ";" + this.y + ")";
	}
	public double length(){
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	public double scalarProduct(Vector2D vect){
		return this.x * vect.getX() + this.y * vect.getY();
	}
	public double cos(Vector2D vect){
		return scalarProduct(vect) / (length() * vect.length());
	}
	public boolean equals(Vector2D vect){
		if (this.x == vect.getX() && this.y == vect.getY()) return true;
		return false;
	}
}
