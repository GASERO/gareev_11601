public class Matrix2x2{
	public static void main(String[] args){
		Matrix2x2 mat1 = new Matrix2x2(3,4,1,2);
		Matrix2x2 mat2 = new Matrix2x2(4,3,7,13);
		Matrix2x2 mat0= new Matrix2x2();
		Matrix2x2 mat4;
		boolean x,y;
		System.out.println(mat1.toString());
		System.out.println(mat2.toString());
		System.out.println(mat0.toString());
		mat4 = mat1.inverseMatrix();
		System.out.println("inverse \n"+ mat1.inverseMatrix().toString());
		System.out.println("det +"+ mat1.det());
		mat1.transpon();
		System.out.println("transpon \n"+ mat1.toString());
		System.out.println("equal diag \n"+ mat1.equivalentDiagonal());
		System.out.println("mult vect \n"+ mat1.multVector(new Vector2D(1,1)));
		
		mat0 = mat1.add(mat2);
		System.out.println("add \n" + mat0.toString() );
		mat0.add2(mat1);
		System.out.println("add2 \n" + mat0.toString() );
		mat0 = mat1.sub(mat2);
		System.out.println("sub \n" + mat0.toString());
		mat0.sub2(mat1);
		System.out.println("sub2 \n" + mat0.toString());
		mat0 = mat1.mult(mat2);
		System.out.println("mult \n" + mat0.toString());
		mat0.mult2(mat1);
		System.out.println("mult2 \n" + mat0.toString());
		System.out.println( x = mat1.equals(mat2));
		System.out.println( y = mat1.equals(mat1));
		
		
		
		
		
	}
	private double [] [] a = new double [3] [3];
	public void assignment(double [] [] x){
		this.a[1][1] = x[1][1];
		this.a[1][2] = x[1][2];
		this.a[2][1] = x[2][1];
		this.a[2][2] = x[2][2];
	}
	public double [][] assignmentX(double x, double y, double z, double u){
		a[1][1] = x;
		a[1][2] = y;
		a[2][1] = z;
		a[2][2] = u;
		return a;
	}
	Matrix2x2(){
		double [][] x = new double [3][3];
		x = assignmentX(0,0,0,0);
		assignment(x);
	}
	Matrix2x2(double a){
		double [][] x = new double [3][3];
		x = assignmentX(a,a,a,a);
		assignment(x);
	}
	Matrix2x2(double [] [] x){
		assignment(x);
	}
	Matrix2x2(double x, double y, double z, double u){
		double [][] r = new double [3][3];
		r = assignmentX(x,y,z,u);
		assignment(r);
	}
	
	public Matrix2x2 add(Matrix2x2 mat){
		Matrix2x2 mat2 = new Matrix2x2(this.a[1][1] + mat.a[1][1],this.a[1][2] + mat.a[1][2],this.a[2][1] + mat.a[2][1],this.a[2][2] + mat.a[2][2]);
		return mat2;
	}
	public void add2(Matrix2x2 mat){
		this.a[1][1] += mat.a[1][1];
		this.a[1][2] += mat.a[1][2];
		this.a[2][1] += mat.a[2][1];
		this.a[2][2] += mat.a[2][2];
	}
	public Matrix2x2 sub(Matrix2x2 mat){
		Matrix2x2 mat2 = new Matrix2x2(this.a[1][1] - mat.a[1][1],this.a[1][2] - mat.a[1][2],this.a[2][1] - mat.a[2][1],this.a[2][2] - mat.a[2][2]);
		return mat2;
	}
	public void sub2(Matrix2x2 mat){
		this.a[1][1] -= mat.a[1][1];
		this.a[1][2] -= mat.a[1][2];
		this.a[2][1] -= mat.a[2][1];
		this.a[2][2] -= mat.a[2][2];
	}
	public Matrix2x2 multNumber(double x){
		Matrix2x2 mat2 = new Matrix2x2(this.a[1][1] *x,this.a[1][2] *x,this.a[2][1] *x,this.a[2][2] *x);
		return mat2;
	}
	public void multNumber2(double x){
		this.a[1][1] *= x;
		this.a[1][2] *= x;
		this.a[2][1] *= x;
		this.a[2][2] *= x;
	}
	public Matrix2x2 mult(Matrix2x2 mat){
		double a1,a2,a3,a4;
		a1 = this.a[1][1] * mat.a[1][1] + this.a[1][2] * mat.a[2][1];
		a2 = this.a[1][2] * mat.a[1][2] + this.a[1][2] * mat.a[2][2];
		a3 = this.a[2][1] * mat.a[1][1] + this.a[2][2] * mat.a[2][1];
		a4 = this.a[2][2] * mat.a[1][2] + this.a[2][2] * mat.a[2][2];
		Matrix2x2 mat2 = new Matrix2x2(a1,a2,a3,a4);
		return mat2;
	}
	public void mult2(Matrix2x2 mat){
		double a1,a2,a3,a4;
		a1 = this.a[1][1] * mat.a[1][1] + this.a[1][2] * mat.a[2][1];
		a2 = this.a[1][2] * mat.a[1][2] + this.a[1][2] * mat.a[2][2];
		a3 = this.a[2][1] * mat.a[1][1] + this.a[2][2] * mat.a[2][1];
		a4 = this.a[2][2] * mat.a[1][2] + this.a[2][2] * mat.a[2][2];
		this.a[1][1] = a1;
		this.a[1][2] = a2;
		this.a[2][1] = a3;
		this.a[2][2] = a4;
		
	}
	public double det(){
		return this.a[1][1] * this.a[2][2] - this.a[2][1] * this.a[1][2];
	}
	public void transpon(){
		double x = this.a[1][2];
		this.a[1][2] = this.a[2][1];
		this.a[2][1] = x;
	}
	public Matrix2x2 inverseMatrix(){
		if (det() == 0 ){
			System.out.print("Error ");
			Matrix2x2 mat = new Matrix2x2();
			return mat;
		}
		else {
			double a1,a2,a3,a4;
			double det = det();
			a1 = this.a[1][1] / det;
			a2 = this.a[1][2] / det;
			a3 = this.a[2][1] / det;
			a4 = this.a[2][2] / det;
			Matrix2x2 mat = new Matrix2x2(a1,a2,a3,a4);
			//mat.transpon();
			return mat;
		}
	}
	
	public Matrix2x2 equivalentDiagonal(){
		double a1,a4,a3,a2;
		a4 = this.a[1][1] * this.a[2][2] - this.a[1][2] * this.a[2][1];
		a1 = this.a[1][1] * a4;
		a2 = a3 = 0;
		Matrix2x2 mat = new Matrix2x2(a1,a2,a3,a4);
		return mat;
	}
	
	public Vector2D multVector(Vector2D vect){
		Vector2D vect2 = new Vector2D(this.a[1][1] * vect.getX() + this.a[1][2] * vect.getY(),this.a[2][1] * vect.getX() + this.a[2][2] * vect.getY());
		return vect2;
	}
	public String toString(){
		return a[1][1] + " " + a[1][2] + "\n" + a[2][1] + " " + a[2][2] + "\n" + "___________";
	}
}