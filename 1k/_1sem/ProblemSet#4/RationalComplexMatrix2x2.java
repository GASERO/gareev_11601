public class RationalComplexMatrix2x2{
	public static void main(String[] args){
		RationalComplexNumber a1 = new RationalComplexNumber(new RationalFraction(3, 1),new RationalFraction(1, 15));
		RationalComplexNumber a2 = new RationalComplexNumber(new RationalFraction(5, 17),new RationalFraction(23, 31));
		RationalComplexNumber a3 = new RationalComplexNumber(new RationalFraction(7, 18),new RationalFraction(14, 19));
		RationalComplexNumber a4 = new RationalComplexNumber(new RationalFraction(13, 11),new RationalFraction(3, 41));
		RationalComplexMatrix2x2 mat1 = new RationalComplexMatrix2x2(a3,a4,a1,a2);
		RationalComplexMatrix2x2 mat2 = new RationalComplexMatrix2x2(a4,a1,a2,a3);
		RationalComplexMatrix2x2 mat0= new RationalComplexMatrix2x2();
		RationalComplexMatrix2x2 mat4;
		boolean x,y;
		System.out.println(mat1.toString());
		System.out.println(mat2.toString());
		System.out.println(mat0.toString());
		
		System.out.println("det mat1 " + mat1.det());
		System.out.println("mult vect \n"+ mat1.multVector(new RationalComplexVector2D(a1,a4)));
		
		mat0 = mat1.add(mat2);
		System.out.println("add \n" + mat0.toString() );
		mat0 = mat1.mult(mat2);
		System.out.println("mult \n" + mat0.toString());

		
		
		
		
		
	}
	private RationalComplexNumber [] [] a = new RationalComplexNumber [3] [3];
	public void assignment(RationalComplexNumber [] [] x){
		this.a[1][1] = x[1][1];
		this.a[1][2] = x[1][2];
		this.a[2][1] = x[2][1];
		this.a[2][2] = x[2][2];
	}
	public RationalComplexNumber [][] assignmentX(RationalComplexNumber x, RationalComplexNumber y, RationalComplexNumber z, RationalComplexNumber u){
		a[1][1] = x;
		a[1][2] = y;
		a[2][1] = z;
		a[2][2] = u;
		return a;
	}
	RationalComplexMatrix2x2(){
		RationalComplexNumber [][] x = new RationalComplexNumber [3][3];
		x = assignmentX( new RationalComplexNumber(),new RationalComplexNumber(),new RationalComplexNumber(),new RationalComplexNumber());
		assignment(x);
	}
	RationalComplexMatrix2x2(RationalComplexNumber a){
		RationalComplexNumber [][] x = new RationalComplexNumber [3][3];
		x = assignmentX(a,a,a,a);
		assignment(x);
	}
	RationalComplexMatrix2x2(RationalComplexNumber x, RationalComplexNumber y, RationalComplexNumber z, RationalComplexNumber u){
		RationalComplexNumber [][] r = new RationalComplexNumber [3][3];
		r = assignmentX(x,y,z,u);
		assignment(r);
	}
	
	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 mat){
		RationalComplexMatrix2x2 mat2 = new RationalComplexMatrix2x2(this.a[1][1].add(mat.a[1][1]),this.a[1][2].add(mat.a[1][2]),this.a[2][1].add(mat.a[2][1]),this.a[2][2].add(mat.a[2][2]));
		return mat2;
	}
	

	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 mat){
		RationalComplexNumber a1,a2,a3,a4;
		a1 = this.a[1][1].mult(mat.a[1][1]).add(this.a[1][2].mult(mat.a[2][1]));
		a2 = this.a[1][2].mult(mat.a[1][2]).add(this.a[1][2].mult(mat.a[2][2]));
		a3 = this.a[2][1].mult(mat.a[1][1]).add(this.a[2][2].mult(mat.a[2][1]));
		a4 = this.a[2][2].mult(mat.a[1][2]).add(this.a[2][2].mult(mat.a[2][2]));
		RationalComplexMatrix2x2 mat2 = new RationalComplexMatrix2x2(a1,a2,a3,a4);
		return mat2;
	}

	public RationalComplexNumber det(){
		return (this.a[1][1].add(this.a[2][2])).sub(this.a[2][1].mult(this.a[1][2]));
	}
	

	
	public RationalComplexVector2D multVector(RationalComplexVector2D vect){
		RationalComplexVector2D vect2 = new RationalComplexVector2D(this.a[1][1].mult(vect.getX()).add(this.a[1][2].mult(vect.getY())),this.a[2][1].mult(vect.getX()).add(this.a[2][2].mult(vect.getY())));
		return vect2;
	}
	public String toString(){
		return a[1][1] + " " + a[1][2] + "\n" + a[2][1] + " " + a[2][2] + "\n" + "___________";
	}
}