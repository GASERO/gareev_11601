public class RationalFraction{
	public static void main(String[] args){
		RationalFraction rf0 = new RationalFraction();
		RationalFraction rf1 = new RationalFraction(13,26);
		RationalFraction rf2 = new RationalFraction(100,25);
				System.out.println("val2 " + rf1.value());
		boolean x,y;
		System.out.println(rf0.toString());
		System.out.println(rf1.toString());
		System.out.println(rf2.toString());
		
		rf0 = rf1.add(rf2);
		System.out.println("add " + rf0.toString() );
		rf0.add2(rf1);
		System.out.println("add2 " + rf0.toString() );
		rf0 = rf1.sub(rf2);
		System.out.println("sub " + rf0.toString());
		rf0.sub2(rf1);
		System.out.println("sub2 " + rf0.toString());
		rf0 = rf1.mult(rf2);
		System.out.println("mult " + rf0.toString());
		rf0.mult2(rf2);
		System.out.println("mult2 " + rf0.toString());
		rf0 = rf1.div(rf2);
		System.out.println("div " + rf0.toString());
		rf0.div2(rf2);
		System.out.println("div2 " + rf0.toString());
		System.out.println( x = rf1.equals(rf2));
		System.out.println( y = rf1.equals(rf1));
		
	}
	private int x,y;
	public void assignment(int x, int y){
		this.x = x;
		this.y = y;
	}
	RationalFraction(int x, int y){
		assignment(x,y);
		reduce();
	}
	RationalFraction(){
		assignment(0,0);
	}
	public int getX(){
		return this.x;
	}
	public int getY(){
		return this.y;
	}
	public void setX(int x){
		this.x = x;	
	}
	public void setY(int y){
		this.y = y;	
	}
	public void reduce(){
		int max = x > y ? x:y;
		for (int i = max; i >=1; i--){
			if (this.x % i == 0 && this.y % i == 0) {
				this.x = x / i;
				this.y = y / i;
			}
		}
	}
	public RationalFraction add(RationalFraction rf){
		int x = this.x * rf.getY() + this.y * rf.getX();
		int y = this.y * rf.getY();
		RationalFraction rf2 = new RationalFraction(x,y);
		rf2.reduce();
		return rf2;
	}
	public void add2(RationalFraction rf){
		this.x = this.x * rf.getY() + this.y * rf.getY();
		this.y = this.y * rf.getY();
		reduce();
	}
	public RationalFraction sub(RationalFraction rf){
		int x = this.x * rf.getY() - this.y * rf.getY();
		int y = this.y * rf.getY();
		RationalFraction rf2 = new RationalFraction(x,y);
		rf2.reduce();
		return rf2;
	}
	public void sub2(RationalFraction rf){
		this.x = this.x * rf.getY() - this.y * rf.getY();
		this.y = this.y * rf.getY();
		reduce();
	}
	public RationalFraction mult(RationalFraction rf){
		int x = this.x * rf.getX();
		int y = this.y * rf.getY();
		RationalFraction rf2 = new RationalFraction(x,y);
		rf2.reduce();
		return rf2;
	}
	public void mult2(RationalFraction rf){
		this.x = this.x * rf.getX();
		this.y = this.y * rf.getY();
		reduce();
	}
	public RationalFraction div(RationalFraction rf){
		int x = this.x * rf.getY();
		int y = this.y * rf.getX();
		RationalFraction rf2 = new RationalFraction(x,y);
		rf2.reduce();
		return rf2;
	}
	public void div2(RationalFraction rf){
		this.x = this.x * rf.getY();
		this.y = this.y * rf.getX();
		reduce();
	}
	public String toString(){
		return x + "/" + y; 
	}
	public double value(){
		return ((double) x/y);
	}
	public boolean equals(RationalFraction rf){
		if (this.x == rf.getX() && this.y == rf.getY()) return true;
		return false;
	}
	public int numberPart(){
		return x / y;
	}
	
}