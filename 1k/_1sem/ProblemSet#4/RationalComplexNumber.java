public class RationalComplexNumber{
	public static void main(String[] args){
		RationalComplexNumber cm0 = new RationalComplexNumber();
		RationalComplexNumber cm1 = new RationalComplexNumber(new RationalFraction(3, 1),new RationalFraction(17, 23));
		RationalComplexNumber cm2 = new RationalComplexNumber(new RationalFraction(6,79),new RationalFraction(1,11));
		boolean x,y;
		System.out.println(cm0.toString());
		System.out.println(cm1.toString());
		System.out.println(cm2.toString());
		
		cm0 = cm1.add(cm2);
		System.out.println("add " + cm0.toString());
		
		cm0 = cm1.sub(cm2);
		System.out.println("sub " + cm0.toString());
		
		cm0 = cm1.mult(cm2);
		System.out.println("mult " + cm0.toString());
		
		
		
	}
	private RationalFraction x,y;
	public void assignment(RationalFraction x, RationalFraction y){
		this.x = x;
		this.y = y;
	}
	RationalComplexNumber(RationalFraction x, RationalFraction y){
		assignment(x,y);
	}
	RationalComplexNumber(){
		assignment(new RationalFraction(),new RationalFraction());
	}
	public RationalFraction getX(){
		return this.x;
	}
	public RationalFraction getY(){
		return this.y;
	}
	public RationalComplexNumber add(RationalComplexNumber cn){
		RationalFraction x = this.x.add(cn.getX());
		RationalFraction y = this.y.add(cn.getY());
		RationalComplexNumber cn2 = new RationalComplexNumber(x,y);
		return cn2;
	}

	
	public RationalComplexNumber sub(RationalComplexNumber cn){
		RationalFraction x = this.x.sub(cn.getX());
		RationalFraction y = this.y.sub(cn.getY());
		RationalComplexNumber cn2 = new RationalComplexNumber(x,y);
		return cn2;
	}
	
	public RationalComplexNumber mult(RationalComplexNumber cn){
		RationalFraction a = this.x, b = this.y;
		RationalFraction x = a.mult(cn.getX()).sub(b.mult(cn.getY()));
		RationalFraction y = a.mult(cn.getY()).add(b.mult(cn.getX()));
		RationalComplexNumber cn2 = new RationalComplexNumber(x,y);
		return cn2;
	}
	
	
	public String toString(){
		if (this.x.equals(new RationalFraction()) && this.y.equals(new RationalFraction())) return "0";
		if (this.x.value() <0 && this.y.value() > 0) return this.y + " * i " + this.x;
		else {
			if (this.y.value() <= 0){
				return this.x.toString() +" "+ this.y.toString() + " * i";
			}
		}
		return this.x.toString() +" + "+ this.y.toString() + " * i";
	}
	
	

}