public class Student{
	String name;
	int group;
	Student (String name, int group){
		this.group = group;
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public int getGroup(){
		return this.group;
	}
}