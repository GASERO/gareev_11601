import java.util.Random;
public class Prepod{
	private String fio;
	private String subject;
	Prepod (String fio, String subject){
		this.fio = fio;
		this.subject = subject;
	}
	public String getFio(){
		return this.fio;
	}
	public String getSubject(){
		return this.subject;
	}
	public void mark(Student student){
		Random rnd = new Random();
		int mark = 2 + rnd.nextInt(4);
		String a = "-";
		switch (mark){
			case 2: a = "неуд";
					break;
			case 3: a = "уд";
					break;
			case 4: a= "хорошо";
					break;
			case 5: a = "отлично";
					break;
		}
		System.out.println("Преподаватель " + this.fio + " оценил студента с именем " + student.name + " по предмету " + this.subject + " на оценку " + a);
		
	}
}