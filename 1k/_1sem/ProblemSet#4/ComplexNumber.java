public class ComplexNumber{
	public static void main(String[] args){
		ComplexNumber cm0 = new ComplexNumber();
		ComplexNumber cm1 = new ComplexNumber(2,4);
		ComplexNumber cm2 = new ComplexNumber(3,1);
		boolean x,y;
		System.out.println(cm0.toString());
		System.out.println(cm1.toString());
		System.out.println(cm2.toString());
		
		cm0 = cm1.add(cm2);
		System.out.println("add " + cm0.toString() );
		cm0.add2(cm1);
		System.out.println("add2 " + cm0.toString() );
		cm0 = cm1.sub(cm2);
		System.out.println("sub " + cm0.toString());
		cm0.sub2(cm1);
		System.out.println("sub2 " + cm0.toString());
		cm0 = cm1.mult(cm2);
		System.out.println("mult " + cm0.toString());
		cm0.mult2(cm2);
		System.out.println("mult2 " + cm0.toString());
		cm0 = cm1.mult(10);
		System.out.println("mult " + cm0.toString());
		cm0.mult2(10);
		System.out.println("mult2 " + cm0.toString());
		cm0 = cm1.div(cm2);
		System.out.println("div " + cm0.toString());
		cm0.div2(cm2);
		System.out.println("div2 " + cm0.toString());
		System.out.println("length " + cm0.length());
		System.out.println("arg " + cm0.arg());
		System.out.println("pow ^5 = " + cm0.pow(5).toString());
		System.out.println( x = cm1.equals(cm2));
		System.out.println( y = cm1.equals(cm1));
		
	}
	private double x,y;
	public void assignment(double x, double y){
		this.x = x;
		this.y = y;
	}
	ComplexNumber(double x, double y){
		assignment(x,y);
	}
	ComplexNumber(){
		assignment(0,0);
	}
	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}
	public void setX(double x){
		this.x = x;	
	}
	public void setY(double y){
		this.y = y;	
	}
	public ComplexNumber add(ComplexNumber cn){
		double x = this.x + cn.getX();
		double y = this.y + cn.getY();
		ComplexNumber cn2 = new ComplexNumber(x,y);
		return cn2;
	}
	public void add2(ComplexNumber cn){
		this.x = this.x + cn.getX();
		this.y = this.y + cn.getY();
	}
	public ComplexNumber mult(double g){
		double x = this.x * g;
		double y = this.y * g;
		ComplexNumber cn2 = new ComplexNumber(x,y);
		return cn2;
	}
	public void mult2(double x){
		this.x *= x;
		this.y *= x;
	}
	public ComplexNumber sub(ComplexNumber cn){
		double x = this.x - cn.getX();
		double y = this.y - cn.getY();
		ComplexNumber cn2 = new ComplexNumber(x,y);
		return cn2;
	}
	public void sub2(ComplexNumber cn){
		this.x = this.x - cn.getX();
		this.y = this.y - cn.getY();
	}
	public ComplexNumber mult(ComplexNumber cn){
		double a = this.x, b = this.y;
		double x = a * cn.getX() - b * cn.getY();
		double y = a * cn.getY() + b * cn.getX();
		ComplexNumber cn2 = new ComplexNumber(x,y);
		return cn2;
	}
	public void mult2(ComplexNumber cn){
		double a = this.x, b = this.y;
		this.x = a * cn.getX() - b * cn.getY();
		this.y = a * cn.getX() - b * cn.getY();
	}
	public ComplexNumber div(ComplexNumber cn){
		ComplexNumber sopr = new ComplexNumber(this.x,-this.y);
		double r2 = (length()*length());
		sopr = mult(sopr);
		sopr.setX((double)sopr.getX() / r2);
		sopr.setY((double)sopr.getY() / r2);
		return sopr;
	}
	public void div2(ComplexNumber cn){
		ComplexNumber sopr = new ComplexNumber(this.x,-this.y);
		double r2 = (length()*length());
		sopr = mult(sopr);
		this.x = (double)sopr.getX() / r2;
		this.y = (double)sopr.getY() / r2;
	}
	public double length(){
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	public String toString(){
		if (this.x ==0 && this.y==0) return "0";
		if (this.x <0 && this.y > 0) return this.y + " * i " + this.x;
		else {
			if (this.y <= 0){
				return this.x +" "+ this.y + " * i";
			}
		}
		return this.x +" + "+ this.y + " * i";
	}
	public double arg(){
		return Math.atan((double)this.y / this.x);
	}
	public boolean equals(ComplexNumber cn){
		if (this.x == cn.getX() && this.y == cn.getY()) return true;
		return false;
	}
	public ComplexNumber pow(double n){
		double x = Math.pow(length(),n) * Math.cos(n * arg());
		double y = Math.pow(length(),n) * Math.sin(n * arg());
		ComplexNumber sopr = new ComplexNumber(x,y);
		return sopr;
	}
}