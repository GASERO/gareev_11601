import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by Роберт on 20.12.2016.
 */
public class test {
    public static void main(String[] args) throws FileNotFoundException{
        Scanner sc = new Scanner(new File("words.txt"));
        PrintWriter pw = new PrintWriter(new File("out.txt"));
        while (sc.hasNextLine()){
            pw.println(sc.next().toLowerCase());
            sc.nextLine();
        }
        pw.close();
    }
}
