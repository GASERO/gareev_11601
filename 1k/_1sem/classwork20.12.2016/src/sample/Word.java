package sample;

/**
 * Created by Роберт on 20.12.2016.
 */
public class Word {
    private String word,num;
    Word(String word){
        this.word = word;
        this.num = Main.association(word);
    }

    public String getWord() {
        return word;
    }

    public String getNum() {
        return num;
    }
}
