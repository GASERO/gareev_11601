package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main extends Application {
    final static int WIDTH = 500, HEIDTH = 500;
    static String a ="",b="";
    static   ArrayList<Word> words = new ArrayList<>();
    static {
        try {
            Scanner sc = new Scanner(new File("out.txt"));
            while (sc.hasNextLine()){
                words.add(new Word(sc.nextLine()));
            }
        }catch (FileNotFoundException e){}
    }
    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root =new Group();
        Button _1 = new Button();
        Button _2 = new Button();
        Button _3 = new Button();
        Button _4 = new Button();
        Button _5 = new Button();
        Button _6 = new Button();
        Button _7 = new Button();
        Button _8 = new Button();
        Button _9 = new Button();

        TextArea ta = new TextArea();
        ta.setWrapText(true);

        _1.setText("BackSpace");
        _1.setOnAction(event -> {
          a=  a.substring(0,a.length()-1);
          b=  b.substring(0,a.length()-1);
            ta.setText(b);
        });

        _1.setLayoutX(100);
        _1.setLayoutY(150);

        _2.setLayoutX(200);
        _2.setLayoutY(150);

        _3.setLayoutX(300);
        _3.setLayoutY(150);

        _4.setLayoutX(100);
        _4.setLayoutY(200);

        _5.setLayoutX(200);
        _5.setLayoutY(200);

        _6.setLayoutX(300);
        _6.setLayoutY(200);

        _7.setLayoutX(100);
        _7.setLayoutY(250);

        _8.setLayoutX(200);
        _8.setLayoutY(250);

        _9.setLayoutX(300);
        _9.setLayoutY(250);


        _2.setText("abc");
        _3.setText("def");
        _4.setText("ghi");
        _5.setText("jkl");
        _6.setText("mno");
        _7.setText("pqrs");
        _8.setText("tuv");
        _9.setText("wxyz");

        _2.setPrefSize(60,10);
        _3.setPrefSize(60,10);
        _4.setPrefSize(60,10);
        _5.setPrefSize(60,10);
        _6.setPrefSize(60,10);
        _7.setPrefSize(60,10);
        _8.setPrefSize(60,10);
        _9.setPrefSize(60,10);



        _2.setOnAction(event -> {
            a+='2';
            b+='a';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _3.setOnAction(event -> {
            a+='3';
            b+='d';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText() );
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _4.setOnAction(event -> {
            a+='4';
            b+='g';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _5.setOnAction(event -> {
            a+='5';
            b+='j';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _6.setOnAction(event -> {
            a+='6';
            b+='m';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _7.setOnAction(event -> {
            a+='7';
            b+='p';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _8.setOnAction(event -> {
            a+='8';
            b+='t';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        _9.setOnAction(event -> {
            a+='9';
            b+='w';
            ta.setText("");
            for (Word x: words) {
                if (x.getNum().equals(a)){
                    ta.setText(x.getWord() + "\n" +ta.getText());
                }
            }
            if (ta.getText().equals("")) ta.setText(b);
        });
        ta.setPrefSize(WIDTH,100);
        root.getChildren().addAll(_1, _2, _3, _4, _5, _6, _7, _8, _9,ta );
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, WIDTH, HEIDTH));
        primaryStage.show();
    }

    public static String association(String string){
        String a = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case 'a':
                case 'b':
                case 'c': a +='2';break;
                case 'd':
                case 'e':
                case 'f': a +='3';break;
                case 'g':
                case 'h':
                case 'i': a +='4';break;
                case 'j':
                case 'k':
                case 'l': a +='5';break;
                case 'm':
                case 'n':
                case 'o': a +='6';break;
                case 'p':
                case 'q':
                case 's':
                case 'r': a +='7';break;
                case 't':
                case 'u':
                case 'v': a +='8';break;
                case 'x':
                case 'y':
                case 'w':
                case 'z': a +='9';break;
            }
        }
        return a;
    }
    public static void main(String[] args) {
        launch(args);
    }

}
