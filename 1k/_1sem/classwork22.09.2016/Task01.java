public class Task01{
	public static void main(String[] args){
		int n = 7;
		int [] a = new int[n];
		int max = -1000;
		for (int i = 0; i < n; i++){
			a[i] = Integer.parseInt(args[i]);
		}
		for (int i = 0; i < n; i++){
			if (a[i] > max){
				max = a[i];
			}
		}
		System.out.print(max);
		
	}
}