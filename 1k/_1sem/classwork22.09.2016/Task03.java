public class Task03{
	public static void main(String[] args){
		int n = 10;
		int k = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int x;
		for (int i = 0; i < n; i++){
			a[i] = i;
		}
		for (int i = 0; i < k / 2; i++){
			x = a[i];
			a[i] = a[k - i - 1];
			a[k - i - 1] = x;
	
			System.out.print("-");
		}
		for (int i = k; i < k +(n - k) / 2 ; i++){
			x = a[i];
			a[i] = a[n + k -i - 1];
			a[n + k - i - 1] = x;
			
		}
		for (int i = 0; i < n / 2;i++ ){
			x = a[i];
			a[i] = a[n - 1 - i];
			a[n - 1 - i] = x;
			System.out.print("-");
		}
		for (int v: a){
			System.out.print(v);
		}
		
		
	}
}