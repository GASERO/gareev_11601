package sample;

import javafx.scene.shape.Circle;

public class Enemy {
    private boolean active;
    private Circle circle;
    Enemy(int x, int y, int rad){
        circle = new Circle(x,y,rad);
        active = true;
    }

    public boolean isActive() {
        return active;
    }

    public Circle getCircle() {
        return circle;
    }
    public void mute(){
        active = false;
    }

}
