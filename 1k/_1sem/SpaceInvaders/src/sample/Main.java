package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.concurrent.TimeUnit;
import static java.lang.Math.*;

public class Main extends Application {
    private final static int WIDTH = 400;
    private final static int HEIGHT = 400;
    private Player player;
    private final  int ENEMIES = 300;
    private boolean game = true;
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();
        player = new Player(WIDTH / 2, HEIGHT - 30, root);

        Enemy[] enemies = new Enemy[ENEMIES];
        final int[] enemies_number = {0};

        new AnimationTimer() {
            long was = System.currentTimeMillis();
            @Override
            public void handle(long now) {
                for (int i = 0; i < enemies_number[0]; i++) {
                    enemies[i].getCircle().setCenterY(
                            enemies[i].getCircle().getCenterY() + 3
                    );
                  /*  if (enemies[i].getCenterY() > HEIGHT) {
                        root.getChildren().remove(enemies[i]);
                    }*/
                    if (enemies[i].isActive() && sqrt( pow(enemies[i].getCircle().getCenterX()- player.getX(), 2) + pow(enemies[i].getCircle().getCenterY()- player.getY(), 2) ) < player.getPr().getRadius() + enemies[i].getCircle().getRadius()){
                        game = false;
                        stop();
                    }

                }

                if (System.currentTimeMillis() - was > 250 && enemies_number[0] < ENEMIES ) {
                    enemies[enemies_number[0]] = new Enemy((int) (Math.random()*WIDTH), 0, 30);
                    enemies[enemies_number[0]].getCircle().setStyle("-fx-fill: tomato");
                    root.getChildren().add(enemies[enemies_number[0]].getCircle());
                    enemies_number[0]++;
                    was = System.currentTimeMillis();
                }
                if (enemies[ENEMIES - 1] != null) {
                    if (enemies[ENEMIES - 1].getCircle().getCenterY() > HEIGHT) {

                        root.getChildren().remove((enemies[ENEMIES-1]));
                        Text tx = new Text("U WIN");
                        tx.setLayoutY(HEIGHT / 2);
                        tx.setLayoutX(WIDTH / 2);
                        root.getChildren().add(tx);
                        stop();

                    }
                }

            }

        }.start();
        Text tx = new Text();
        root.setOnKeyPressed(event -> {
            if (game) {
                switch (event.getCode()) {
                    case D:
                    case RIGHT:
                        if (player.getX() < WIDTH - player.getPr().getRadius() - player.getV()) {
                            player.updateX(1);
                        }
                        break;
                    case A:
                    case LEFT:
                        if (player.getX() > player.getPr().getRadius() + player.getV()) {
                            player.updateX(-1);
                        }
                        break;
                    case SPACE:
                        Circle bullet = new Circle(player.getX(),
                                player.getY(), 3);
                        root.getChildren().add(bullet);
                        new AnimationTimer() {

                            @Override
                            public void handle(long now) {
                                bullet.setCenterY(bullet.getCenterY() - 10);
                                if (bullet.getCenterY() < -10) {
                                    this.stop();
                                }
                                for (int i = 0; i < enemies_number[0]; i++){
                                    if (enemies[i].isActive() && sqrt( pow(enemies[i].getCircle().getCenterX()- bullet.getCenterX(), 2) + pow(enemies[i].getCircle().getCenterY()- bullet.getCenterY(), 2) ) < bullet.getRadius() + enemies[i].getCircle().getRadius()){
                                        root.getChildren().removeAll(enemies[i].getCircle(),bullet);
                                        enemies[i].mute();
                                        player.incScores();
                                        tx.setText(Integer.toString(player.getScores()));
                                        stop();
                                    }
                                }

                            }

                        }.start();

                        break;
                }
            }
        });

        tx.setLayoutY(HEIGHT - 20);
        tx.setLayoutX(WIDTH - 50);

        root.getChildren().add(tx);
        primaryStage.setTitle("SPACE INVADERS");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
        root.requestFocus();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
