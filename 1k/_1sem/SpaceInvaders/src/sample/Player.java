package sample;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;

public class Player {
    private int x, y, v, hp, scores;
    private PlayerRepr pr;

    public void updateX(int dir) {
        this.x = this.x + dir * v;
        pr.updateX();
    }

    public Player(int x, int y, Group group) {
        this.x = x;
        this.y = y;
        this.v = 20;
        this.hp = 100;
        this.scores = 0;
        pr = new PlayerRepr(group, this);
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public PlayerRepr getPr() {
        return pr;
    }

    public int getV() {
        return v;
    }
    public int getScores(){
        return scores;
    }
    public void incScores(){
        scores++;
    }
}
