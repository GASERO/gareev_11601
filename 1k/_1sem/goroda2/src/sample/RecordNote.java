package sample;

public class RecordNote {
    private String name;
    private int score;
    RecordNote(String string) {
        String[] field = string.split(",");
        name = field[1];
        score =Integer.parseInt(field[0]);

    }

    public int getScore() {
        return score;
    }
    public String toString(){
        return " "+ name + " " + score;
    }

    public static void main(String[] args) {
        RecordNote a = new RecordNote("1,alala");
        System.out.println(a.toString());
    }
}