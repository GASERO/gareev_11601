package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import static sample.CheckWithRegEx.check;

public class Main extends Application {
    final int HEIGHT = 500;
    final int WIDTH = 500;
    static ArrayList<String> words = new ArrayList<>();
    static ArrayList<RecordNote> spisok = new ArrayList<>();
    static HashSet<String> wordlist = new HashSet<>();
    static RecordNote[] record = new RecordNote[1000];
    static boolean[] letters = new boolean[33];
    static int score;
    static int recNum;

    static {
        try {
            Scanner sc = new Scanner(new File("words.txt"));
            while (sc.hasNextLine()) {
                wordlist.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
        }
    }

    static {
        try {
            Scanner sc = new Scanner(new File("words.txt"));
            while (sc.hasNextLine()) {
                words.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

      /*  try {
            Scanner sc1 = new Scanner(new File("words.txt"));

            while (sc1.hasNext()) {
                words.add(sc1.nextLine());
            }
        } catch (FileNotFoundException e) {
        }*/
        for (boolean x: letters
             ) {x = true;

        }
        int kol = 0;
        try {
            Scanner sc = new Scanner(new File("records.txt"));

            System.out.println(sc.nextLine());
            for (int i = 0; i < 100 && sc.hasNextLine(); i++) {
                record[i] = new RecordNote(sc.nextLine());
                kol++;
            }
        }
        catch (FileNotFoundException e){}
        int i = kol;
        System.out.println(i);
        RecordNote swap;
        for ( int c = 0; c <= ( i - 1 ); c++) {
            for (int d = 0; d < i - c - 1; d++) {
                if (record[d].getScore() < record[d + 1].getScore()) {
                    swap = record[d];
                    record[d] = record[d + 1];
                    record[d + 1] = swap;
                }
            }
        }
        recNum = i;
        for (int k = 0; k < 3; k++){
            spisok.add(record[k]);
        }


        Group root = new Group();
        primaryStage.setResizable(false);
        primaryStage.setTitle("Города");


        TextArea history = new TextArea();
        history.setPrefSize(WIDTH - 40, HEIGHT * 0.85 - 60);
        history.setLayoutX(20);
        history.setLayoutY(40);
        history.setEditable(false);
        history.setWrapText(true);
        history.setFont(Font.font("Algerian", 15));
        MenuBar menu = new MenuBar();
        menu.setPrefSize(WIDTH, 30);
        Menu menuGame = new Menu("Меню");
        menu.getMenus().add(menuGame);
        MenuItem records = new MenuItem("Рекорды");
        menuGame.getItems().add(records);

        Stage rec = new Stage();
        rec.initStyle(StageStyle.UTILITY);
        Group recGroup = new Group();
        TextArea meme = new TextArea();
        meme.setWrapText(true);
        for (int j = 0; j < 3; j++) {
            meme.setText(meme.getText() + "\n" + (j+1)+ record[j].toString());
        }
        meme.setEditable(false);
        meme.setPrefSize(250, 150);
        recGroup.getChildren().addAll(meme);
        rec.setScene(new Scene(recGroup, 301, 201));
        rec.setTitle("Рекорды");




        records.setOnAction(event -> {
            rec.show();
        });

        TextField input = new TextField();
        input.setLayoutY(HEIGHT * 0.85);
        input.setLayoutX(20);
        input.setPrefSize(WIDTH / 2, HEIGHT / 10);

        MenuItem ng = new MenuItem("Новая игра");
        ng.setOnAction(event -> {
            history.setText("");
            input.setText("");
            gameStart();
        });

        Alert alreadyexists = new Alert(Alert.AlertType.ERROR);
        alreadyexists.setHeaderText("Такой город уже существует");
        alreadyexists.setContentText("Такой город уже есть в списке, и вы не можете его добавить");

        Alert notaname = new Alert(Alert.AlertType.ERROR);
        notaname.setHeaderText("Не похоже на название города...");

        MenuItem options = new MenuItem("Добавить город...");
        options.setOnAction(event -> {

            Stage dialog = new Stage();
            dialog.initStyle(StageStyle.UTILITY);
            Group group = new Group();
            Text tx = new Text();
            tx.setText("Введите название города ");
            tx.setLayoutX(20);
            tx.setLayoutY(30);
            TextField add = new TextField();
            add.setLayoutX(HEIGHT / 2);
            add.setLayoutY(12);
            Button confirm = new Button("Подтвердить");
            confirm.setLayoutX(WIDTH / 2.7);
            confirm.setLayoutY(HEIGHT / 6);
            confirm.setOnAction(event1 -> {
                if (wordlist.contains(add.getText().toLowerCase())) {
                    alreadyexists.showAndWait();
                } else {
                    if (check(add.getText().toLowerCase())) {
                        wordlist.add(add.getText().toLowerCase());
                        words.add(add.getText().toLowerCase());
                        try {
                            PrintWriter pw = new PrintWriter(new FileOutputStream(new File("words.txt"), true));
                            pw.println(add.getText().toLowerCase());
                            pw.close();
                        } catch (FileNotFoundException e) {
                        }
                        dialog.close();
                    } else {
                        notaname.showAndWait();
                    }
                }

            });
            group.getChildren().addAll(confirm, tx, add);
            dialog.setScene(new Scene(group, WIDTH, HEIGHT / 3));
            dialog.setTitle("Добавить город");
            dialog.show();


        });


        MenuItem about = new MenuItem("Инфо");
        Alert info = new Alert(Alert.AlertType.INFORMATION);
        info.setContentText("Game by R. Gareev");
        info.setHeaderText("");
        about.setOnAction(event -> info.showAndWait());

        MenuItem exit = new MenuItem("Выход");
        exit.setOnAction(event -> System.exit(0));

        menuGame.getItems().addAll(ng, options, about, exit);


        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText("Неккоректный текст!");
        error.setContentText("Неккоректные текс, пожалуйста используйте только русские буквы");

        Alert citynotfound = new Alert(Alert.AlertType.INFORMATION);
        citynotfound.setHeaderText("Такой город не найден");
        citynotfound.setContentText("Попробуйте найти ошибку в слове, или введите другое название");

        Alert citywaspicked = new Alert(Alert.AlertType.ERROR);
        citywaspicked.setHeaderText("Нельзя выбрать город");
        citywaspicked.setContentText("Город уже был использован, выберите другой");

        Button b = new Button("Вввод");
        b.setPrefSize(WIDTH / 5, HEIGHT / 10);
        b.setLayoutX(WIDTH / 1.7);
        b.setLayoutY(HEIGHT * 0.85);
        b.setOnAction(event -> {
         //   if (letters[input.getText().toLowerCase().charAt(0) - 'a']) {
                if (check(input.getText().toLowerCase())) {
                    if (words.contains(input.getText().toLowerCase())) {
                        history.setText("you: " + input.getText() + "\r\n" + history.getText());
                        words.remove(input.getText().toLowerCase());
                        score++;
                        String a = input.getText().trim();
                        char c = a.charAt(a.length() - 1);
                        if (c == 'ы' | c == 'ь'| c == 'ъ'){
                            c= a.charAt(a.length() - 2);
                        }
                        for (String x : words) {
                            if (x.toLowerCase().charAt(0) == c) {
                                history.setText("PC: " + x + "\r\n" + history.getText());
                                words.remove(x);
                                break;
                            }
                        }
                        input.setText("");
                    } else {
                        if (wordlist.contains(input.getText().toLowerCase())) {
                            citywaspicked.showAndWait();
                        } else citynotfound.showAndWait();
                    }
                } else error.showAndWait();

        });

        root.getChildren().addAll(b, history, input, menu);
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }

    public static void gameStart() {
        for (boolean x: letters
                ) {x = true;

        }
        words.clear();
        try {
            Scanner sc1 = new Scanner(new File("words.txt"));

            while (sc1.hasNext()) {
                words.add(sc1.nextLine());
            }
        } catch (FileNotFoundException e) {
        }
        recFill();

    }

    public static void recFill(){
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(new File("records.txt"), true));

            Stage input = new Stage();
            input.initStyle(StageStyle.UTILITY);
            Group recGroup = new Group();
            TextField name = new TextField();
            name.setPrefSize(100, 15);
            input.setScene(new Scene(recGroup, 300, 100));
            name.setLayoutX(100);
            input.setTitle("Введите имя");
            Button ok = new Button("ok");
            ok.setLayoutX(120);
            ok.setLayoutY(45);
            recGroup.getChildren().addAll(name,ok);
            input.show();
            ok.setOnAction(event -> {
                if (name.getText().contains(" ")){
                    Alert x = new Alert(Alert.AlertType.ERROR);
                    x.setHeaderText("Имя не должно содержать пробелов");
                    x.showAndWait();
                }
                else {

                    pw.println(score + "," +name.getText());
                    pw.close();
                    input.close();
                    score = 0;
                }
            });
            //
        }catch (FileNotFoundException e){}
        int kol = 0;
        try {
            Scanner sc = new Scanner(new File("records.txt"));

        System.out.println(sc.nextLine());
        for (int i = 0; i < 100 && sc.hasNextLine(); i++) {
            record[i] = new RecordNote(sc.nextLine());
            kol++;
        }
        }
        catch (FileNotFoundException e){}
        int i = kol;
        System.out.println(i);
        RecordNote swap;
        for ( int c = 0; c <= ( i - 1 ); c++) {
            for (int d = 0; d < i - c - 1; d++) {
                if (record[d].getScore() < record[d + 1].getScore()) {
                    swap = record[d];
                    record[d] = record[d + 1];
                    record[d + 1] = swap;
                }
            }
        }
        recNum = i;
        for (int k = 0; k < 3; k++){
            spisok.add(record[k]);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}