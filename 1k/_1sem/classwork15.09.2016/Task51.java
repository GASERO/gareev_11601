public class Task51 {
	public static void main(String[] args){
		final double EPS = 1e-9;
		double x = 1;
		int n = 1;
		double y;
		do	{
			y = x;
			x = (double ) (n - 1) / 2 / n  ;
			n++;
			System.out.println(x);
		}
		while (Math.abs(y - x) > EPS);
		System.out.println("____________________");
		System.out.print("ANSWER: ");
		System.out.print(x);
	}
}