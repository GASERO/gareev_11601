public class Task49 {
	public static void main(String[] args){
		final double EPS = 1e-9;
		double k = 4;
		double x = 1;
		double a = 9;
		int n = 2;
		double y;
		do	{
			y = x;
			x = (double ) (k + a) / (k * (-2) + 3 * a ) ;
			n++;
			k *= -2;
			a *= 3;
			System.out.println(x);
		}
		while (Math.abs(y - x) > EPS);
		System.out.println("____________________");
		System.out.print("ANSWER: ");
		System.out.print(x);
	}
}