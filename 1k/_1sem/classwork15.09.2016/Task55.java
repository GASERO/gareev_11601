public class Task55 {
	public static void main(String[] args){
		final double EPS = 1e-9;
		double x = 0.5;
		int n = 2;
		long k = 4;
		int b = 3;
		double y;
		do	{
			y = x;
			x += (double ) b / k;
			n++;
			b += 2;
			k *= 2;
			System.out.println(x);
		}
		while (Math.abs(y - x) > EPS);
		System.out.println("____________________");
		System.out.print("ANSWER: ");
		System.out.print(x);
	}
}