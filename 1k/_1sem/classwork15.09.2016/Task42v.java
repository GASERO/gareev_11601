public class Task42v {
	public static void main(String[] args){
		final double EPS = 1e-9;
		int k = 1;
		double x = 1;
		int n = 2;
		double y;
		do	{
			y = x;
			k *=n;
			x = (double ) 1 / k;
			n++;
			System.out.println(x);
		}
		while (Math.abs(y - x) > EPS);
		System.out.println("____________________");
		System.out.print("ANSWER: ");
		System.out.print(x);
	}
}