public class Task42a {
	public static void main(String[] args){
		final double EPS = 1e-9;
		int k = 1;
		double x = 1;
		double y = 2;
		int n = 1;
		while (Math.abs(y - x) > EPS){
			if ( n != 1 ) {
				y = x;
			}
			x =(double) k / n / n;
			k = -1 * k;
			n++;
			
			System.out.println(x);
		}
		System.out.print(x);
	}
}