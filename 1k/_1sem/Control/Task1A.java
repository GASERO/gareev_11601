import java.util.Scanner;
public class Task1A{
	public static void main(String[] arg){
		Scanner scan = new Scanner(System.in);
		System.out.print("x = ");
		double x = scan.nextDouble();
		x = x * x + x + 1 ;
		final double EPS = 1e-9;
		int f = 1;
		int k = 1;
		double b = x;
		double y,yprev = 0;
		int i = 0;
		y = 0;
		double result = 0;
		do {
			yprev = y;
			y = k * b / f;
			result +=y;
			k *= -1;
			b = b * x * x;
			i++;
			f = f * (i + 1)* (i + 2);
			
			System.out.println(y);
		}
		while(Math.abs(y - yprev)>EPS);
		System.out.print("result = " + result);
		System.out.print("right answer = " + Math.sin(x));
	}
}