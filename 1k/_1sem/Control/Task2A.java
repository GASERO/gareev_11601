public class Task2A{
	public static void main(String[] args){
		boolean correct = false;
		int n,k = 0;
		for (int i = 0; i < args.length && !correct && k < 3; i++){
			n = Integer.parseInt(args[i]);
			if (check(n)) k++;
			
		}
		if (k == 2){
			System.out.print("+");
		}
		else {
			System.out.print("-");
		}
	}
	public static boolean check(int n){
		int digit;
		while (n > 0) {
			digit = n % 10;
			if (digit % 2 != 0 ) return false;
			n /= 10;
		}
		return true;
	}
}