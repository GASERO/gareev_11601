package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        ComboBox oper = new ComboBox();
        oper.getItems().addAll(
                "+","-","*","/"

        );
        TextField a = new TextField();
        oper.setLayoutX(200);
        a.setLayoutX(50);
        a.setMaxSize(125,10);
        TextField b = new TextField();
        b.setMaxSize(125,10);
        b.setLayoutX(285);
        TextField c = new TextField();
        c.setLayoutX(450);
        c.setLayoutY(20);
        a.setLayoutY(20);
        b.setLayoutY(20);
        Button go = new Button ("get result!");
        go.setLayoutY(75);
        go.setLayoutX(300);
        go.setOnAction(event -> {
            switch ((String) oper.getValue()){
                case "+": c.setText(Double.toString(Double.parseDouble(a.getText()) + Double.parseDouble(b.getText())));
                    break;
                case "/": c.setText(Double.toString(Double.parseDouble(a.getText()) / Double.parseDouble(b.getText())));
                    break;
                case "*": c.setText(Double.toString(Double.parseDouble(a.getText()) * Double.parseDouble(b.getText())));
                    break;
                case "-": c.setText(Double.toString(Double.parseDouble(a.getText()) - Double.parseDouble(b.getText())));
                    break;

            }
        });

        oper.setLayoutY(20);
        Text a1 = new Text(425,40,"=");
      /*  double x = Integer.parseInt(a.getText());
        double y = Integer.parseInt(b.getText());*/
        c.setText((String)oper.getValue());
        root.getChildren().addAll(a1,a,b,c,oper,go);
        primaryStage.setTitle("Сalculator by Robert");
        primaryStage.setScene(new Scene(root, 700, 150));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
