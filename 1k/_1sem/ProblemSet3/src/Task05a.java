import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Роберт on 21.12.2016.
 */

public class Task05a {
    public static void main(String[] args) {
        int k = 0;
        int kolvo = 0;
        Pattern pat = Pattern.compile("([02468]{0,2}[13579])+[02468]{0,2}");
        int x;
        Matcher matcher = pat.matcher("221221000");
        System.out.println(matcher.matches());
        Random rnd = new Random();
        while (k < 10){
            kolvo++;
            x = rnd.nextInt(2147483646)+1;
            Matcher mat= pat.matcher(""+ x);
            if (mat.matches()){
                k++;
                System.out.println(x);
            }
        }

        System.out.println(kolvo);
    }
}
