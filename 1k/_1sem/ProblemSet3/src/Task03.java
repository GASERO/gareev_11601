import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Роберт on 21.12.2016.
 */

public class Task03 {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("1+|0+|((10)+1?)|((01)+0?)");
        Matcher mat= pat.matcher("1010");
        System.out.println(mat.matches());
    }
}
