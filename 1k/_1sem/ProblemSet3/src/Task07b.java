import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Роберт on 21.12.2016.
 */

public class Task07b {
    public static void main(String[] args) {
        int k = 0;
        int kolvo = 0;
        Pattern pat = Pattern.compile("([02468]{2})[0-9]*([02468{2}])\\1");
        int x;
        Random rnd = new Random();
        while (k < 10){
            kolvo++;
            x = rnd.nextInt(2147483646)+1;
            Matcher mat= pat.matcher(""+ x);
            if (mat.find()){
                k++;
                System.out.println(x);
            }
        }

        System.out.println(kolvo);
    }
}