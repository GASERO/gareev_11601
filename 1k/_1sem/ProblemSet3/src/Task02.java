import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Роберт on 21.12.2016.
 */

public class Task02 {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("([-+]?[1-9][0-9]+((\\.|\\,)[0-9]*(\\([0-9]+[1-9]\\))?)?)|0|[+-]0((\\.|\\,)[0-9]*(\\([0-9]+[1-9]\\))?)");
        Matcher mat= pat.matcher("-0.6");
        System.out.println(mat.matches());
    }
}

