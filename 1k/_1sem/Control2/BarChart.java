import java.util.Scanner;
public class BarChart{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("n = ");
		int n = sc.nextInt();
		int [] a = new int[n];
		int max = 0;
		for (int i = 0; i < n; i++){
			a[i] = sc.nextInt();
			if (max < a[i]) max = a[i];
		}
		int k = max;
		for (int i = 0; i < max; i ++){
			for (int j = 0; j < n; j++){
				if (a[j] == k) {
					System.out.print("0");
					a[j]--;
				}
				else System.out.print(" ");
			}
			k--;
			System.out.println();
		}
	}
}