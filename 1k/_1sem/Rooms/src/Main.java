/**
 * Created by Роберт on 30.11.2016.
 */
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        Scanner scRoom = new Scanner(new File("c:\\gr\\Rooms.csv"));
        Scanner scRes = new Scanner(new File("c:\\gr\\Reservations.csv"));
        scRoom.nextLine();
        scRes.nextLine();
        Room [] rooms = new Room[20];
        int krum = 0;
        while (scRoom.hasNextLine()){
            rooms[krum] = new Room(scRoom.nextLine());
            krum++;
        }
        Reservations [] res = new Reservations[1000];
        int kres = 0;
        while (scRes.hasNextLine()){
            res[kres] = new Reservations(scRes.nextLine());
            kres++;
        }
        System.out.println("Write reservation's number: ");
        int x = sc.nextInt();
        res[x].resInfo();
        res[x].roomInfo(rooms);

        Room swap;
        for ( int c = 0; c < ( krum - 1 ); c++) {
            for (int d = 0; d < krum - c - 1; d++) {
                if (rooms[d].basePrice > rooms[d + 1].basePrice) {
                    swap = rooms[d];
                    rooms[d] = rooms[d + 1];
                    rooms[d + 1] = swap;
                }
            }
        }
        System.out.println();
        System.out.println("SORT");
        System.out.println();
        for (int i = 0; i < krum; i++){
            System.out.println(rooms[i].roomID);
        }

    }
}
