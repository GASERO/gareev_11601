import java.util.InputMismatchException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
public class Reservations {
    int code;
    float rate;
    byte adults,kids;
    String checkIn,checkOut,lastName,firstName,room;
    Reservations(String string) throws InputMismatchException{
        String[] field =string.split(",");
        code = Integer.parseInt(field[0]);
        room = pruning(field[1]);
       // System.out.println(room);
        field[2] = pruning(field[2]);
        field[3] = pruning(field[3]);
       // System.out.println(field[2]);
       // System.out.println(field[3]);
        checkIn = field[2];
        checkOut =field[3];
        if (!check(field[2])) {
            System.out.println("CheckIN date is incorrect ");
            throw new InputMismatchException();
        }
        if (!check(field[3])) {
            System.out.println("CheckOUT date is incorrect ");
            throw new InputMismatchException();
        }

        rate = (float) Double.parseDouble(field[4]);
        lastName =pruning(field[5]);
        firstName =pruning(field[6]);
        adults = (byte) Integer.parseInt(field[7]);
        kids = (byte) Integer.parseInt(field[8]);
    }
    public static boolean check(String date){
        Pattern p = Pattern.compile("((0\\d|1\\d|2\\d|3(0|1))\\-(JAN|MAR|MAY|JUL|AUG|OCT|DEC)\\-(\\d\\d)|(0\\d|1\\d|2\\d|30)-(APR|JUN|SEP|NOV)-(\\d{2})|(0|1|2)\\d-FEB-(\\d{2}))");
        Matcher m = p.matcher(date);
        return m.matches();
    }
    private String pruning(String string) {
       // System.out.println(string.substring(1,string.length()-1));
        return string.substring(1,string.length()-1);
    }
    public void roomInfo(Room[] rooms){
        for (Room x: rooms){
            if (this.room.equals(x.roomID)){
               // System.out.println(x.roomID +" / "+ this.room);
                x.printInfo();
               return;
            }
        }
        System.out.println("RoomType not found");
    }
    public void resInfo(){
        System.out.println("RESERVATION'S INFORM");
        System.out.println("code: " + code);
        System.out.println("roomd: " + room);
        System.out.println("checkIN: " + checkIn);
        System.out.println("checkOut: "+ checkOut);
        System.out.println("Rate: "+ rate);
        System.out.println("LastName: " + lastName);
        System.out.println("FirstName: " + firstName);
        System.out.println("Adults: "+ adults);
        System.out.println("Kids: "+ kids);

    }
    /*(1\\d|2\\d|3(0|1))-(JAN|MAR|MAY|JUL|AUG|OCT|DEC)-(\\d{2}) // for 31 days
    (1\\d|2\\d|30)-(APR|JUN|SEP|NOV)-(\\d{2}) // for 30 days
    (1|2)\\d-FEB-(\\d{2}) for feb */
}

