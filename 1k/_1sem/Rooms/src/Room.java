
public class Room {
    String roomID, roomName, bedType, decor;
    byte beds, maxOccupancy;
    short basePrice;
    Room(String string){
        String[] field =string.split(",");
        roomID = pruning(field[0]);
        roomName = pruning(field[1]);
        beds =(byte) Integer.parseInt(field[2]);
        bedType = pruning(field[3]);
        maxOccupancy = (byte) Integer.parseInt(field[4]);
        basePrice =(short) Integer.parseInt(field[2]);
        decor = pruning(field[6]);
    }
    private String pruning(String string) {
        return string.substring(1,string.length()-1);
    }
    public void printInfo(){
        System.out.println();
        System.out.println("ROOM'S INFO: ");
        System.out.println();
        System.out.println("roomId: " + roomID);
        System.out.println("roomName: "+roomName);
        System.out.println("beds: " + beds);
        System.out.println("bedTyp: "+bedType);
        System.out.println("maxOccupancy:"+maxOccupancy );
        System.out.println("basePrice: "+basePrice);
        System.out.println("decor: "+ decor);
    }
}
