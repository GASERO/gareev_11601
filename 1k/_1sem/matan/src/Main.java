
public class Main {
    static double a;
    static double b;
    public static void main(String[] args) {
        int n = 23;
        int[] x = {0,100, 100 + n, 230 - n, 240 + n, 300, 380 - n};
        int[] y = {0,200 + n, 312, 347, 495, 450 + n, 603};
        a = (6 * sumProis(x, y) - sum(x) * sum(y)) / (6 * sumKvad(x) - sum(x) * sum(x));
        System.out.println(a);
        b = (sum(y)*sumKvad(x)-sumProis(x,y)*sum(x)) / (6*sumKvad(x)-sum(x)*sum(x));
        System.out.println(b);
        double r = Math.sqrt((sumMinusSred(y,srZnach(y)) - sumMinusYi(x,y)) / ((sumMinusSred(y,srZnach(y)))));
        System.out.println(r);
    }
    public static double sumProis(int[] a, int[] b){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=(a[i] * b[i]);
        }
        return  sum;
    }
    public static double sum(int[] a){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=a[i];
        }
        return sum;
    }
    public static double sumKvad(int[] a){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=(a[i] * a[i]);
        }
        return sum;
    }
    public static double srZnach(int[] a){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=a[i];
        }
        return sum/6;
    }
    public static double sumMinusSred(int[] a, double sr){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=(a[i] - sr) * (a[i] - sr);
        }
        return sum;
    }
    public static double yi(int x){
        return x*a + b;
    }
    public static double sumMinusYi(int[] x, int[] y){
        double sum = 0;
        for (int i = 1; i <= 6; i++) {
            sum+=(y[i] - yi(x[i])) * (y[i] - yi(x[i]));
        }
        return sum;
    }

}
