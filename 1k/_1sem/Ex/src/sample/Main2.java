package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main2 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        int windowSize = 300;
        int rad = 10;
        Circle c = new Circle(30,30,rad);
        c.setStyle("-fx-fill: tomato");
        root.getChildren().add(c);
        root.setOnKeyPressed(event -> {
           if ((event.getCode() == KeyCode.S) && (c.getCenterY() <= windowSize - rad-5) ){
               c.setCenterY(c.getCenterY() + 5);
           }
           if (event.getCode() == KeyCode.D && c.getCenterX() <= windowSize - rad -5 ) {
               c.setCenterX(c.getCenterX() + 5);
           }
            if (event.getCode() == KeyCode.A && c.getCenterX() >= rad +5 ){
                c.setCenterX(c.getCenterX() - 5);
            }
            if (event.getCode() == KeyCode.W && c.getCenterY() >= rad +5){
                c.setCenterY(c.getCenterY() - 5);
            }
        });
        primaryStage.setScene(new Scene(root, 300, 300));
        primaryStage.show();
        primaryStage.getScene().getRoot().requestFocus();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
