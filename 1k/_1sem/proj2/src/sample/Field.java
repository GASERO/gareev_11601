package sample;

import Solders.Archer;
import Solders.Solder;
import Solders.SwordsMan;

import java.util.ArrayList;

/**
 * Created by Роберт on 20.12.2016.
 */
public class Field {
    ArrayList<Solder>[] team = new ArrayList[2];
    private ArrayList<Solder> team1 = new ArrayList<>();
    private ArrayList<Solder> team0 = new ArrayList<>();
    Solder[][] map = new Solder[9][12];

    Field() {
        team[0] = team0;
        team[1] = team1;

        addSW(1, 1, 0);
        addSW(3, 1, 0);
        addSW(5, 1, 0);
        addSW(7, 1, 0);

        addSW(1, 10, 1);
        addSW(3, 10, 1);
        addSW(5, 10, 1);
        addSW(7, 10, 1);

        addAR(2, 0, 0);
        addAR(4, 0, 0);
        addAR(6, 0, 0);

        addAR(2, 11, 1);
        addAR(4, 11, 1);
        addAR(6, 11, 1);
    }

    private void addSW(int y, int x, int team) {
        map[y][x] = new SwordsMan(team);
        map[y][x].setIvLayout(x, y);
        if (team == 0) {
            team0.add(map[y][x]);

        } else {
            team1.add(map[y][x]);
        }
    }

    private void addAR(int y, int x, int team) {
        map[y][x] = new Archer(team);
        map[y][x].setIvLayout(x, y);
        if (team == 0) {
            team0.add(map[y][x]);

        } else {
            team1.add(map[y][x]);
        }
    }

}
