package sample;

import Solders.Solder;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
// lightgreen 0.37 0x90ee905e
//ping 0.37 0xffc0cb5e


public class Main extends Application {
    public final static int HEIGHT = 900;
    public final static int WIDTH = 1200, CHEIDTH = (int) (HEIGHT * 0.85 / 9), CWIDTH = (int) (WIDTH / 12);
    static int x = 3, y = 2, stage = 1, yprev, xprev;
    static int turn = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Media sound = new Media(new File("sounds\\Human1.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();


        Field field = new Field();
        Group root = new Group();
        Image background = new Image("bg2.jpg");
        ImageView iv = new ImageView(background);
        root.getChildren().addAll(iv);
        for (Solder x : field.team[0]
                ) {
            root.getChildren().addAll(x.getIv(), x.getTx());
        }
        for (Solder x : field.team[1]
                ) {
            root.getChildren().addAll(x.getIv(), x.getTx());
        }


        Rectangle[][] map = new Rectangle[9][12];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 12; j++) {
                map[i][j] = new Rectangle();
                map[i][j].setHeight(CHEIDTH);
                map[i][j].setWidth(CWIDTH);
                map[i][j].setLayoutX(j * CWIDTH);
                map[i][j].setLayoutY(0.15 * HEIGHT + i * CHEIDTH);
                map[i][j].setStroke(Color.TRANSPARENT);
                map[i][j].setFill(Color.TRANSPARENT);
                root.getChildren().add(map[i][j]);
            }
        }

        Line[] hor = new Line[11];
        Line[] ver = new Line[12];
        for (int i = 0; i < 11; i++) {
            hor[i] = new Line();
            hor[i].setStroke(Color.LIGHTGREEN);
            hor[i].setStrokeWidth(3);
            hor[i].setStartX(0);
            hor[i].setStartY(HEIGHT * 0.15 + i * CHEIDTH);
            hor[i].setEndX(WIDTH);
            hor[i].setEndY(HEIGHT * 0.15 + i * CHEIDTH);
            root.getChildren().add(hor[i]);
        }
        for (int i = 0; i < 12; i++) {
            ver[i] = new Line();
            ver[i].setStroke(Color.LIGHTGREEN);
            ver[i].setStrokeWidth(3);
            ver[i].setStartX(i * CWIDTH);
            ver[i].setStartY(HEIGHT * 0.15);
            ver[i].setEndX(i * CWIDTH);
            ver[i].setEndY(HEIGHT);
            root.getChildren().add(ver[i]);
        }
        //primaryStage.setResizable(false);
        Rectangle cursor = new Rectangle();
        cursor.setHeight(CHEIDTH);
        cursor.setWidth(CWIDTH);
        cursor.setLayoutX(CWIDTH * 3);
        cursor.setLayoutY(CHEIDTH * 2 + HEIGHT * 0.15);
        cursor.setStroke(Color.LIGHTBLUE);
        cursor.setFill(Color.web("lightblue", 0.37));

        Alert nextTurn = new Alert(Alert.AlertType.INFORMATION);
        nextTurn.setHeaderText("Next turn");

        Alert atk = new Alert(Alert.AlertType.INFORMATION);
        atk.setHeaderText("The attack was successful!");

        Alert ctrAtk = new Alert(Alert.AlertType.INFORMATION);
        ctrAtk.setHeaderText("The counterattack was successful!");

        Alert end = new Alert(Alert.AlertType.INFORMATION);
        end.setHeaderText("Congratulation!");

        Alert alreadyMoved = new Alert(Alert.AlertType.ERROR);
        alreadyMoved.setHeaderText("Character has attacked already!");

        // if (stage == 1) {
        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case E:
                    if (stage == 1) {
                        turn = (turn + 1) % 2;
                        nextTurn.show();
                    }
                    break;
                case W:
                    if (y > 0) {
                        cursor.setLayoutY(cursor.getLayoutY() - CHEIDTH);
                        y--;
                        //System.out.println(x + "/" + y);
                    }
                    break;
                case A:
                    if (x > 0) {
                        cursor.setLayoutX(cursor.getLayoutX() - CWIDTH);
                        x--;
                        // System.out.println(x + "/" + y);
                    }
                    break;
                case S:
                    if (y < 8) {
                        cursor.setLayoutY(cursor.getLayoutY() + CHEIDTH);
                        y++;
                        // System.out.println(x + "/" + y);
                    }
                    break;
                case D:
                    if (x < 11) {
                        x++;
                        cursor.setLayoutX(cursor.getLayoutX() + CWIDTH);
                        //System.out.println(x + "/" + y);
                    }
                    break;
                case ESCAPE:
                    if (stage == 2 || stage == 3) {
                        for (Rectangle[] x : map
                                ) {
                            for (Rectangle y : x
                                    ) {
                                y.setFill(Color.TRANSPARENT);
                            }

                        }
                        if (stage == 3) {
                            field.map[yprev][xprev].setActive(false);
                        }

                        stage = 1;

                    }

                    if (check(field.team[turn])) {
                        turn = (turn + 1) % 2;
                        nextTurn.show();
                        for (Solder x : field.team[0]
                                ) {
                            x.setActive(true);
                        }
                        for (Solder x : field.team[1]
                                ) {
                            x.setActive(true);
                        }

                    }
                    break;
                case ENTER:
                    if (field.map[y][x] != null && stage == 1 && field.map[y][x].isActive() && field.map[y][x].getTeam() == turn) {
                        if (field.map[y][x].getClass().getSimpleName().equals("SwordsMan")) {
                            String file;
                            if (field.map[y][x].getTeam() == 0) {
                                file = "sounds\\SM\\" + (int) (Math.random() * 11) + ".wav";
                            } else {
                                file = "sounds\\ORC\\" + (int) (Math.random() * 7) + ".wav";
                            }

                            Media sound1 = new Media(new File(file).toURI().toString());
                            MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                            mediaPlayer1.play();
                        } else {
                            String file;
                            if (field.map[y][x].getTeam() == 0) {
                                file = "sounds\\ARCH\\" + (int) (Math.random() * 10) + ".wav";
                            } else {
                                file = "sounds\\SH\\" + (int) (Math.random() * 5) + ".wav";
                            }
                            Media sound1 = new Media(new File(file).toURI().toString());
                            MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                            mediaPlayer1.play();
                        }
                    }
                    if (stage == 1) {
                        if (field.map[y][x] != null && field.map[y][x].getTeam() == turn) {
                            if (field.map[y][x].isActive()) {
                                yprev = y;
                                xprev = x;
                                ArrayList<Rectangle> queue = new ArrayList<Rectangle>();
                                queue.add(map[y][x]);
                                map[y][x].setFill(Color.web("lightgreen", 0.37));
                                int x1, y1;
                                while (!queue.isEmpty()) {
                                    x1 = (int) Math.round(queue.get(0).getLayoutX() / CWIDTH);
                                    y1 = (int) Math.round((queue.get(0).getLayoutY() - HEIGHT * 0.15) / CHEIDTH);
                                    if (y1 - 1 >= 0) {
                                        if (!map[y1 - 1][x1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance() && field.map[y1 - 1][x1] == null) {
                                            map[y1 - 1][x1].setFill(Color.web("lightgreen", 0.37));
                                            queue.add(map[y1 - 1][x1]);
                                        }
                                        if (field.map[y1 - 1][x1] != null && !(y1 - 1 == yprev && x1 == xprev) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance()) {
                                            map[y1 - 1][x1].setFill((Color.web("pink", 0.37)));
                                        }
                                    }
                                    if (y1 + 1 <= 8) {
                                        if (!map[y1 + 1][x1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance() && field.map[y1 + 1][x1] == null) {
                                            map[y1 + 1][x1].setFill(Color.web("lightgreen", 0.37));
                                            queue.add(map[y1 + 1][x1]);
                                        }
                                        if (field.map[y1 + 1][x1] != null && !(y1 + 1 == yprev && x1 == xprev) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance()) {
                                            map[y1 + 1][x1].setFill((Color.web("pink", 0.37)));
                                        }
                                    }
                                    if (x1 - 1 >= 0) {
                                        if (!map[y1][x1 - 1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance() && field.map[y1][x1 - 1] == null) {
                                            map[y1][x1 - 1].setFill(Color.web("lightgreen", 0.37));
                                            queue.add(map[y1][x1 - 1]);
                                        }
                                        if (field.map[y1][x1 - 1] != null && !(y1 == yprev && x1 - 1 == xprev) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance()) {
                                            map[y1][x1 - 1].setFill((Color.web("pink", 0.37)));
                                        }
                                    }
                                    if (x1 + 1 <= 11) {
                                        if (!map[y1][x1 + 1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance() && field.map[y1][x1 + 1] == null) {
                                            map[y1][x1 + 1].setFill(Color.web("lightgreen", 0.37));
                                            queue.add(map[y1][x1 + 1]);
                                        }
                                        if (field.map[y1][x1 + 1] != null && !(y1 == yprev && x1 + 1 == xprev) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getMoveDistance()) {
                                            map[y1][x1 + 1].setFill((Color.web("pink", 0.37)));
                                        }
                                    }
                                    queue.remove(0);

                                }
                                stage = 2;
                            } else {
                                alreadyMoved.show();
                            }
                        }
                        break;
                    }
                    if (stage == 2 && map[y][x].getFill().toString().equals("0x90ee905e")) {
                        // if (field.map[y][x] == null && Math.abs(x - xprev) + Math.abs(y - yprev) < field.map[yprev][xprev].getMoveDistance()) {
                        if (map[y][x].getFill().toString().equals("0x90ee905e")) {
                            field.map[yprev][xprev].setIvLayout(x, y);
                            if (x != xprev || y != yprev) {
                                field.map[y][x] = field.map[yprev][xprev];
                                field.map[yprev][xprev] = null;

                            }
                            for (Rectangle[] x : map
                                    ) {
                                for (Rectangle y : x
                                        ) {
                                    y.setFill(Color.TRANSPARENT);
                                }

                            }

                        }

                        yprev = y;
                        xprev = x;
                        ArrayList<Rectangle> queue = new ArrayList<Rectangle>();
                        queue.add(map[y][x]);
                        map[y][x].setFill(Color.web("pink", 0.37));
                        int x1, y1;
                        while (!queue.isEmpty()) {
                            x1 = (int) Math.round(queue.get(0).getLayoutX() / CWIDTH);
                            y1 = (int) Math.round((queue.get(0).getLayoutY() - HEIGHT * 0.15) / CHEIDTH);
                            if (y1 - 1 >= 0) {
                                if (!map[y1 - 1][x1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getAttackDistance()) {
                                    map[y1 - 1][x1].setFill(Color.web("pink", 0.37));
                                    queue.add(map[y1 - 1][x1]);
                                }

                            }
                            if (y1 + 1 <= 8) {
                                if (!map[y1 + 1][x1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getAttackDistance()) {
                                    map[y1 + 1][x1].setFill(Color.web("pink", 0.37));
                                    queue.add(map[y1 + 1][x1]);
                                }

                            }
                            if (x1 - 1 >= 0) {
                                if (!map[y1][x1 - 1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getAttackDistance()) {
                                    map[y1][x1 - 1].setFill(Color.web("pink", 0.37));
                                    queue.add(map[y1][x1 - 1]);
                                }

                            }
                            if (x1 + 1 <= 11) {
                                if (!map[y1][x1 + 1].getFill().equals(map[y][x].getFill()) && Math.abs(x - x1) + Math.abs(y - y1) < field.map[y][x].getAttackDistance()) {
                                    map[y1][x1 + 1].setFill(Color.web("pink", 0.37));
                                    queue.add(map[y1][x1 + 1]);
                                }

                            }
                            queue.remove(0);


                        }
                        map[yprev][xprev].setFill(Color.TRANSPARENT);
                        stage = 3;
                    }
                    if (stage == 3) {
                        //0xffc0cb5e
                        if (map[y][x].getFill().toString().equals("0xffc0cb5e")) {
                            if (map[y][x].getFill().toString().equals("0xffc0cb5e")) {
                                if (field.map[y][x] == null) {
                                    atk.setContentText(field.map[yprev][xprev].getClass().getSimpleName() + " successfully attacked the GRASS!");
                                    atk.showAndWait();
                                } else {
                                    int a;
                                    a = field.map[yprev][xprev].doDamage();
                                    field.map[y][x].minusHP(a);
                                    field.map[y][x].updateTX();
                                    if (field.map[y][x].getHP() <= 0) {
                                        atk.setContentText(field.map[yprev][xprev].getClass().getSimpleName() + " successfully attacked this " + field.map[y][x].getClass().getSimpleName() + "gave " + a + " damage and killed him!");
                                        field.map[y][x].getIv().setVisible(false);
                                        field.map[y][x].getTx().setVisible(false);
                                        field.team[(turn + 1) % 2].remove(field.map[y][x]);
                                        field.map[y][x] = null;
                                        if (field.team[(turn + 1) % 2].isEmpty()) {
                                            end.setContentText("Player " + (turn + 1) + "win!");
                                            end.show();
                                        }
                                        atk.showAndWait();
                                    } else {
                                        atk.setContentText(field.map[yprev][xprev].getClass().getSimpleName() + " successfully attacked this " + field.map[y][x].getClass().getSimpleName() + "and gave " + a + " damage");
                                        atk.showAndWait();
                                        if (field.map[y][x].getClass().getSimpleName().equals("SwordsMan") && Math.abs(x - xprev) + Math.abs(y - yprev) <= 1) {
                                            a = field.map[y][x].doDamage();
                                            field.map[yprev][xprev].minusHP(a);
                                            field.map[yprev][xprev].updateTX();
                                            if (field.map[yprev][xprev].getHP() <= 0) {
                                                ctrAtk.setContentText(field.map[y][x].getClass().getSimpleName() + " successfully counterattacked your " + field.map[yprev][xprev].getClass().getSimpleName() + "gave " + a + " damage and killed your unit");
                                                field.map[yprev][xprev].getIv().setVisible(false);
                                                field.map[yprev][xprev].getTx().setVisible(false);
                                                field.team[turn].remove(field.map[yprev][xprev]);
                                                field.map[yprev][xprev] = null;
                                                if (field.team[turn].isEmpty()) {
                                                    end.setContentText("Player " + (turn) + "win!");
                                                    end.show();
                                                }
                                                ctrAtk.show();
                                            } else {
                                                ctrAtk.setContentText(field.map[y][x].getClass().getSimpleName() + " successfully counterattacked your " + field.map[yprev][xprev].getClass().getSimpleName() + "and gave " + a + " damage");
                                                ctrAtk.show();
                                            }
                                        }

                                    }
                                }
                                stage = 1;
                                for (Rectangle[] x : map
                                        ) {
                                    for (Rectangle y : x
                                            ) {
                                        y.setFill(Color.TRANSPARENT);
                                    }

                                }
                                if (field.map[yprev][xprev] != null) {
                                    field.map[yprev][xprev].setActive(false);
                                }
                                if (check(field.team[turn])) {
                                    for (Solder x : field.team[0]
                                            ) {
                                        x.setActive(true);
                                    }
                                    for (Solder x : field.team[1]
                                            ) {
                                        x.setActive(true);
                                    }
                                    turn = (turn + 1) % 2;
                                    nextTurn.show();
                                }
                            } else {

                            }
                        }

                    }

            }
        });


        root.getChildren().addAll(cursor);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        root.requestFocus();
        primaryStage.show();


    }

    public static boolean check(ArrayList<Solder> x) {
        for (Solder y : x
                ) {
            if (y.isActive()) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
