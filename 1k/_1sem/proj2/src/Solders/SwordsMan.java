package Solders;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Created by Роберт on 20.12.2016.
 */
public class SwordsMan extends  Solder{
    public SwordsMan(int team){
        this.hp = 150;
        this.maxHP = 150;
        this.attackDistance = 1;
        this.moveDistance = 4;
        this.dmg = 45;
        this.team = team;
        if (team == 0) {
            this.image = new Image("swordman(1).png");
        }
        else{
            this.image = new Image("orc.png");
        }
        this.iv = new ImageView(image);
        this.tx = new Text();
        this.tx.setText("" + hp);
        this.tx.setStyle("-fx-font-size: 18");
        this.tx.setFill(Color.GREENYELLOW);
        this.active = true;
    }
}
