package Solders;

import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import sample.Main;


/**
 * Created by Роберт on 20.12.2016.
 */
public class Solder {
    protected int hp, dmg, attackDistance = 1, moveDistance, team;
    protected Image image;
    protected ImageView iv;
    protected Text tx;
    protected boolean active;
    protected int maxHP;

    public Image getImage() {
        return image;
    }

    public int getMoveDistance() {
        return moveDistance;
    }

    public int getAttackDistance() {
        return attackDistance;
    }

    public ImageView getIv() {
        return iv;
    }

    public void setIvLayout(int x, int y) {
        tx.setLayoutX(Main.CWIDTH * x);
        tx.setLayoutY(Main.HEIGHT * 0.15 + Main.CHEIDTH * y + 15);
        iv.setLayoutY(Main.HEIGHT * 0.15 + Main.CHEIDTH * y);
        iv.setLayoutX(Main.CWIDTH * x);
    }

    public int doDamage() {
        return (int) ((dmg * (Math.random() * 0.5 + 0.75)) * (double) (hp / (maxHP + 0.0)));
    }

    public void minusHP(int dmg) {

        this.hp -= dmg;

    }

    public int getHP() {
        return hp;
    }

    public Text getTx() {
        return tx;
    }

    public void updateTX() {
        this.tx.setText("" + hp);
    }

    public boolean isActive() {
        return active;
    }

    public int getTeam() {
        return team;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
