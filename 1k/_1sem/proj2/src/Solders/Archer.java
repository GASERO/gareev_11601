package Solders;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;


/**
 * Created by Роберт on 20.12.2016.
 */
public class Archer extends Solder {
    public Archer(int team) {
        this.hp = 100;
        this.maxHP = 100;
        this.attackDistance = 3;
        this.moveDistance = 3;
        this.dmg = 35;
        this.team = team;
        if (team == 0) {
            this.image = new Image("bowman.png");
        } else {
            this.image = new Image("warlock.png");
        }
        this.iv = new ImageView(image);
        this.tx = new Text();
        this.tx.setText("" + hp);
        this.tx.setStyle("-fx-font-size: 18");
        this.tx.setFill(Color.GREENYELLOW);
        this.active = true;
    }


}
