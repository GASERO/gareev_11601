package sample;

import javafx.application.Application;
import javafx.beans.binding.IntegerBinding;
import javafx.geometry.NodeOrientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import static sample.CheckWithRegEx.check;

public class Main extends Application {
    final int HEIGHT = 500;
    final int WIDTH = 500;
    static ArrayList<String> words = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception{
        gameStart();
        Group root = new Group();
        primaryStage.setTitle("Goroda");

        TextArea history = new TextArea();
        history.setPrefSize(WIDTH - 40, HEIGHT  *0.85 - 60);
        history.setLayoutX(20);
        history.setLayoutY(40);
        history.setEditable(false);
        history.setWrapText(true);
        history.setFont(Font.font("Algerian",15 ));
        MenuBar menu = new MenuBar();
        Menu menuGame = new Menu("Game");
        menu.getMenus().add(menuGame);

        MenuItem ng = new MenuItem("New Game");
        ng.setOnAction(event -> {
            history.setText("");
            gameStart();

        });
        menuGame.getItems().add(ng);


        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText("Incorrect text!");
        error.setContentText("Incorrect name. Please write existing russian city ");

        Alert citynotfound = new Alert(Alert.AlertType.INFORMATION);
        citynotfound.setHeaderText("You can't chose this city");
        citynotfound.setContentText("Try too found a typo in word or add this name to list of cities. May be this city has already chosen.");

        TextField input = new TextField();
        input.setLayoutY(HEIGHT *0.85);
        input.setLayoutX(20);
        input.setPrefSize(WIDTH / 2,HEIGHT / 10);

        Button b = new Button("Enter");
        b.setPrefSize(WIDTH / 5, HEIGHT / 10);
        b.setLayoutX(WIDTH / 1.7);
        b.setLayoutY(HEIGHT * 0.85);
        b.setOnAction(event -> {
            if(check(input.getText())){
                if (words.contains(input.getText())) {
                    history.setText("you: " + input.getText() + "\r\n" + history.getText());
                    words.remove(input.getText());
                    String a = input.getText().trim();
                    char c = a.charAt(a.length()-1);
                    for (String x: words){
                        if (x.toLowerCase().charAt(0) == c ){
                            history.setText("PC: " + x + "\r\n" + history.getText());
                            words.remove(x);
                            break;
                        }
                    }
                    input.setText("");
                }
                else {
                    citynotfound.showAndWait();
                }
            }
            else error.showAndWait();
        });

        root.getChildren().addAll(b, history,input,menu);
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }
    public static void gameStart(){
        words.clear();
        try {
            Scanner sc = new Scanner(new File("list.txt"));
            while (sc.hasNextLine()) {
                words.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Word list wasn't found");
            a.showAndWait();
        }
    }



    public static void main(String[] args) {
        launch(args);
    }
}
