package sample;

        import java.util.regex.Matcher;
        import java.util.regex.Pattern;

public class CheckWithRegEx {
    public static boolean check(String string) {
        string = string.trim();
        string += " ";
        Pattern p = Pattern.compile("([А-Я][а-я]+[ \\-])+");
        Matcher m = p.matcher(string);
        return m.matches();
    }

    public static void main(String[] args) {
        System.out.println(check(""));
    }
}