public class Task13{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		byte meter = 0;
		while (n > 0){
			int digit = n % 10;
			if (digit == k){
				meter++;
			}
			n /=10;
		}
		System.out.print(meter);
	}
}