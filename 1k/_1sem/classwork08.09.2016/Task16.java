public class Task16{
	public static void main(String[] args){
		double x = Double.parseDouble(args [0]);
		double y = Double.parseDouble(args [1]);
		double z = Double.parseDouble(args [2]);
		x += 2;
		x *= y;
		x -= z;
		x /= y;
		y *= z;
		x += y;
		System.out.print(x);
	}
}