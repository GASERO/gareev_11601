public class Task08{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		double result = 0;
		int i = 1;
	
		while (i < n){
			double x = (double)(i * i) / ((i+1) * (i+1));
			//System.out.println(x);
			result += x;
			i = i + 2;
		}
	
		System.out.print(result);

	}

}