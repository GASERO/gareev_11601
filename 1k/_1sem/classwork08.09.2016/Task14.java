public class Task14{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		int result = 1;
		int i = 10;
		while (n > 0){
			int digit = n % 10;
			result += digit * i;
			i *= 10;
			n /= 10;
		}
		System.out.print(result);
	}
}