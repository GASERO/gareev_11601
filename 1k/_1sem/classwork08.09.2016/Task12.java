public class Task12{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		byte result = 0;
		while (n > 0){
			result += (n % 10);
			n /=10;
		}
		System.out.print(result);
	}
}