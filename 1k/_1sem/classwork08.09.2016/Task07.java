public class Task07{
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		double n = Double.parseDouble(args[1]);
		byte k = 0;
		if (n < 0 ){
			n = -n;
			k ++;
		}
		long result = 1;
		for (int i = 1; i <= n; i ++){
			result *= x;
			
		}
		switch (k){
			case 0: 
				System.out.print(result);
				break;
			case 1:
				System.out.print(1.0/result);
				break;
		}
	}
}