public class Task15{
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		int result = 0;
		int i = 1;
		while (n > 0 ){
			int digit = n%10;
			if (digit != k){
				result += digit*i;
				i *=10;
			}
			n /=10;
		}
		System.out.print(result);
	}
}