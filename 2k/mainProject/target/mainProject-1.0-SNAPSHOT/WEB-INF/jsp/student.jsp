<%--
  Created by IntelliJ IDEA.
  User: Роберт
  Date: 28.09.2017
  Time: 9:08
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Студент</title>
</head>
<body>
<form method=POST>
    <p>Name:</p>
    <p><input required pattern="[A-z]\w{1,10}" type=text name=name title="Name" value = ${student.name}></p>
    <p>Age:</p>
    <p><input required pattern="[1-9]|([1-9]\d)" type=text name=age title="Age" value = ${student.age}></p>
    <p>Group:</p>
    <p><input required type=text name=group title="Group" value = ${student.group}></p>
    <p><input type=submit value="Edit"></p>
</form>
</body>
</html>