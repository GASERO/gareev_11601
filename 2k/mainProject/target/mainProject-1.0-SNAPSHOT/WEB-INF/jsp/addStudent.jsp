<%--
  Created by IntelliJ IDEA.
  User: Роберт
  Date: 01.10.2017
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Добавить студента</title>
</head>
<body>
<form method=POST>
    <p>Name:</p>
    <p><input required pattern="[A-z]\w{1,10}" type=text name=name title="Name"></p>
    <p>Age:</p>
    <p><input required pattern="[1-9]|([1-9]\d)" type=text name=age title="Age"></p>
    <p>Group:</p>
    <p><input required  type=text name=group title="Group"></p>
    <p><input type=submit value="Add"></p>
</form>
</body>
</html>
