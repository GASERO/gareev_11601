
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Студенты</title>
</head>
<body>
<form action = "student/add">
<p  ><input type = submit value = "Add"></p>
</form>
<table>
    <tr>
        <th>Имя</th>
        <%--<th>Возраст</th>
        <th>Группа</th>--%>
    </tr>
    <c:forEach items="${students}" var="student">
        <tr>

            <%--<td>
                <form action="student/${student.id}">
                    ${student.name}
                </form></td>--%>
            <%--<td>${student.age}</td>
            <td>${student.group}</td>--%>
                <td>
                    <a href="/students/${student.id}">${student.name}</a>
                </td>
            <td>
                <form action="/students/${student.id}/edit">
                    <p><input type="submit" value=edit></p>
                </form>
            </td>
            <td>
                <form action="/students/${student.id}/delete">
                    <p><input type="submit" name="" value=delete></p>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>