package Models;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class User {
    Long id;
    String login;
    String password;
}
