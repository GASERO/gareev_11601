package Models;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Student {
    Long id;
    String name;
    int age;
    String group;
}
