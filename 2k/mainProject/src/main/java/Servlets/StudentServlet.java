package Servlets;

import Dao.StudentDao;
import Dao.StudentDaoTemplateImpl;
import Models.Student;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class StudentServlet extends HttpServlet {
    StudentDao studentDao;
    Student student;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String group = request.getParameter("group");
        int age =Integer.parseInt( request.getParameter("age"));
        student.setName(name);
        student.setAge(age);
        student.setGroup(group);
        studentDao.update(student.getId(), student);
       // response.sendRedirect("/students/" + student.getId());
        System.out.println(student.getAge());
        response.sendRedirect("/students");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String)request.getSession().getAttribute("current_user");
        if (current_user == null){
            response.sendRedirect("/login");
        }

        String pathInfo = request.getPathInfo(); // /{value}/test
        String[] pathParts = pathInfo.split("/");
        String id = pathParts[1];
        String command;
        student = studentDao.find((long)Integer.parseInt(id));
        request.setAttribute("student", student);
        if(pathParts.length >2){
            command= pathParts[2];
            if (command.equals("edit")) {
                request.getRequestDispatcher("/WEB-INF/jsp/student.jsp").forward(request, response);
            } else {
                if (command.equals("delete")) {
                    studentDao.delete(Long.parseLong(id));
                    response.sendRedirect("/students");
                    return;
                }

            }
        }else {
            request.getRequestDispatcher("/WEB-INF/jsp/student.jsp").forward(request, response);
        }

    }

    @Override
    public void init() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/cw");
        dataSource.setUsername("postgres");
        dataSource.setPassword("gasero");
        studentDao = new StudentDaoTemplateImpl(dataSource);
    }
}
