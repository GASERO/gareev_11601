package Servlets;

import Dao.StudentDao;
import Dao.StudentDaoTemplateImpl;
import Models.Student;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class AllStudentsServlet extends HttpServlet {
    StudentDao studentDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String)request.getSession().getAttribute("current_user");
        if (current_user == null){
            response.sendRedirect("/login");
            return;
        }
        List<Student> list = studentDao.findAll();

        request.setAttribute("students", list);
        request.getRequestDispatcher("/WEB-INF/jsp/allStudents.jsp").forward(request, response);
    }

    @Override
    public void init() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/cw");
        dataSource.setUsername("postgres");
        dataSource.setPassword("gasero");
        studentDao = new StudentDaoTemplateImpl(dataSource);
    }
}
