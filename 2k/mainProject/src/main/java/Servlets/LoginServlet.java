package Servlets;

import Dao.StudentDaoTemplateImpl;
import Dao.UserDao;
import Dao.UserDaoTemplateImpl;
import Models.User;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by NVYas on 20.09.2017.
 */
public class LoginServlet extends HttpServlet {
    UserDao userDao;

    private boolean check(String username, String password) {
        User user = User.builder()
                .login(username)
                .password(password)
                .build();
        return  userDao.contains(user);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (check(username, password)) {
            request.getSession().setAttribute("current_user", username);
            response.sendRedirect("/students");
        }else{
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String current_user = (String) request.getSession().getAttribute("current_user");
        if (current_user == null) {
            response.setContentType("text/html");
            response.getWriter().println("<form method=\"POST\"" +
                    "<p><input label = \"Login\" type = \"text\" name = \"username\"></p>" +
                    "<p><input label = \"Password\" type = \"password\" name = \"password\"></p>" +
                    "<p><input type = \"submit\" value = \"Log in\"></p>" +
                    "</form>");
        }else {
            response.sendRedirect("/students");
        }
    }
    @Override
    public void init() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/cw");
        dataSource.setUsername("postgres");
        dataSource.setPassword("gasero");
        userDao = new UserDaoTemplateImpl(dataSource);
    }
}
