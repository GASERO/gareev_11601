package Servlets;

import Dao.StudentDao;
import Dao.StudentDaoTemplateImpl;
import Models.Student;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class AddStudentServlet extends HttpServlet {
    StudentDao studentDao;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String group = request.getParameter("group");
        int age =Integer.parseInt( request.getParameter("age"));
        Student student = Student.builder()
                .age(age)
                .name(name)
                .group(group)
                .build();
        studentDao.save(student);
        response.sendRedirect("/students");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String)request.getSession().getAttribute("current_user");
        if (current_user == null){
            response.sendRedirect("/login");
            return;
        }
        request.getRequestDispatcher("/WEB-INF/jsp/addStudent.jsp").forward(request, response);
    }
    @Override
    public void init() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/cw");
        dataSource.setUsername("postgres");
        dataSource.setPassword("gasero");
        studentDao = new StudentDaoTemplateImpl(dataSource);
    }

}
