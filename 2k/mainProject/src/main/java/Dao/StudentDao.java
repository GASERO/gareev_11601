package Dao;

import Models.Student;

public interface StudentDao extends CrudDao<Student,Long> {
}
