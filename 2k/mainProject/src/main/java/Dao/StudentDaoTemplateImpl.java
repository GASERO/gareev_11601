package Dao;

import Models.Student;
import com.google.common.collect.Lists;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Models.Student;
import com.google.common.collect.Lists;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentDaoTemplateImpl implements StudentDao {
    private final static String SQL_INSERT = "INSERT INTO student(name,age,\"group\") values (?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM student WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM student";
    private static final String SQL_DELETE = "DELETE FROM student WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE student SET name = ?, age = ?, \"group\" = ? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Student> students;

    public StudentDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.students = new HashMap<Long, Student>();
    }


    private RowMapper<Student> studentRowMapper = (resultSet, rowNumber) -> {
        Long currentStudentId = resultSet.getLong(1);
        if (students.get(currentStudentId) == null) {
            students.put(currentStudentId, Student.builder()
                    .id(currentStudentId)
                    .name(resultSet.getString(2))
                    .age(resultSet.getInt(3))
                    .group(resultSet.getString(4))
                    .build());
        }
        return students.get(currentStudentId);
    };

    public void save(Student model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getName());
            preparedStatement.setInt(2, model.getAge());
            preparedStatement.setString(3, model.getGroup());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Student find(Long id) {
        Student result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, studentRowMapper).get(0);
        students.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Student model) {
        template.update(SQL_UPDATE, model.getName(), model.getAge(),model.getGroup(), id);

    }

    public List<Student> findAll() {
        template.query(SQL_SELECT_ALL, studentRowMapper);
        List<Student> result = Lists.newArrayList(students.values());
        students.clear();
        return result;
    }

}
