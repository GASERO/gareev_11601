package Dao;

import Models.User;

public interface UserDao extends CrudDao<User,Long> {
    Boolean contains(User user);
}
