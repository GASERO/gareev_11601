package Dao;

import Models.User;
import com.google.common.collect.Lists;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDaoTemplateImpl implements UserDao {
    private final static String SQL_INSERT = "INSERT INTO \"user\"(login,password) values (?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM \"user\" WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM\"user\"";
    private static final String SQL_DELETE = "DELETE FROM \"user\" WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE \"user\" SET login = ?, password = ? WHERE id = ?";
    private static final String SQL_CONTAINS = "select * from \"user\" where  \"login\" = ? and \"password\" = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, User> users;

    public UserDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.users = new HashMap<Long, User>();
    }


    private RowMapper<User> userRowMapper = (resultSet, rowNumber) -> {
        Long currentUserId = resultSet.getLong(1);
        if (users.get(currentUserId) == null) {
            users.put(currentUserId, User.builder()
                    .id(currentUserId)
                    .login(resultSet.getString(2))
                    .password(resultSet.getString(3))
                    .build());
        }
        return users.get(currentUserId);
    };

    public void save(User model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getLogin());
            preparedStatement.setString(2, model.getPassword());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public User find(Long id) {
        User result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, userRowMapper).get(0);
        users.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, User model) {
        template.update(SQL_UPDATE, model.getLogin(), model.getPassword(), id);

    }

    public List<User> findAll() {
        template.query(SQL_SELECT_ALL, userRowMapper);
        List<User> result = Lists.newArrayList(users.values());
        users.clear();
        return result;
    }

    @Override
    public Boolean contains(User user) {
       /* User result = template.query(SQL_CONTAINS,new String[] {user.getPassword(),user.getLogin()},userRowMapper).get(0);
        users.clear();
        if (result == null){
            return false;
        }
        return  true;*/
       return   !template.query(SQL_CONTAINS,new String[] {user.getPassword(),user.getLogin()},userRowMapper).isEmpty();
    }
}
