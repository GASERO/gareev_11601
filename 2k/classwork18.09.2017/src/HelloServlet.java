import java.io.IOException;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String name = request.getParameter("name");
        response.getWriter().println("<h1>"+ name+", CocaTb</h1>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.getWriter().println
                ("<form action=\"\" method = \"post\" accept-charset=\"UTF-8 \"> <p><input type =\"text\" name = \"name\" value = \"Введите имя\"</p>" +
                        "<p><input type=\"submit\" value=\"Отправить\"></p>" +
                        "</form>" +
                        "");
    }
}
