import java.io.IOException;

public class CalcServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.getWriter().println(
                ("<form action=\"answer\" method = \"get\">\n" +
                        "<p><input type = \"text\" name = \"var1\" value = \"1st argument\"</p>\n" +
                        "<p><input type = \"text\" name = \"var2\" value = \"2nd argument\"</p>\n" +
                        "<p><select  name = \"oper\">\n" +
                        "<option name = \"plus\">+</option>\n" +
                        "<option name = \"minus\">-</option>\n" +
                        "<option name = \"mult\">*</option>\n" +
                        "<option name = \"div\">/</option>\n" +
                        "</select>\n" +
                        "<p><input type = \"submit\" value = \"calc\"></p>")
        );
    }
}
