import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "AnswerServlet")
public class AnswerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean allIsOK = true;
        String svar1 = request.getParameter("var1");
        String svar2 = request.getParameter("var2");
        String oper = request.getParameter("oper");
        Double var1 = 0.0, var2 = 0.0;
        try {

           var1 =Double.parseDouble(svar1);
        }catch (Exception e){
            allIsOK = false;
            response.getWriter().println("var1 error");
        }
        try {
            var2 =Double.parseDouble((svar2));
        }catch (Exception e){
            allIsOK = false;
            response.getWriter().println("var2 error");
        }
        double result = 0;
        if(allIsOK){
            switch (oper){
                case "+":  result = var1 + var2;
                    break;
                case "-":  result = var1 - var2;
                    break;
                case "*":  result = var1 * var2;
                    break;
                case "/":  result = var1 / var2;
                    break;
            }response.getWriter().println("<p>" + result+"</p>");
        }else{
            response.getWriter().println("<p> error</p>");
        }


    }
}
