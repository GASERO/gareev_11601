package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by NVYas on 20.09.2017.
 */
public class SecretServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String current_user = (String)request.getSession().getAttribute("current_user");
        if (current_user != null){
            response.getWriter().println("Hello " + current_user);
        }
        else {
            response.sendRedirect("/login");
        }
    }
}
