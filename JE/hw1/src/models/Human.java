package models;

public class Human {
  private   String name;
  private   long id;
  private   int age;
  private   String color;

  Human(Builder builder){
      this.name = builder.name;
      this.id = builder.id;
      this.age = builder.age;
      this.color = builder.color;
  }


  public static class Builder{
      private   String name;
      private   long id;
      private   int age;
      private   String color;

      public  Builder id(Long id){
          this.id = id;
          return this;
      }

      public  Builder name(String name){
          this.name = name;
          return this;
      }

      public  Builder color(String color){
          this.color = color;
          return this;
      }
      public Builder age(int age){
          this.age = age;
          return this;
      }
      public Human build(){
          return new Human(this);
      }
  }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (id != human.id) return false;
        if (age != human.age) return false;
        if (!name.equals(human.name)) return false;
        return color != null ? color.equals(human.color) : human.color == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + age;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public Human(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
