package DAO;

import models.Human;

public interface HumanDAO extends CrudDAO<Human,Long> {
}
