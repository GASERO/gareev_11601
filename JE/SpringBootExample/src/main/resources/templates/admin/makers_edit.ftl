<head>
<#--<link href="/css/style.css" rel="stylesheet"/>-->
</head>
<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey;}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Country</th>
    </tr>
<#list model.makers as maker>
    <tr>
        <td>${maker.id}</td>
        <td>${maker.name}</td>
        <td>${maker.country}</td>
    </tr>
</#list>
</table>


<h3>Add new:</h3>

<div class="container">
    <div class="row">
        <div class="form-login">
            <form class="form-horizontal" action="/admin/makers/add" method="post">
                <input type="text" class="form-control" id="name" name="name" placeholder="name">
                </br>
                <input type="text" class="form-control" id="countryr" name="country" placeholder="country">
                </br>
                <input type="submit">

            </form>
        </div>
    </div>
</div>
</body>
