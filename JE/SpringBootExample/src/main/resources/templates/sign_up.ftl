<#ftl encoding='UTF-8'>
<head>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
              integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
                integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
                integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
                integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
                crossorigin="anonymous"></script>

    </head>
</head>
<body style="background-color: #f5f5f5">
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse sticky-top">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://localhost:8080/main">HW Shop</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <div class="dropdown" style="padding-top: 2px">
                <button class="btn btn-secondary dropdown-toggle " type="button"
                        style="background-color:#292b2c;color: rgba(255,255,255,0.5);border-color:#292b2c "
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Products
                </button>

                <div class="dropdown-menu" style="background-color:#292b2c" aria-labelledby="dropdownMenu2">
                    <ul>
                        <li><a class="nav-link" href="http://localhost:8080/items/cpus">CPUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/motherboards">Motherboards</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/psus">PSUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/rams">RAMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/roms">ROMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/videoCards">Video cards</a></li>
                    </ul>

                </div>
            </div>


            <li class="nav-item">
                <a class="nav-link" href="">Shipping</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">Payment</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">About</a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/items/search" method="get"><input class="form-control mr-sm-2"
                                                                                          type="text" id="search"
                                                                                          name="search"
                                                                                          placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        <a href="/cart">
            <img style="border-radius: 5px; " src="http://localhost:8080/files/e5e98891-e2c7-44e7-ae7d-a33759ee84b4.png" height="38" width="38">
        </a>

        <div class="navbar-nav ">


            <div class="nav-item ">
                <a class="nav-link active" href="http://localhost:8080/login">Log in <span
                        class="sr-only">(current)</span></a>
            </div>
        </div>
    </div>

</nav>
<#if error??>
<div class="alert alert-danger" role="alert">${error}</div>
</#if>

<div class="container">
    <div class="row">
        <div class="offset-md-4 col-md-3" style="padding-top: 100px">
            <div class="form-login">
                <form class="form-horizontal" action="/signUp" method="post">
                    <h4 style="margin-left: 40px">Hello</h4>
                    <input type="text" required name="login" class="form-control input-sm chat-input"
                           placeholder="Логин">
                    </br>
                    <input type="password" required name="password" placeholder="Пароль"
                           class="form-control input-sm chat-input">
                    </br>
                    <input required pattern=".+@[\w\d]+\.[\w\d]+" name="email" class="form-control input-sm chat-input"
                           placeholder="email">
                    </br>
                    <button class="btn btn-primary" style="float: right;"> submit</button>


                </form>

            </div>

        </div>
    </div>
</div>


</body>