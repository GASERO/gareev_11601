<#ftl encoding='UTF-8'>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
          crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


</head>
<body style="background-color: #f5f5f5">
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse sticky-top">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://localhost:8080/main">HW Shop</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <div class="dropdown" style="padding-top: 2px">
                <button class="btn btn-secondary dropdown-toggle " type="button"
                        style="background-color:#292b2c;color: rgba(255,255,255,0.5);border-color:#292b2c "
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Products
                </button>

                <div class="dropdown-menu" style="background-color:#292b2c" aria-labelledby="dropdownMenu2">
                    <ul>
                        <li><a class="nav-link" href="http://localhost:8080/items/cpus">CPUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/motherboards">Motherboards</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/psus">PSUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/rams">RAMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/roms">ROMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/videoCards">Video cards</a></li>
                    </ul>

                </div>
            </div>


            <li class="nav-item">
                <a class="nav-link" href="">Shipping</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">Payment</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">About</a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/items/search" method="get">
            <input class="form-control mr-sm-2" type="text" id="search" name="search" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        <a href="/cart">
            <img style="border-radius: 5px; " src="http://localhost:8080/files/e5e98891-e2c7-44e7-ae7d-a33759ee84b4.png" height="38" width="38">
        </a>

        <div class="navbar-nav ">
            <div class="nav-item "><a class="nav-link" href="http://localhost:8080/user/profile">Profile <span
                    class="sr-only">(current)</span></a></div>        <#if model.aut??>

            <div class="nav-item ">
                <a class="nav-link" href="http://localhost:8080/logout">Log out <span
                        class="sr-only">(current)</span></a>
            </div>
        <#else>

            <div class="nav-item ">
                <a class="nav-link active" href="http://localhost:8080/login">Log in <span
                        class="sr-only">(current)</span></a>
            </div>
        </#if>
        </div>
    </div>

</nav>
</br>
<h1 style="margin-left: 10%">Searching results for "${model.request}":</h1>
<#if model.cpus??>
    <#list model.cpus as cpu>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${cpu.maker.name} ${cpu.model}</h3>
                    <img style="border-radius: 5px; " src="http://localhost:8080/files/${cpu.photo.storageFileName}"
                         height="200" width="200">
                </div>
                <div class="cl-lg-4 col-md-4" style="font-family:  Helvetica Neue
                , Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Socket: ${cpu.socket}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Speed: ${cpu.speed}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Core number: ${cpu.coreNumber}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Energy : ${cpu.energy}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${cpu.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${cpu.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${cpu.price}$</p>
                <button onclick="addToCart('cpu',${cpu.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>
</#if>
<#if model.motherboards??>

    <#list model.motherboards as mb>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${mb.maker.name} ${mb.model}</h3>
                    <img style="border-radius: 5px; " src="http://localhost:8080/files/${mb.photo.storageFileName}"
                         height="200" width="200">
                </div>
                <div class="cl-lg-8 col-md-8" style="font-family:  Helvetica Neue
                , Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Socket: ${mb.socket}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory Type: ${mb.memoryType}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Graphic
                    Proc: ${mb.igp?string('yes', 'no')}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Energy : ${mb.energy}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${mb.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${mb.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${mb.price}$</p>
                <button onclick="addToCart('motherboard',${mb.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>
</#if>
<#if model.psus??>
    <#list model.psus as psu>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${psu.maker.name} ${psu.model}</h3>
                    <img style="border-radius: 5px; " src="http://localhost:8080/files/${psu.photo.storageFileName}"
                         height="200" width="200">
                </div>
                <div class="cl-lg-4 col-md-4" style="font-family:  Helvetica Neue
                , Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Power: ${psu.power}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${psu.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${psu.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${psu.price}$</p>
                <button onclick="addToCart('psu',${psu.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>
</#if>
<#if model.rams??>
    <#list model.rams as ram>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${ram.maker.name} ${ram.model}</h3>
                    <img style="border-radius: 5px; " src="http://localhost:8080/files/${ram.photo.storageFileName}"
                         height="200" width="200">
                </div>
                <div class="cl-lg-4 col-md-4" style="font-family:  Helvetica Neue
                , Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory: ${ram.memory}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory Type: ${ram.memoryType}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Energy: ${ram.energy}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${ram.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${ram.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${ram.price}$</p>
                <button onclick="addToCart('ram',${ram.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>
</#if>
<#if model.roms??>
    <#list model.roms as rom>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${rom.maker.name} ${rom.model}</h3>
                    <img style="border-radius: 5px; " src="http://localhost:8080/files/${rom.photo.storageFileName}"
                         height="200" width="200">
                </div>
                <div class="cl-lg-4 col-md-4" style="font-family:  Helvetica Neue, Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory: ${rom.memory}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Type: ${rom.type}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Energy: ${rom.energy}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${rom.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${rom.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${rom.price}$</p>
                <button onclick="addToCart('rom',${rom.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>
</#if>
<#if model.videcards??>
    <#list model.videocards as videoCard>
    <div class="container">
        <div class="well" style="background: #fefffa; border-radius: 7px;">
            <div class="row"
                 style="margin-left: 50px;margin-right: 50px; margin-top: 15px;margin-bottom: 15px; padding-bottom: 15px; padding-top: 25px">
                <div class="col-lg-4 col-md-4">
                    <h3 style="color: gray;">${videoCard.maker.name} ${videoCard.model}</h3>
                    <img style="border-radius: 5px; "
                         src="http://localhost:8080/files/${videoCard.photo.storageFileName}" height="200" width="200">
                </div>
                <div class="cl-lg-4 col-md-4" style="font-family:  Helvetica Neue
                , Helvetica, Arial, sans-serif;">
                <h5 style="color: gray; text-transform: uppercase">Характеристики:</h5>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory: ${videoCard.memory}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Memory Type: ${videoCard.memoryType}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Speed: ${videoCard.speed}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Energy: ${videoCard.energy}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Maker : ${videoCard.maker.name}</p>
                <p style="margin: 0px; color: deepskyblue; font-size: 20px;">Country : ${videoCard.maker.country}</p>
            </div>
            <div class="cl-lg-4 col-md-4">
                <p style="margin: 0px; color: deepskyblue; font-size: 40px;">Price : ${videoCard.price}$</p>
                <button onclick="addToCart('videocard',${videoCard.id})" class="btn btn-primary"> BUY</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </#list>


</#if>


</body>

<script type="application/javascript">
    function addToCart(type, id) {
        $.ajax({
            url: '/cart/add',
            type: 'POST',
            data: {
                type: type,
                id: id
            },
            dataType: 'text',
            success: function (data) {

                if (data == "true") {
                    console.log("added");
                    /*$("#alertOk").attr('hidden', false);
                    $("#alertOk").fadeOut(3000);*/
                    alert('Successfully added');
                    /* s = document.getElementById("alertOk");
                     s.attr('hidden', false);
                     s.fadeOut;*/

                } else {
                    console.log("errror")
                }

            },
            error: function () {
                console.log("Ajax error")
            }
        });
    }


</script>