<#ftl encoding='UTF-8'>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
          crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
            integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

</head>
<body style="background-color: #f5f5f5">
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse sticky-top">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://localhost:8080/main">HW Shop</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <div class="dropdown" style="padding-top: 2px">
                <button class="btn btn-secondary dropdown-toggle " type="button"
                        style="background-color:#292b2c;color: rgba(255,255,255,0.5);border-color:#292b2c "
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Products
                </button>

                <div class="dropdown-menu" style="background-color:#292b2c" aria-labelledby="dropdownMenu2">
                    <ul>
                        <li><a class="nav-link" href="http://localhost:8080/items/cpus">CPUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/motherboards">Motherboards</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/psus">PSUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/rams">RAMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/roms">ROMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/videoCards">Video cards</a></li>
                    </ul>

                </div>
            </div>


            <li class="nav-item">
                <a class="nav-link" href="">Shipping</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">Payment</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">About</a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/items/search" method="get">
            <input class="form-control mr-sm-2" type="text" id="search" name="search" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        <a href="/cart">
            <img style="border-radius: 5px; " src="http://localhost:8080/files/861ad4b1-cd18-4de5-aaff-e33f8d7a78eb.png" height="38" width="38">
        </a>


        <div class="navbar-nav ">
            <div class="nav-item "><a class="nav-link" href="http://localhost:8080/user/profile">Profile <span
                    class="sr-only">(current)</span></a></div>
        <#if model.aut??>

            <div class="nav-item ">
                <a class="nav-link" href="http://localhost:8080/logout">Log out <span
                        class="sr-only">(current)</span></a>
            </div>
        <#else>

            <div class="nav-item ">
                <a class="nav-link" href="http://localhost:8080/login">Log in <span class="sr-only">(current)</span></a>
            </div>
        </#if>
        </div>
    </div>

</nav>
</br>

<div class="container col-lg-8 col-md-8 col-md-offset-2 col-lg-offset-2">
<table class="table table-hover table-bordered">

<#if model.items??>
    <thead>
    <tr>
        <th>#</th>
        <th>Model</th>
        <th>Maker</th>
        <th>Price</th>
        <th>Remove</th>


    </tr>
    </thead>
    <tbody class="demotable">
        <#list model.items as item>
        <tr>

            <td> <img style="border-radius: 5px; margin-right: 20px; margin-top: 10px; margin-bottom: 10px;" src="http://localhost:8080/files/${item.photo.storageFileName}"
                      height="100" width="100"> ${item.model}</td>
            <td>${item.maker.name}</td>
            <td>${item.price}$</td>
            <td><button onclick="removeFromCart('${item.getClass().getSimpleName()}',${item.id})" class="btn btn-primary"> Drop</button></td>
        </tr>
        </#list>
    </tbody>
</table>
<div style="margin-left: 75%">
    <h2>Total: ${model.total}$ </h2>
    <button disabled class="btn btn-primary">BUY</button>
</div>



<#else>
    Корзина пуста

</#if>
    </div>
<style>
    .demotable {
        border-collapse: collapse;
        counter-reset: schetchik;  /* счётчик с названием "schetchik" работает в рамках класса .demotable */
    }
    .demotable tr {
        counter-increment: schetchik;  /* при встрече тега tr счётчик с названием "schetchik" увеличивается на единицу */
    }
    .demotable td,
    .demotable tr:before {
        padding: .1em .5em;
        border: 1px solid #E7D5C0;
    }
    .demotable tr:before {
        content: counter(schetchik);  /* значение счётчика с названием "schetchik" записывается в первую клетку строки */
        display: table-cell;
        vertical-align: middle;
        color: #978777;
    }
</style>


</body>
<script type="application/javascript">
    function removeFromCart(type, id) {
        $.ajax({
            url: '/cart/remove',
            type: 'POST',
            data: {
                type: type,
                id: id
            },
            dataType: 'text',
            success: function (data) {

                if (data == "true") {
                    console.log("added");
                    /*$("#alertOk").attr('hidden', false);
                    $("#alertOk").fadeOut(3000);*/
                   // alert('Successfully added');
                    /* s = document.getElementById("alertOk");
                     s.attr('hidden', false);
                     s.fadeOut;*/

                } else {
                    console.log("errror")
                }

            },
            error: function () {
                console.log("Ajax error")
            }
        });
    }


</script>
