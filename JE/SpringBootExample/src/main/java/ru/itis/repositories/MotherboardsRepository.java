package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.Cpu;
import ru.itis.models.Motherboard;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface MotherboardsRepository extends JpaRepository<Motherboard, Long> {
    List<Motherboard> findAll();
    void delete(Long id);
    @Query("Select a from Motherboard as a where lower(a.model) like lower(:q) or lower(a.maker.name) like lower(:q)")
    List<Motherboard> searchInModelAndMaker(@Param("q") String q);
}
