package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAll();
    Optional<User> findOneByLogin(String login);

    List<User> findAllByRole(Role role);

    Optional<User> findById(Long userId);

    Optional<User> findByUuid(String uuid);
}
