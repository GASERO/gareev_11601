package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.CartNote;

public interface CartNoteRepository extends JpaRepository<CartNote,Long> {
}
