package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.Cpu;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface CpusRepository extends JpaRepository<Cpu, Long> {
    List<Cpu> findAll();

    @Query("Select a from Cpu as a where lower(a.model) like lower(:q) or lower(a.maker.name) like lower(:q)")
    List<Cpu> searchInModelAndMaker(@Param("q") String q);

    void delete(Long id);
}
