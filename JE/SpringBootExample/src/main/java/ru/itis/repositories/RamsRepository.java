package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.Cpu;
import ru.itis.models.Ram;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface RamsRepository extends JpaRepository<Ram, Long> {
    List<Ram> findAll();
    void delete(Long id);
    @Query("Select a from Ram as a where lower(a.model) like lower(:q) or lower(a.maker.name) like lower(:q)")
    List<Ram> searchInModelAndMaker(@Param("q") String q);
}
