package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.Cpu;
import ru.itis.models.Psu;

import java.util.List;


public interface PsusRepository extends JpaRepository<Psu, Long> {
    List<Psu> findAll();
    void delete(Long id);
    @Query("Select a from Psu as a where lower(a.model) like lower(:q) or lower(a.maker.name) like lower(:q)")
    List<Psu> searchInModelAndMaker(@Param("q") String q);
}
