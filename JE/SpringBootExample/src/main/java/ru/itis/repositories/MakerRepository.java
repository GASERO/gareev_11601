package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Cpu;
import ru.itis.models.Maker;
import ru.itis.models.User;
import ru.itis.security.role.Role;

import java.util.List;
import java.util.Optional;


public interface MakerRepository extends JpaRepository<Maker, Long> {
    Maker findOne(Long id);

}
