package ru.itis.models;

import lombok.*;


import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "ram")
public class Ram extends Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double price;
    private String model;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id")
    private FileInfo photo;

    private Integer memory;
    private String memoryType;

    private Integer energy;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "maker_id")
    private Maker maker;

    //private String email;

}