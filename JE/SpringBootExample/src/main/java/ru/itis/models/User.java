package ru.itis.models;

import lombok.*;
import ru.itis.security.role.Role;
import ru.itis.security.states.State;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String name2;

    private String phone;


 /*   @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
    private Set<Auto> autos;*/

    @Column(unique = true)
    private String login;

    @Column(unique = true)
    private String uuid;

    private String hashPassword;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private State state;

    private String hashTempPassword;

    private String email;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<CartNote> card;


}
