package ru.itis.models;

import lombok.*;


import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "cpu")
public class Cpu extends Item{

    private String model;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "maker_id")
    private Maker maker;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    private String socket;

    private Double speed;

    private Integer coreNumber;


    private Integer energy;
    private Double price;



    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id")
    private FileInfo photo;


}