package ru.itis.models;

import lombok.*;


import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "maker")
public class Maker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;


    private String country;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<Cpu> cpus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<Motherboard> motherboards;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<Psu> psus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<Ram> rams;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<Rom> roms;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "maker")
    private Set<VideoCard> videoCards;


}