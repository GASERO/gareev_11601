package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.UserInfoEditForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.Optional;


@Component
public class UserInfoEditFormValidator implements Validator {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(UserInfoEditForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserInfoEditForm form = (UserInfoEditForm) target;

        /*Optional<User> existedUser = usersRepository.findOneByLogin(form.getLogin());

        if (existedUser.isPresent()) {
            errors.reject("bad.login", "Логин занят");
        }
*/
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Incorrect name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name2", "empty.name2", "Incorrect 2nd name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "empty.phone", "Incorrect number");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "empty.mail", "Incrrect email");

    }
}
