package ru.itis.validators;

;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ru.itis.forms.VideoCardAddingForm;

import java.util.Optional;
@Component
public class VideoCardAddingFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(VideoCardAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {

    }
}