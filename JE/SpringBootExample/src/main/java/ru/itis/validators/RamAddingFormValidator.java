package ru.itis.validators;

;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ru.itis.forms.RamAddingForm;

import java.util.Optional;
@Component
public class RamAddingFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(RamAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {

    }
}