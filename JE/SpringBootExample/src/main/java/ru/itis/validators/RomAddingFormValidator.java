package ru.itis.validators;

;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ru.itis.forms.RomAddingForm;

import java.util.Optional;
@Component
public class RomAddingFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(RomAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {

    }
}