package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.AccountEditForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.Optional;


@Component
public class AccountEditFormValidator implements Validator {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(AccountEditForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {
        AccountEditForm form = (AccountEditForm) target;
        Optional<User> existedUser = usersRepository.findOneByLogin(form.getLogin());


        if (existedUser.isPresent()) {
            errors.reject("bad.login", "Login is reserved");
        }
        if (form.getPassword() != null && form.getPassword2() !=null) {
            if (!form.getPassword().equals(form.getPassword2())) {
                errors.reject("bad.pas", "Passwords are not the same");
            }
        }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "empty.login", "Empty login");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "empty.password", "Empty password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password2", "empty.password", "Empty password");

    }
}