package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.MotherboardAddingForm;
import ru.itis.forms.PsuAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.Optional;
@Component
public class PsuAddingFormValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(PsuAddingForm.class.getName());
    }

    @Override
    public void validate(Object target, Errors errors) {

    }
}