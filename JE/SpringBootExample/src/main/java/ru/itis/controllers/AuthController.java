package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.AccountEditForm;
import ru.itis.forms.UserInfoEditForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.security.details.UserDetailsImpl;
import ru.itis.security.role.Role;
import ru.itis.security.states.State;
import ru.itis.services.AuthenticationService;
import ru.itis.services.UserService;
import ru.itis.validators.AccountEditFormValidator;
import ru.itis.validators.UserInfoEditFormValidator;
import ru.itis.validators.UserRegistrationFormValidator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
import java.util.Random;

@Controller
public class AuthController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoEditFormValidator userInfoEditFormValidator;

    @InitBinder("userForm")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(userInfoEditFormValidator);
    }

    @Autowired
    private AccountEditFormValidator accountEditFormValidator;

    @InitBinder("accountForm")
    public void initAccountFormValidator(WebDataBinder binder) {
        binder.addValidators(accountEditFormValidator);
    }


    @GetMapping("/login")
    public String login(@ModelAttribute("model") ModelMap model, Authentication authentication,
                        @RequestParam Optional<String> error) {
        if (authentication != null) {
            return "redirect:/";
        }
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }

    @GetMapping("/")
    public String root(Authentication authentication) {
        if (authentication != null) {
            User user = service.getUserByAuthentication(authentication);
            if (user.getRole().equals(Role.USER)) {
                return "redirect:/main";
            } else if (user.getRole().equals(Role.ADMIN)) {
                return "redirect:/admin";
            }
        }
        return "redirect:/login";
    }

    @GetMapping("/user/profile")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model) {
       // model.addAttribute(service.getUserByAuthentication(authentication));
        User user = service.getUserByAuthentication(authentication);
        model.addAttribute("user",user );
        if (user.getState() == State.NOT_CONFIRMED){
            model.addAttribute("not_conf", true);
        }else{
            model.addAttribute("not_conf", false);
        }
        return "profile";
    }



    @PostMapping("/user/profile")
    public String editProfileInfo(@Valid @ModelAttribute("userForm") UserInfoEditForm userInfoEditForm,
                               BindingResult errors, RedirectAttributes attributes,Authentication authentication) {

        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/user/profile";
        }
        userService.updateInfo(userInfoEditForm,service.getUserByAuthentication(authentication));
        return "redirect:/user/profile";


    }
    @PostMapping("/user/account")
    public String editAccountInfo(@Valid @ModelAttribute("accountForm") AccountEditForm accountEditForm,
                               BindingResult errors, RedirectAttributes attributes,Authentication authentication) {

        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/user/profile";
        }
        userService.updateAccountInfo(accountEditForm,service.getUserByAuthentication(authentication));
        return "redirect:/user/profile";
    }

    @PostMapping("/check")
    @ResponseBody
    public String checkLogin(@RequestParam("login") String login){
        if (userService.findByLogin(login).isPresent()){
            return "true";
        }else{
            return "false";
        }
    }
}
