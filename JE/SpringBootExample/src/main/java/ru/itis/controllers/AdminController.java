package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.*;
import ru.itis.models.FileInfo;
import ru.itis.repositories.MakerRepository;
import ru.itis.services.AdminService;
import ru.itis.services.FileStorageService;
import ru.itis.services.items.*;
import ru.itis.validators.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService service;

    @Autowired
    private MakerRepository makerRepository;

    @Autowired
    private MakersService makersService;

    @Autowired
    private CpusService cpusService;

    @Autowired
    private MotherboardsService motherboardsService;

    @Autowired
    private PsusService psusService;

    @Autowired
    private RamsService ramsService;

    @Autowired
    private RomsService romsService;

    @Autowired
    private VideoCardsService videoCardsService;

    @Autowired
    private FileStorageService fileStorageService;


    @Autowired
    private CpuAddingFormValidator cpuAddingFormValidator;

    @Autowired
    private MotherboardAddingFormValidator motherboardAddingFormValidator;

    @Autowired
    private PsuAddingFormValidator psuAddingFormValidator;
    @Autowired
    private RamAddingFormValidator ramAddingFormValidator;
    @Autowired
    private RomAddingFormValidator romAddingFormValidator;
    @Autowired
    private VideoCardAddingFormValidator videoCardAddingFormValidator;

    @InitBinder("cpuForm")
    public void initCpuFormValidator(WebDataBinder binder) {
        binder.addValidators(cpuAddingFormValidator);
    }

    @InitBinder("motherboardForm")
    public void initMotherboardFormValidator(WebDataBinder binder) {
        binder.addValidators(motherboardAddingFormValidator);
    }
    @InitBinder("psuForm")
    public void initPsuFormValidator(WebDataBinder binder) {
        binder.addValidators(psuAddingFormValidator);
    }
    @InitBinder("ramForm")
    public void initRamFormValidator(WebDataBinder binder) {
        binder.addValidators(ramAddingFormValidator);
    }
    @InitBinder("romForm")
    public void initRomFormValidator(WebDataBinder binder) {
        binder.addValidators(romAddingFormValidator);
    }
    @InitBinder("videoCardForm")
    public void initVideoCardFormValidator(WebDataBinder binder) {
        binder.addValidators(videoCardAddingFormValidator);
    }



    @GetMapping("/users")
    public String getUsersPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("users", service.getAllUsers());
        return "admin/users_edit";
    }
    @GetMapping("")
    public String getMainAdminPage() {
        return "admin/admin";
    }



    @GetMapping("items/cpus")
    public String getCpusEditPage(@ModelAttribute("model") ModelMap model){
        model.addAttribute("cpus",cpusService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/cpus_edit";
    }
    @PostMapping("items/cpus/delete/{cpu-id}")
    public String deleteCpu( @PathVariable("cpu-id") Long cpuId){
        cpusService.delete(cpuId);
        return "redirect:/admin/items/cpus";
    }
    @PostMapping("items/cpus/add")
    public String addCpu( @RequestBody @Valid @ModelAttribute("cpuForm")CpuAddingForm cpuAddingForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/cpus";
        }
        cpusService.add(cpuAddingForm);
        return "redirect:/admin/items/cpus";
    }




    @GetMapping("items/motherboards")
    public String getMotherboardsEditPage(@ModelAttribute("model") ModelMap model){
        model.addAttribute("motherboards",motherboardsService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/motherboards_edit";
    }
    @PostMapping("items/motherboard/delete/{motherboard-id}")
    public String deleteMotherboard( @PathVariable("motherboard-id") Long motherboardId){
        cpusService.delete(motherboardId);
        return "redirect:/admin/items/motherboards";
    }
    @PostMapping("items/motherboards/add")
    public String addMotherboard(@RequestBody @Valid @ModelAttribute("motherboardForm")MotherboardAddingForm motherboardAddingForm,
                                 BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/motherboards";
        }
        motherboardsService.add(motherboardAddingForm);
        return "redirect:/admin/items/motherboards";
    }


    @GetMapping("/items/psus")
    public String gePsusPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("psus", psusService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/psus_edit";
    }
    @PostMapping("items/psus/delete/{psu-id}")
    public String deletePsu( @PathVariable("psu-id") Long psuId){
        psusService.delete(psuId);
        return "redirect:/admin/items/psus";
    }
    @PostMapping("items/psus/add")
    public String addPsu(@RequestBody @Valid @ModelAttribute("psuForm")PsuAddingForm psuAddingForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/psus";
        }
        psusService.add(psuAddingForm);
        return "redirect:/admin/items/psus";
    }




    @GetMapping("/items/rams")
    public String geRamsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("rams", ramsService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/rams_edit";
    }
    @PostMapping("items/ram/delete/{ram-id}")
    public String deleteRam( @PathVariable("ram-id") Long ramId){
        ramsService.delete(ramId);
        return "redirect:/admin/items/rams";
    }
    @PostMapping("items/rams/add")
    public String addRam(@RequestBody @Valid @ModelAttribute("ramForm")RamAddingForm ramAddingForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/rams";
        }
        ramsService.add(ramAddingForm);
        return "redirect:/admin/items/rams";
    }


    @GetMapping("/items/roms")
    public String geRomsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("roms", romsService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/roms_edit";
    }
    @PostMapping("items/rom/delete/{rom-id}")
    public String deleteRom( @PathVariable("rom-id") Long romId){
        romsService.delete(romId);
        return "redirect:/admin/items/roms";
    }
    @PostMapping("items/roms/add")
    public String addRom(@RequestBody @Valid @ModelAttribute("romForm")RomAddingForm romAddingForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/roms";
        }
        romsService.add(romAddingForm);
        return "redirect:/admin/items/roms";
    }


    @GetMapping("/items/videoCards")
    public String geVideoCardsPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("videoCards", videoCardsService.getAll());
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/videoCards_edit";
    }
    @PostMapping("items/videoCard/delete/{videoCard-id}")
    public String deleteVideoCard( @PathVariable("videoCard-id") Long videoCardId){
        videoCardsService.delete(videoCardId);
        return "redirect:/admin/items/videoCards";
    }
    @PostMapping("items/videoCards/add")
    public String addVideoCard(@RequestBody @Valid @ModelAttribute("videoCardForm")VideoCardAddingForm videoCardAddingForm,
                               BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/items/videoCards";
        }
        videoCardsService.add(videoCardAddingForm);
        return "redirect:/admin/items/videoCards";
    }

    @GetMapping("makers")
    public String geMakersPage(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("makers",makerRepository.findAll());
        return "admin/makers_edit";
    }
    @PostMapping("makers/delete/{rom-id}")
    public String deleteMaker( @PathVariable("maker-id") Long makerId){
        makersService.delete(makerId);
        return "redirect:/admin/makers";
    }
    @PostMapping("makers/add")
    public String addMaker(@RequestBody @Valid @ModelAttribute("makerForm")MakerAddingForm makerAddingForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/admin/makers";
        }
        makersService.add(makerAddingForm);
        return "redirect:/admin/makers";
    }



    @GetMapping("/password/temp/{user-id}")
    public String getNewPasswordOfUserPage(@ModelAttribute("model") ModelMap model,
                                           @PathVariable("user-id") Long userId) {
        service.createTempPassword(userId);
        return "admin/temp_password_page";
    }
}
