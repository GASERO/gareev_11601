package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.services.AuthenticationService;
import ru.itis.services.RegistrationService;
import ru.itis.validators.UserRegistrationFormValidator;

import javax.validation.Valid;
@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService service;



    @Autowired
    private UserRegistrationFormValidator userRegistrationFormValidator;

    @InitBinder("userForm")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(userRegistrationFormValidator);
    }


    @PostMapping(value = "/signUp")
    public String signUp(@Valid @ModelAttribute("userForm") UserRegistrationForm userRegistrationForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/signUp";
        }

        service.register(userRegistrationForm);

        return "success_registration";
    }
    @GetMapping("/confirm/{uuid}")
    public String submitEmail(@PathVariable("uuid") String uuid){
        service.confirm(uuid);
        return "success_confirmation";
    }

    @GetMapping(value = "/signUp")
    public String getSignUpPage() {
     //   return "redirect:/main";
        return "sign_up";
    }
    @GetMapping("/main")
    public String getVideoCardsPage(@ModelAttribute("model") ModelMap model,Authentication authentication) {
        model.addAttribute("aut", authentication);

        return "main";
    }
}
