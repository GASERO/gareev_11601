package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.itis.services.items.*;


@Controller
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    private CpusService cpusService;

    @Autowired
    private MotherboardsService motherboardsService;

    @Autowired
    private PsusService psusService;

    @Autowired
    private RamsService ramsService;

    @Autowired
    private RomsService romsService;

    @Autowired
    private VideoCardsService videoCardsService;

    @GetMapping("/cpus")
    public String getCpusPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("cpus", cpusService.getAll());
        model.addAttribute("aut", authentication);
        return "items/cpus";
    }

    @GetMapping("/motherboards")
    public String getMotherboardsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("motherboards", motherboardsService.getAll());
        model.addAttribute("aut", authentication);
        return "items/motherboards";
    }

    @GetMapping("/psus")
    public String gePsusPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("psus", psusService.getAll());
        model.addAttribute("aut", authentication);
        return "items/psus";
    }

    @GetMapping("/rams")
    public String getRamsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("rams", ramsService.getAll());
        model.addAttribute("aut", authentication);
        return "items/rams";
    }

    @GetMapping("/roms")
    public String getRomsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("roms", romsService.getAll());
        model.addAttribute("aut", authentication);
        return "items/roms";
    }

    @GetMapping("/videoCards")
    public String getVideoCardsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("videocards", videoCardsService.getAll());
        model.addAttribute("aut", authentication);
        return "items/videoCards";
    }

    @GetMapping("/search")
    public String search(@ModelAttribute("model") ModelMap model, Authentication authentication,
                         @RequestParam("search") String q) {
        model.addAttribute("request",q);
        q = "%" + String.join("%", q.split("\\s+")) + "%";
        model.addAttribute("cpus", cpusService.searchInModelAndMaker(q));
        model.addAttribute("motherboards", motherboardsService.searchInModelAndMaker(q));
        model.addAttribute("psus", psusService.searchInModelAndMaker(q));
        model.addAttribute("rams", ramsService.searchInModelAndMaker(q));
        model.addAttribute("roms", romsService.searchInModelAndMaker(q));
        model.addAttribute("videocards", videoCardsService.searchInModelAndMaker(q));
        model.addAttribute("aut", authentication);

        return "search";
    }
   /* @GetMapping("")
    public String getItemsPage() {
        return "items";
    }*/

}
