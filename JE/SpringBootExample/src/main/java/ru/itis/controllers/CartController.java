package ru.itis.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.itis.models.CartNote;
import ru.itis.models.Cpu;
import ru.itis.models.Item;
import ru.itis.models.User;
import ru.itis.repositories.CartNoteRepository;
import ru.itis.services.AuthenticationService;
import ru.itis.services.CartNoteService;
import ru.itis.services.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;


@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @Autowired
    private CartNoteService cartNoteService;

    @Autowired
    private ObjectMapper objectMapper;


    @PostMapping("/add")
    @ResponseBody
    @SneakyThrows
    public boolean addItemToCart(@RequestParam("type") String type, @RequestParam("id") Long id, Authentication authentication, HttpServletRequest req, HttpServletResponse resp) {
        System.out.println(type);

        if (authentication == null) {
            Cookie[] cookies = req.getCookies();
            Cookie cartCookie = null;
            if (cookies != null) {
                for (Cookie cookie :
                        cookies) {
                    if ("cart".equals(cookie.getName())) {
                        cartCookie = cookie;
                        break;
                    }
                }
            }
            ArrayList<CartNote> cart = null;
            if (cartCookie == null) {
                cartCookie = new Cookie("cart",null);
                cart = new ArrayList<>();


            } else {
                cart = objectMapper.readValue(URLDecoder.decode(cartCookie.getValue(),"UTF-8"), ArrayList.class);
            }
            cart.add(CartNote.builder()
                    .itemType(type)
                    .modelId(id)
                    .build()
            );

            cartCookie.setValue(URLEncoder.encode(objectMapper.writeValueAsString(cart),"UTF-8"));
            cartCookie.setMaxAge(24*60*60);

            System.out.println(cartCookie);
            System.out.println(cartCookie.getName());
            System.out.println(objectMapper.writeValueAsString(cart));
            System.out.println(cartCookie.getValue().toString());
            resp.addCookie(cartCookie);
            return true;

        } else {
            cartNoteService.addCartNote(CartNote.builder()
                    .user(authenticationService.getUserByAuthentication(authentication))
                    .itemType(type)
                    .modelId(id)
                    .build());
        }
        return false;
    }

    @PostMapping("/remove")
    @ResponseBody
    @SneakyThrows
    public boolean removeItemFromCart(@ModelAttribute("model") ModelMap model,@RequestParam("type") String type, @RequestParam("id") Long id, Authentication authentication, HttpServletRequest req, HttpServletResponse resp) {
        System.out.println(type);
        if (authentication == null) {
            //УДАЛЯЕМ ИЗ КУКОВ
            Cookie[] cookies = req.getCookies();
            Cookie cartCookie = null;
            if (cookies != null) {
                for (Cookie cookie :
                        cookies) {
                    if ("cart".equals(cookie.getName())) {
                        cartCookie = cookie;
                        break;
                    }
                }
            }
            ArrayList cart = null;
            if (cartCookie == null) {
                cartCookie = new Cookie("cart",null);
                cart = new ArrayList<>();
            } else {
                cart = objectMapper.readValue(URLDecoder.decode(cartCookie.getValue(),"UTF-8"), ArrayList.class);
            }

            for (Object x :
                    cart) {
                System.out.println(((LinkedHashMap)x).get("itemType"));
                System.out.println(id);
                System.out.println(x.toString());
                System.out.println(((LinkedHashMap)x).get("modelId"));
                System.out.println(((LinkedHashMap)x).get("modelId").getClass());
                System.out.println(((LinkedHashMap)x).get("modelId").equals(id));
                System.out.println(((Integer)((LinkedHashMap)x).get("modelId")).longValue()==id);
                if (((Integer)((LinkedHashMap)x).get("modelId")).longValue()==id){
                    System.out.println(((String)((LinkedHashMap)x).get("itemType")).toLowerCase().substring(0,2));
                    System.out.println(type.substring(0,2).toLowerCase());
                    if (((String)((LinkedHashMap)x).get("itemType")).toLowerCase().substring(0,2).equals(type.substring(0,2).toLowerCase())) {
                        cart.remove(x);
                        System.out.println("NAWEL");
                        break;
                    }
                }
            }
            cartCookie = new Cookie("cart",URLEncoder.encode(objectMapper.writeValueAsString(cart),"UTF-8"));
            cartCookie.setPath("/cart");
            resp.addCookie(cartCookie);
            return true;


        }else {

            Set<CartNote> cartNotes = authenticationService.getUserByAuthentication(authentication).getCard();
            for (CartNote x :
                    cartNotes) {
                if (x.getModelId().longValue() == id && x.getItemType().toLowerCase().substring(0,2).equals(type.substring(0,2).toLowerCase())){
                    cartNoteService.delete(x);
                    System.out.println("DROPNUL");
                    System.out.println(x.toString() );
                    break;
                }
            }
            return true;
        }

    }


    @GetMapping("")
    @SneakyThrows
    public String getShoppingCart(Authentication authentication, @ModelAttribute("model") ModelMap model,HttpServletRequest req,HttpServletResponse resp) {
        User user = null;
        if (authentication != null) {
            user = authenticationService.getUserByAuthentication(authentication);
            model.addAttribute("aut", user);
        }

        List cart = null;
        Cookie[] cookies = req.getCookies();
        Cookie cartCookie = null;
        if (cookies != null) {
            for (Cookie x :
                    cookies) {
                if ("cart".equals(x.getName())) {
                    cartCookie = x;
                    break;
                }
            }
        }
        /*if (cartCookie != null) {
            ArrayList<CartNote> cartNoteArrayList = new ArrayList<>();
            for (Object x :
                    objectMapper.readValue(URLDecoder.decode(cartCookie.getValue(), "UTF-8"), ArrayList.class)
                    ) {
                cartNoteArrayList.add(CartNote.builder()
                        .modelId(Long.parseLong(((LinkedHashMap) x).get("modelId") + ""))
                        .itemType((String) ((LinkedHashMap) x).get("itemType"))
                        .build());
            }

        if (user != null) {
            // перенос куков в бд
            for (CartNote x :
                    cartNoteArrayList) {
                x.setUser(user);
                cartNoteService.addCartNote(x);
            }
            //удаление куков
            Cookie c = new Cookie("cart",null);
            c.setMaxAge(0);
            c.setPath("/cart");
            resp.addCookie(c);
            cart = userService.getCart(
                    authenticationService.getUserByAuthentication(authentication).getCard());
        } else {
            Set cartNoteSet = new HashSet();
            for (CartNote x :
                    cartNoteArrayList) {
                cartNoteSet.add(x);
            }
                cart = userService.getCart(cartNoteSet);
            }
        }*/
        ArrayList<CartNote> cartNoteArrayList = new ArrayList<>();
        if (cartCookie != null){
            for (Object x :
                    objectMapper.readValue(URLDecoder.decode(cartCookie.getValue(), "UTF-8"), ArrayList.class)
                    ) {
                cartNoteArrayList.add(CartNote.builder()
                        .modelId(Long.parseLong(((LinkedHashMap) x).get("modelId") + ""))
                        .itemType((String) ((LinkedHashMap) x).get("itemType"))
                        .build());
            }
        }
        if (user == null) {
            if (cartCookie == null){
                cart = null;
            }else {
                //cart из куков
                Set cartNoteSet = new HashSet();
                for (CartNote x :
                        cartNoteArrayList) {
                    cartNoteSet.add(x);
                }
                cart = userService.getCart(cartNoteSet);
            }
        }else{
            if (cartCookie == null){
                //из бд
                cart = userService.getCart(
                        authenticationService.getUserByAuthentication(authentication).getCard());
            }else{
                //перенос куков в бд
                for (CartNote x :
                        cartNoteArrayList) {
                    x.setUser(user);
                    cartNoteService.addCartNote(x);
                }
                //удаление куков
                Cookie c = new Cookie("cart",null);
                c.setMaxAge(0);
                c.setPath("/cart");
                resp.addCookie(c);
                cart = userService.getCart(
                        authenticationService.getUserByAuthentication(authentication).getCard());
            }
        }
        if (cart != null) {
            double total = 0;
            for (Object x :
                    cart) {
               total += ((Item)x).getPrice();

            }
            model.addAttribute("total", total);
        }
        model.addAttribute("items", cart);

        return "cart";
    }

}
