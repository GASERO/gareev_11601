package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RomAddingForm {
    private String model;
    private Integer energy;
    private Integer memory;
    private String type;
    private MultipartFile file;
    private Double price;
    private Long maker_id;
}
