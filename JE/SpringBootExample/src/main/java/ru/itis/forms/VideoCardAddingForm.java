package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VideoCardAddingForm {
    private String model;
    private Integer energy;
    private Double speed;
    private Integer memory;
    private String memory_type;
    private Long maker_id;
    private MultipartFile file;
    private Double price;
}
