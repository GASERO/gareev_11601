package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MotherboardAddingForm {
    private String model;
    private MultipartFile file;
    private Double price;
    private String socket;
    private String memory_type;
    private boolean igp;
    private String energy;
    private Long maker_id;
}
