package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PsuAddingForm {
    private String model;
    private MultipartFile file;
    private Double price;
    private Integer power;
    private Long maker_id;
}
