package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RamAddingForm {
    private String model;
    private Integer energy;
    private Integer memory;
    private MultipartFile file;
    private Double price;
    private String memory_type;
    private Long maker_id;
}
