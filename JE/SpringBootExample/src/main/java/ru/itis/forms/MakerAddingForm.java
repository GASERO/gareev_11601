package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.FileInfo;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MakerAddingForm {
    private String name;
    private String country;

}
