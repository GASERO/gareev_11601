package ru.itis.forms;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.FileInfo;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CpuAddingForm {
    private String model;
    private MultipartFile file;
    private Double price;
    private Double speed;
    private String socket;
    private Integer core_number;
    private Integer energy;
    private Long maker_id;
}
