package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;
import ru.itis.security.role.Role;
import ru.itis.security.states.State;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private EmailService emailService;


    private ExecutorService executorService = Executors.newCachedThreadPool();

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Transactional
    @Override
    public void register(UserRegistrationForm userForm) {
        User newUser = User.builder()
                .login(userForm.getLogin())
                .email(userForm.getEmail())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.USER)
                .state(State.NOT_CONFIRMED)
                .uuid(UUID.randomUUID().toString())
                .build();
        usersRepository.save(newUser);

        executorService.submit(() -> {
            emailService.sendMail("<h1> http://localhost:8080/confirm/" + newUser.getUuid() + "</h1>",
                    "Подтверждение почты " + newUser.getLogin(),
                    newUser.getEmail());
        });
    }

    @Override
    public void confirm(String uuid) {
       Optional<User> optional =  usersRepository.findByUuid(uuid);
       if (optional.isPresent()){
           User user = optional.get();
           user.setState(State.CONFIRMED);
           user.setUuid(null);
           usersRepository.save(user);
       }
    }
}
