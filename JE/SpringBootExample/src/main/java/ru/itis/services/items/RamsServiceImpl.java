package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.RamAddingForm;
import ru.itis.models.FileInfo;
import ru.itis.models.Ram;
import ru.itis.models.Maker;
import ru.itis.models.VideoCard;
import ru.itis.repositories.*;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class RamsServiceImpl implements RamsService {

    @Autowired
    private RamsRepository ramsRepository;
    @Autowired
    private FileStorageService fileStorageService;


    @Override
    public List<Ram> getAll() {
        return ramsRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        ramsRepository.delete(id);
    }


    @Override
    public void add(RamAddingForm ramForm) {
        Ram newRam = Ram.builder()
                .model(ramForm.getModel())
                .memory(ramForm.getMemory())
                .memoryType(ramForm.getMemory_type())
                .energy(ramForm.getEnergy())
                .maker(Maker.builder().id(ramForm.getMaker_id()).build())
                .price(ramForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(ramForm.getFile())))
                        .build())
                .build();
        ramsRepository.save(newRam);
    }

    @Override
    public List<Ram> searchInModelAndMaker(String q) {
        return ramsRepository.searchInModelAndMaker(q);
    }
    // @Transactional ???


}