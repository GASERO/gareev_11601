package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.FileInfo;
import ru.itis.models.Maker;
import ru.itis.models.Motherboard;
import ru.itis.repositories.MakerRepository;
import ru.itis.repositories.MotherboardsRepository;
import ru.itis.forms.MotherboardAddingForm;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class MotherboardsServiceImpl implements MotherboardsService {

    @Autowired
    private MotherboardsRepository motherboardsRepository;
    @Autowired
    private FileStorageService fileStorageService;



    @Override
    public List<Motherboard> getAll() {
        return motherboardsRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        motherboardsRepository.delete(id);
    }


    @Override
    public void add(MotherboardAddingForm motherboardForm) {
        Motherboard newMotherboard = Motherboard.builder()
                .model(motherboardForm.getModel())
                .socket(motherboardForm.getSocket())
                .memoryType(motherboardForm.getMemory_type())
                .igp(motherboardForm.isIgp())
                .energy(Integer.parseInt(motherboardForm.getEnergy()))
                .maker(Maker.builder().id(motherboardForm.getMaker_id()).build())
                .price(motherboardForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(motherboardForm.getFile())))
                        .build())
                .build();
        motherboardsRepository.save(newMotherboard);
    }

    @Override
    public List<Motherboard> searchInModelAndMaker(String q) {
        return motherboardsRepository.searchInModelAndMaker(q);
    }

    // @Transactional ???


}
