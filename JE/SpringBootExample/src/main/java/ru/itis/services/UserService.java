package ru.itis.services;

import ru.itis.forms.AccountEditForm;
import ru.itis.forms.UserInfoEditForm;
import ru.itis.models.CartNote;
import ru.itis.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {
    void updateInfo(UserInfoEditForm form, User user);
    Optional<User> findByLogin(String s);

    void updateAccountInfo(AccountEditForm accountEditForm, User userByAuthentication);

    List getCart(Set<CartNote> set);
}

