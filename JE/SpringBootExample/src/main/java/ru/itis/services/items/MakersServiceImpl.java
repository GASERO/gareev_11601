package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.MakerAddingForm;
import ru.itis.models.Maker;
import ru.itis.repositories.MakerRepository;

import java.util.List;
@Service
public class MakersServiceImpl implements MakersService {
    @Autowired
    private MakerRepository  makerRepository;
    @Override
    public List<Maker> getAll() {
        return makerRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        makerRepository.delete(id);
    }

    @Override
    public void add(MakerAddingForm makerAddingForm) {
        makerRepository.save(Maker.builder()
            .country(makerAddingForm.getCountry())
                .name(makerAddingForm.getName())
                .build()
        );
    }
}
