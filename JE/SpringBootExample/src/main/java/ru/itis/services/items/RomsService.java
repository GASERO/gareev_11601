package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.RomAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.Cpu;
import ru.itis.models.Rom;
import ru.itis.models.User;

import java.util.List;

public interface RomsService {
    List<Rom> getAll();
    void delete(Long id);
    void add(RomAddingForm romAddingForm);
    List<Rom> searchInModelAndMaker(String q);
}
