package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.Cpu;
import ru.itis.models.User;

import java.util.List;

public interface CpusService {
    List<Cpu> getAll();
    void delete(Long id);
    void add(CpuAddingForm cpuAddingForm);
    List<Cpu> searchInModelAndMaker(String q);
}
