package ru.itis.services.items;

import ru.itis.models.Motherboard;
import ru.itis.forms.MotherboardAddingForm;

import java.util.List;

public interface MotherboardsService {
    List<Motherboard> getAll();
    void delete(Long id);
    void add(MotherboardAddingForm motherboardAddingForm);
    List<Motherboard> searchInModelAndMaker(String q);
}
