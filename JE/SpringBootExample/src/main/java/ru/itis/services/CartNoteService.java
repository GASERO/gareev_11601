package ru.itis.services;

import ru.itis.models.CartNote;

public interface CartNoteService {
    void addCartNote(CartNote cartNote);
    void delete(CartNote cartNote);
}






