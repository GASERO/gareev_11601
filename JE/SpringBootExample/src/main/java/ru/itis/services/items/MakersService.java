package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.MakerAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.Cpu;
import ru.itis.models.Maker;
import ru.itis.models.User;

import java.util.List;

public interface MakersService {
    List<Maker> getAll();
    void delete(Long id);
    void add(MakerAddingForm makerAddingForm);
}
