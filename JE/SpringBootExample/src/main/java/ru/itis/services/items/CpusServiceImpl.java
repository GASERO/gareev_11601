package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.CpuAddingForm;
import ru.itis.models.Cpu;
import ru.itis.models.FileInfo;
import ru.itis.models.Maker;
import ru.itis.models.VideoCard;
import ru.itis.repositories.*;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class CpusServiceImpl implements CpusService {

    @Autowired
    private CpusRepository cpusRepository;
    @Autowired
    private FileStorageService fileStorageService;



    @Override
    public List<Cpu> getAll() {
        return cpusRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        cpusRepository.delete(id);
    }


    @Override
    public void add(CpuAddingForm cpuForm) {
        Cpu newCpu = Cpu.builder()
                .model(cpuForm.getModel())
                .socket(cpuForm.getSocket())
                .speed(cpuForm.getSpeed())
                .coreNumber(cpuForm.getCore_number())
                .energy(cpuForm.getEnergy())
                .maker(Maker.builder().id(cpuForm.getMaker_id()).build())
                .price(cpuForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(cpuForm.getFile())))
                                .build())
                .build();
        cpusRepository.save(newCpu);
    }

    @Override
    public List<Cpu> searchInModelAndMaker(String q) {
        return cpusRepository.searchInModelAndMaker(q);
    }

    // @Transactional ???


}