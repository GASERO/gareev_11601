package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.PsuAddingForm;
import ru.itis.models.FileInfo;
import ru.itis.models.Psu;
import ru.itis.models.Maker;
import ru.itis.repositories.PsusRepository;
import ru.itis.repositories.MakerRepository;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class PsusServiceImpl implements PsusService {

    @Autowired
    private PsusRepository psusRepository;
    @Autowired
    private FileStorageService fileStorageService;




    @Override
    public List<Psu> getAll() {
        return psusRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        psusRepository.delete(id);
    }
    @Override
    public void add(PsuAddingForm psuForm) {
        Psu newPsu = Psu.builder()
                .model(psuForm.getModel())
                .power(psuForm.getPower())
                .maker(Maker.builder().id(psuForm.getMaker_id()).build())
                .price(psuForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(psuForm.getFile())))
                        .build())
                .build();
        psusRepository.save(newPsu);
    }

    @Override
    public List<Psu> searchInModelAndMaker(String q) {
        return psusRepository.searchInModelAndMaker(q);
    }
}