package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.RomAddingForm;
import ru.itis.models.FileInfo;
import ru.itis.models.Rom;
import ru.itis.models.Maker;
import ru.itis.models.VideoCard;
import ru.itis.repositories.*;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class RomsServiceImpl implements RomsService {

    @Autowired
    private RomsRepository romsRepository;
    @Autowired
    private FileStorageService fileStorageService;



    @Override
    public List<Rom> getAll() {
        return romsRepository.findAll();
    }


    @Override
    public void delete(Long id) {
        romsRepository.delete(id);
    }
    @Override
    public void add(RomAddingForm romForm) {
        Rom newRom = Rom.builder()
                .model(romForm.getModel())
                .memory(romForm.getMemory())
                .type(romForm.getType())
                .energy(romForm.getEnergy())
                .maker(Maker.builder().id(romForm.getMaker_id()).build())
                .price(romForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(romForm.getFile())))
                        .build())
                .build();
        romsRepository.save(newRom);
    }

    @Override
    public List<Rom> searchInModelAndMaker(String q) {
        return romsRepository.searchInModelAndMaker(q);
    }

    // @Transactional ???


}