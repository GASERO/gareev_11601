package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.RamAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.Cpu;
import ru.itis.models.Ram;
import ru.itis.models.User;

import java.util.List;

public interface RamsService {
    List<Ram> getAll();
    void delete(Long id);
    void add(RamAddingForm ramAddingForm);
    List<Ram> searchInModelAndMaker(String q);
}
