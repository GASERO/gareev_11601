package ru.itis.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.forms.AccountEditForm;
import ru.itis.forms.UserInfoEditForm;
import ru.itis.models.CartNote;
import ru.itis.models.Motherboard;
import ru.itis.models.User;
import ru.itis.repositories.*;
import ru.itis.services.items.RomsService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private CpusRepository cpusRepository;

    @Autowired
    private MotherboardsRepository motherboardsRepository;

    @Autowired
    private PsusRepository psusRepository;

    @Autowired
    private RamsRepository ramsRepository;

    @Autowired
    private RomsRepository romsRepository;

    @Autowired
    private VideoCardsRepository videoCardsRepository;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Override
    public void updateInfo(UserInfoEditForm form, User user) {
                user.setName(form.getName());
                user.setName2(form.getName2());
                user.setEmail(form.getEmail());
                user.setPhone(form.getPhone());
        usersRepository.save(user);

    }

    @Override
    public Optional<User> findByLogin(String s) {
        return usersRepository.findOneByLogin(s);
    }

    @Override
    public void updateAccountInfo(AccountEditForm accountEditForm, User user) {
        user.setLogin(accountEditForm.getLogin());
        user.setHashPassword(passwordEncoder.encode( accountEditForm.getPassword()));
        usersRepository.save(user);
    }

    @Override
    public List getCart(Set<CartNote> set) {
        if (set == null || set.isEmpty()){
            return null;
        }else{
            ArrayList result = new ArrayList();
            for (CartNote x :
                    set) {
                switch (x.getItemType()){
                    case "cpu":
                        result.add(cpusRepository.getOne(x.getModelId()));
                        break;
                    case "motherboard":
                        result.add(motherboardsRepository.getOne(x.getModelId()));
                        break;
                    case "psu":
                        result.add(psusRepository.getOne(x.getModelId()));
                        break;
                    case "ram":
                        result.add(ramsRepository.getOne(x.getModelId()));
                        break;
                    case "rom":
                        result.add(romsRepository.getOne(x.getModelId()));
                        break;
                    case "videocard":
                        result.add(videoCardsRepository.getOne(x.getModelId()));
                        break;
                }
            }
            return result;
        }
    }


}
