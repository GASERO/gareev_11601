package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.CartNote;
import ru.itis.repositories.CartNoteRepository;
@Service
public class CartNoteServiceImpl implements CartNoteService {
    @Autowired
    private CartNoteRepository cartNoteRepository;
    @Override
    public void addCartNote(CartNote cartNote) {
        cartNoteRepository.save(cartNote);
    }

    @Override
    public void delete(CartNote cartNote) {
        cartNoteRepository.delete(cartNote);
    }
}
