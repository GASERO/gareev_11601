package ru.itis.services.items;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.VideoCardAddingForm;
import ru.itis.models.FileInfo;
import ru.itis.models.VideoCard;
import ru.itis.models.Maker;
import ru.itis.models.VideoCard;
import ru.itis.repositories.*;
import ru.itis.services.FileStorageService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class VideoCardsServiceImpl implements VideoCardsService {

    @Autowired
    private VideoCardsRepository videoCardsRepository;
    @Autowired
    private FileStorageService fileStorageService;



    @Override
    public List<VideoCard> getAll() {
        return videoCardsRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        videoCardsRepository.delete(id);
    }

    @Override
    public void add(VideoCardAddingForm videoCardForm) {
        VideoCard newVideoCard = VideoCard.builder()
                .model(videoCardForm.getModel())
                .speed(videoCardForm.getSpeed())
                .memory(videoCardForm.getMemory())
                .memoryType(videoCardForm.getMemory_type())
                .energy(videoCardForm.getEnergy())
                .maker(Maker.builder().id(videoCardForm.getMaker_id()).build())
                .price(videoCardForm.getPrice())
                .photo(FileInfo.builder()
                        .id(Long.parseLong(fileStorageService.saveFile(videoCardForm.getFile())))
                        .build())
                .build();
        videoCardsRepository.save(newVideoCard);
    }

    @Override
    public List<VideoCard> searchInModelAndMaker(String q) {
        return videoCardsRepository.searchInModelAndMaker(q);
    }
    // @Transactional ???


}