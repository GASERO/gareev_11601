package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.forms.VideoCardAddingForm;
import ru.itis.models.Cpu;
import ru.itis.models.User;
import ru.itis.models.VideoCard;

import java.util.List;

public interface VideoCardsService {
    List<VideoCard> getAll();
    void delete(Long id);
    void add(VideoCardAddingForm videoCardAddingForm);
    List<VideoCard> searchInModelAndMaker(String q);
}
