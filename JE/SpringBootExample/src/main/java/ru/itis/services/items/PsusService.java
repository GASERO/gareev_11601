package ru.itis.services.items;

import ru.itis.forms.CpuAddingForm;
import ru.itis.forms.PsuAddingForm;
import ru.itis.forms.UserRegistrationForm;
import ru.itis.models.Cpu;
import ru.itis.models.Psu;
import ru.itis.models.User;

import java.util.List;

public interface PsusService {
    List<Psu> getAll();
    void delete(Long id);
    void add(PsuAddingForm psuAddingForm);
    List<Psu> searchInModelAndMaker(String q);
}
