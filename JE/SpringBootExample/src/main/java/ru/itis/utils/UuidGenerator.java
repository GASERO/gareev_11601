package ru.itis.utils;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UuidGenerator {


    public static String generate(){
        return UUID.randomUUID().toString();
    }
}
