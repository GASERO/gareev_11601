<#ftl encoding='UTF-8'>
<head>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
              integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
                integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
                integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
                integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>
</head>
<body style="background-color: #f5f5f5">
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse sticky-top">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://localhost:8080/main">HW Shop</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <div class="dropdown" style="padding-top: 2px">
                <button class="btn btn-secondary dropdown-toggle " type="button"
                        style="background-color:#292b2c;color: rgba(255,255,255,0.5);border-color:#292b2c "
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Products
                </button>

                <div class="dropdown-menu" style="background-color:#292b2c" aria-labelledby="dropdownMenu2">
                    <ul>
                        <li><a class="nav-link" href="http://localhost:8080/items/cpus">CPUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/motherboards">Motherboards</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/psus">PSUs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/rams">RAMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/roms">ROMs</a></li>
                        <li><a class="nav-link" href="http://localhost:8080/items/videoCards">Video cards</a></li>
                    </ul>

                </div>
            </div>


            <li class="nav-item">
                <a class="nav-link" href="">Shipping</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">Payment</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="">About</a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0" action="/items/search" method="get"><input class="form-control mr-sm-2"
                                                                                          type="text" id="search"
                                                                                          name="search"
                                                                                          placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        <a href="/cart">
            <img style="border-radius: 5px; " src="http://localhost:8080/files/e5e98891-e2c7-44e7-ae7d-a33759ee84b4.png" height="38" width="38">
        </a>

        <div class="navbar-nav ">
            <div class="nav-item ">
                <a class="nav-link active" href="http://localhost:8080/user/profile">Profile <span
                        class="sr-only">(current)</span></a>
            </div>

            <div class="nav-item ">
                <a class="nav-link" href="http://localhost:8080/logout">Log out <span
                        class="sr-only">(current)</span></a>
            </div>

        </div>
    </div>

</nav>
<#if model.not_conf>
<div class="alert alert-warning alert-dismissable" id="alertOk"  >
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Ooops!</strong> Your email is not confirmed.Check it form confirmation message.
</div>
</#if>




<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
<#--  <li class="nav-item">
      <a class="nav-link  active" data-toggle="tab" href="#orders" role="tab">Shopping Cart</a>
  </li>-->
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#account" role="tab">Account</a>
    </li>
    <!-- <li class="nav-item">
         <a class="nav-link" data-toggle="tab" href="#service" role="tab">Мои услуги</a>
     </li>-->
</ul>

<!-- Tab panes -->
<div class="tab-content">




    <div class="tab-pane active" id="profile" role="tabpanel">
        <div class="container">

            <div class="offset-md-4 col-md-5" style="padding-top: 50px">
                <div class="form-login">
                    <form class="form-horizontal" action="/user/profile" method="post">

                        <h2 style="margin-left: 15px">Info Edit</h2>
                        <input required type="text" name="name" class="form-control input-sm chat-input"
                               value="<#if model.user.name??> ${model.user.name}</#if>"
                               placeholder="Name"/>
                        </br>
                        <input required type="text" name="name2" class="form-control input-sm chat-input"
                               value="<#if model.user.name2??> ${model.user.name2}</#if>"
                               placeholder="2nd Name"/>
                        </br>
                        <input required type="text" name="phone" class="form-control input-sm chat-input"
                               value="<#if model.user.phone??> ${model.user.phone}</#if>"
                               pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$" placeholder="Phone"/>
                        </br>
                        <input required type="text" pattern=".+@[\w\d]+\.[\w\d]+" name="email"
                               value="<#if model.user.email??> ${model.user.email}</#if>"
                               class="form-control input-sm chat-input" placeholder="Email"/>
                        </br>
                        <button class="btn btn-primary" style="float: right">Save</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


    <div class="tab-pane" id="account" role="tabpanel">
        <div class="container">

            <div class="offset-md-4 col-md-5" style="padding-top: 50px">
                <div class="form-login">
                    <form class="form-horizontal" action="/user/account" method="post">

                        <h2 style="margin-left: 15px">Account Edit</h2>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="errorLog" hidden>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p id="p1">
                            </p>
                        </div>
                        <input required type="text" oninput="checkLogin()" name="login" id="login"
                               class="form-control input-sm chat-input" value="${model.user.login}"
                               placeholder="Login"/>
                        </br>
                        <input required type="password" oninput="checkPassword()" name="password" id="password"
                               class="form-control input-sm chat-input"
                               placeholder="Password"/>
                        </br>
                        <input required type="password" oninput="checkPassword()" name="password2" id="password2"
                               class="form-control input-sm chat-input"
                               placeholder="Password repeat"/>
                        </br>
                        <button class="btn btn-primary" style="float: right" id="save">Save</button>


                    </form>
                </div>

            </div>
        </div>
    </div>

</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>


<script type="application/javascript">
    var correctLogin = true;
    var correctPassword = false;

    function isAllOkay() {
        if (correctPassword && correctLogin) {
            $("#errorLog").attr('hidden', true);
            $("#save").attr('disabled', false);
            console.log('oooops');
        } else {
            document.getElementById("p1").innerHTML = "";
            $("#errorLog").attr('hidden', false);
            $("#save").attr('disabled', true);
            if (!correctLogin) {
                document.getElementById("p1").innerHTML = "-Такой логин уже занят \n"
            }
            if (!correctPassword) {

                document.getElementById("p1").innerHTML = document.getElementById("p1").innerHTML + "-Введенные пароли не совпадают";
            }
        }
    }

    function checkLogin() {
        $.ajax({
            url: '/check',
            type: 'POST',
            data: {
                login: $('#login').val()
            },
            dataType: 'text',
            success: function (data) {

                if (data == "false") {
                    console.log("login is free");

                } else {
                    console.log("login is reserved")
                }
                /*  if (data == "false" ||  $('#login').val() == '${model.user.login}
                ') {
                                    console.log(1);
                                    $('#save').attr('disabled', false);
                                }else{
                                    $('#save').attr('disabled', true);
                                    console.log(2);
                                }*/
                if (data == "false" || $('#login').val() == '${model.user.login}') {
                    correctLogin = true;
                } else {
                    correctLogin = false;
                }
                isAllOkay();

            },
            error: function () {
                console.log("Ajax error")
            }
        });
    }

    function checkPassword() {
        var p1 = $('#password').val();
        var p2 = $('#password2').val();
        correctPassword = (p1 == p2);
        console.log(p1 == p2);
        isAllOkay();

    }
</script>
</body>
