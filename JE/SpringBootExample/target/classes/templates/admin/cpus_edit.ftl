<head>
    <#--<link href="/css/style.css" rel="stylesheet"/>-->
</head>
<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey;}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<table>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Socket</th>
        <th>Speed</th>
        <th>Core number</th>
        <th>Energy</th>
        <th>Maker</th>
        <th>Country</th>
    </tr>
<#list model.cpus as cpu>
    <tr>
        <td>${cpu.id}</td>
        <td>${cpu.model}</td>
        <td>${cpu.socket}</td>
        <td>${cpu.speed} ГГЦ</td>
        <td>${cpu.coreNumber}</td>
        <td>${cpu.energy} W</td>
        <td>${cpu.maker.name}</td>
        <td>${cpu.maker.country}</td>
      <#--  <td><button formmethod="post" formaction="/admin/items/cpus/delete/${cpu.id}">Delete</button></td>-->
        <td>
            <form action="/admin/items/cpus/delete/${cpu.id}" method="post">
                <input type="submit" value="Delete">
            </form>
           </td>

        <#--<td>
          <img src="http://localhost:8080/files/${cpu.photo.storageFileName}">
        </td>
-->

    </tr>
</#list>
</table>


<h3>Add new:</h3>

<div class="container">
    <div class="row">
        <div class="form-login">
            <form class="form-horizontal"<#-- action="/admin/items/cpus/add" method="post"-->>
                <input type="text" class="form-control" id="model" name="model" placeholder="model">
                </br>
                <input type="text" class="form-control" id="socket" name="socket" placeholder="socket">
                </br>
                <input type="text" class="form-control" id="speed" name="speed" placeholder="speed">
                </br>
                <input type="text" class="form-control" id="core_number" name="core_number" placeholder="core number">
                </br>
                <input type="text" class="form-control" id="energy" name="energy" placeholder="energy">
                </br>
                <input type="text" class="form-control" id="price" name="price" placeholder="price">
                </br>
                <select  id="maker_id" name="maker_id">
                    <option selected disabled>Maker</option>
                <#list model.makers as maker>
                    <option value="${maker.id}">${maker.name}</option>
                </#list>
                </select>
                </br>


                <input type="file" id="file" name="file" placeholder="Имя файла..."/>
                    <button type="button" onclick="sendFile(($('#file'))[0]['files'][0])"
                            class="btn btn-primary">Save
                    </button>
                    <input type="hidden" id="file_hidden">
                    <div class="filename"></div>

             <#--  <input type="submit" onclick="sendFile(($('#file'))[0]['files'][0])">-->

            </form>
        </div>
    </div>
</div>
</body>
<script src="/js/jquery.js"></script>
<script>
    var formData;
    function sendFile(file) {
        formData = new FormData();
        formData.append("file", file);
        formData.append("model", $('#model').val());
        formData.append("socket", $('#socket').val());
        formData.append("speed", $('#speed').val());
        formData.append("core_number", $('#core_number').val());
        formData.append("energy", $('#energy').val());
        formData.append("price", $('#price').val());
        formData.append("maker_id", $('#maker_id').val());

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/admin/items/cpus/add", true);
        xhr.send(formData,);
    }
</script>