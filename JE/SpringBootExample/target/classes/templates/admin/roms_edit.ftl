<head>
<#--   <link href="/css/style.css" rel="stylesheet"/>-->
</head>
<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey;}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<table>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Memory</th>
        <th>Type</th>
        <th>Energy</th>
        <th>Maker</th>

    </tr>
<#list model.roms as rom>
    <tr>
        <td>${rom.id}</td>
        <td>${rom.model}</td>
        <td>${rom.memory}</td>
        <td>${rom.type}</td>
        <td>${rom.energy}</td>
        <td>${rom.maker.name}</td>
        <td>${rom.maker.country}</td>
        <td>
            <form action="/admin/items/roms/delete/${rom.id}" method="post">
                <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
</#list>
</table>
<#--<form class="form-vertical" action="/admin/items/roms/add" method="post">
    <input name="model" placeholder="model">
    <input name="memory" placeholder="memory">
    <input name="type" placeholder="type">
    <input name="energy" placeholder="energy">
    <input name="maker_id" placeholder="maker_id">
    <input type="submit">
</form>-->
<div class="container">
    <div class="row">
        <div class="form-login">
            <form class="form-horizontal"<#-- action="/admin/items/cpus/add" method="post"-->>
                <input type="text" class="form-control" id="model" name="model" placeholder="model">
                </br>
                <input id="memory" name="memory" placeholder="memory">
                </br>
                <input id="type" name="type" placeholder="type">
                </br>
                <input type="text" class="form-control" id="energy" name="energy" placeholder="energy">
                </br>
                <input type="text" class="form-control" id="price" name="price" placeholder="price">
                </br>
                <select  id="maker_id" name="maker_id">
                    <option selected disabled>Maker</option>
                <#list model.makers as maker>
                    <option value="${maker.id}">${maker.name}</option>
                </#list>
                </select>
                </br>


                <input type="file" id="file" name="file" placeholder="Имя файла..."/>
                <button type="button" onclick="sendFile(($('#file'))[0]['files'][0])"
                        class="btn btn-primary">Save
                </button>
                <input type="hidden" id="file_hidden">
                <div class="filename"></div>

            <#--  <input type="submit" onclick="sendFile(($('#file'))[0]['files'][0])">-->

            </form>
        </div>
    </div>
</div>
</body>
<script src="/js/jquery.js"></script>
<script>
    var formData;
    function sendFile(file) {
        formData = new FormData();
        formData.append("file", file);
        formData.append("model", $('#model').val());
        formData.append("memory", $('#memory').val());
        formData.append("type", $('#type').val());
        formData.append("energy", $('#energy').val());
        formData.append("price", $('#price').val());
        formData.append("maker_id", $('#maker_id').val());

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/admin/items/roms/add", true);
        xhr.send(formData,);
    }
</script>