<head>
 <#--   <link href="/css/style.css" rel="stylesheet"/>-->
</head>
<body>
<style>
    /* внешние границы таблицы серого цвета толщиной 1px */
    table {border: 1px solid grey;}
    /* границы ячеек первого ряда таблицы */
    th {border: 1px solid grey;}
    /* границы ячеек тела таблицы */
    td {border: 1px solid grey;}
</style>
<table>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Power</th>

    </tr>
<#list model.psus as psu>
    <tr>
        <td>${psu.id}</td>
        <td>${psu.model}</td>
        <td>${psu.power}</td>
        <td>${psu.maker.name}</td>
        <td>${psu.maker.country}</td>
        <td>
            <form action="/admin/items/psus/delete/${psu.id}" method="post">
                <input type="submit" value="Delete">
            </form>
        </td>
    </tr>
</#list>
</table>
<#--
<form class="form-vertical" action="/admin/items/psus/add" method="post">
    <input name="model" placeholder="model">
    <input name="power" placeholder="power">
    <input name="maker_id" placeholder="maker_id">
    <input type="submit">
</form>
-->



<div class="container">
    <div class="row">
        <div class="form-login">
            <form class="form-horizontal"<#-- action="/admin/items/cpus/add" method="post"-->>
                <input type="text" class="form-control" id="model" name="model" placeholder="model">
                </br>
                <input type="text" class="form-control" id="power" name="power" placeholder="power">
                </br>
                <input type="text" class="form-control" id="price" name="price" placeholder="price">
                </br>
                <select  id="maker_id" name="maker_id">
                    <option selected disabled>Maker</option>
                <#list model.makers as maker>
                    <option value="${maker.id}">${maker.name}</option>
                </#list>
                </select>
                </br>


                <input type="file" id="file" name="file" placeholder="Имя файла..."/>
                <button type="button" onclick="sendFile(($('#file'))[0]['files'][0])"
                        class="btn btn-primary">Save
                </button>
                <input type="hidden" id="file_hidden">
                <div class="filename"></div>

            <#--  <input type="submit" onclick="sendFile(($('#file'))[0]['files'][0])">-->

            </form>
        </div>
    </div>
</div>
</body>
<script src="/js/jquery.js"></script>
<script>
    var formData;
    function sendFile(file) {
        formData = new FormData();
        formData.append("file", file);
        formData.append("model", $('#model').val());
        formData.append("power", $('#power').val());
        formData.append("price", $('#price').val());
        formData.append("maker_id", $('#maker_id').val());

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/admin/items/psus/add", true);
        xhr.send(formData,);
    }
</script>