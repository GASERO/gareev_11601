package ru.itis;

import java.util.List;

public interface HumanDAO extends CrudDAO<Human,Long> {
    List<Human> findAllByColorOrAge(String color, int age);
}
