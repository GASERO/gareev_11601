package ru.itis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Роберт on 10.09.2017.
 */
public class Program {
    public static void main(String[] args) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/JE",
                    "postgres",
                    "gasero");

            HumanDAO humansDao = new HumanDAOJDBCImpl(connection);
            Human human =  Human.builder()
                    .name("Гена")
                    .color("Зеленый")
                    .age(99)
                    .build();

          //  humansDao.save(human);
           // System.out.println(human.getId());

            Human rob = humansDao.find(2l);
            System.out.println(rob.getName());

            ArrayList<Human> list = new HumanDAOJDBCImpl(connection).findAll();
            for (Human x :
                    list) {
                System.out.println(x.getName());
            }

            humansDao.delete(8l);
            humansDao.update(2l,Human.builder()
                    .name("Tim")
                    .color("Зеленый")
                    .age(89)
                    .build());

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
/*
public class Main {

        private static final String DB_URL = "jdbc:postgresql://localhost:5432/JE",
                USER = "postgres", PASSWORD = "gasero";

        public static void main(String[] args) {

            int command;
            String name, age, sql;
            Scanner scanner = new Scanner(System.in);
            Connection connection = null;
            Statement statement = null;


            try {
//
                connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
                statement = connection.createStatement();
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }

            while (true) {
                System.out.println("Select command:\n1. ADD\n2. EXIT");

                command = scanner.nextInt();
                scanner.nextLine();

                if (command == 2) break;

                System.out.println("Enter the following data to add to database:");
                System.out.print("NAME: ");
                name = scanner.nextLine().toUpperCase();

                while (name.matches(".*\\d+.*")) {
                    System.out.print("Name cannot contain digits. Please enter correct info.\nNAME: ");
                    name = scanner.nextLine().toUpperCase();
                }


                System.out.print("AGE: ");
                age = scanner.nextLine();

                while (!age.matches("\\d+") | Integer.parseInt(age) < 0 | Integer.parseInt(age) > 100) {
                    System.out.print("Please enter correct age (0 to 100).\nAGE: ");
                    age = scanner.nextLine();
                }

                sql = "INSERT INTO student (name, age, color) VALUES('" + name + "', " +
                        Integer.parseInt(age) + ", 'white')";

                try {
                    statement.executeUpdate(sql);
                } catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }

                System.out.println("YOUR INFO ADDED TO DATABASE");
                System.out.println();
            }

            try {
                if (connection != null) {
                    connection.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }
        }

    }

*/
