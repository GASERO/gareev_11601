
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Владельцы машин</title>
</head>
<body>
<table>
    <tr>
        <th>Имя</th>
        <th>Возраст</th>
    </tr>
    <%--<%--%>
    <%--ArrayList<Human> owners = (ArrayList<Human>) request.getAttribute("owners");--%>
    <%--for (Human owner : owners) {--%>
    <%--%>--%>
    <%--<tr>--%>
    <%--<td><%=owner.getName()%></td>--%>
    <%--<td><%=owner.getAge()%></td>--%>
    <%--</tr>--%>
    <%--<%}%>--%>
    <c:forEach items="${owners}" var="owner">
        <tr>
            <td>${owner.name}</td>
            <td>${owner.age}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>