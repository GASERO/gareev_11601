package Dao;

import models.Motherboard;

public interface MotherboardDao extends CrudDao<Motherboard,Long> {
}
