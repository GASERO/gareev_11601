package Dao;

import java.util.List;

public interface CrudDao<M, I> {
    void save(M model);
    M find(I id);
    void delete(I id);
    void update(I id,M model);
    List<M> findAll();
}

