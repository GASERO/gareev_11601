package Dao;

import models.Maker;

public interface MakerDao extends CrudDao<Maker,Long> {
}
