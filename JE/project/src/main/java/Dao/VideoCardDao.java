package Dao;

import models.VideoCard;

public interface VideoCardDao extends CrudDao<VideoCard,Long> {
}
