package DaoImpl;

import Dao.VideoCardDao;
import com.google.common.collect.Lists;
import models.VideoCard;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VideoCardDaoTemplateImpl implements VideoCardDao {
    private final static String SQL_INSERT = "INSERT INTO video_card(model,memory,speed,memory_type,energy,maker_id) values (?,?,?,?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM video_card WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM video_card";
    private static final String SQL_DELETE = "DELETE FROM video_card WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE video_card SET model = ?, memory = ?, speed = ?, " +
            "memory_type = ?, energy = ? , maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, VideoCard> videoCards;

    public VideoCardDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.videoCards = new HashMap<Long, VideoCard>();
    }


    private RowMapper<VideoCard> videoCardRowMapper = (resultSet, rowNumber) -> {
        Long currentVideoCardId = resultSet.getLong(1);
        if (videoCards.get(currentVideoCardId) == null) {
            videoCards.put(currentVideoCardId, VideoCard.builder()
                    .id(currentVideoCardId)
                    .model(resultSet.getString(2))
                    .memory(resultSet.getInt(3))
                    .speed(resultSet.getDouble(4))
                    .memoryType(resultSet.getString(5))
                    .energy(resultSet.getInt(6))
                    .makerId(resultSet.getInt(7))
                    .build());
        }
        return videoCards.get(currentVideoCardId);
    };

    public void save(VideoCard model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setInt(2, model.getMemory());
            preparedStatement.setDouble(3, model.getSpeed());
            preparedStatement.setString(4, model.getMemoryType());
            preparedStatement.setInt(5, model.getEnergy());
            preparedStatement.setLong(6, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public VideoCard find(Long id) {
        VideoCard result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, videoCardRowMapper).get(0);
        videoCards.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, VideoCard model) {
        template.update(SQL_UPDATE, model.getModel(), model.getMemory(), model.getSpeed(), model.getMemoryType(), model.getEnergy(), model.getMakerId(), id);

    }

    @Override
    public List<VideoCard> findAll() {
        template.query(SQL_SELECT_ALL, videoCardRowMapper);
        List<VideoCard> result = Lists.newArrayList(videoCards.values());
        videoCards.clear();
        return result;
    }

}
