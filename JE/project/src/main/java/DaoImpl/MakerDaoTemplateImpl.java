package DaoImpl;

import Dao.MakerDao;
import com.google.common.collect.Lists;
import models.Maker;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakerDaoTemplateImpl implements MakerDao {
    private final static String SQL_INSERT = "INSERT INTO maker(name,country) values (?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM maker WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM maker";
    private static final String SQL_DELETE = "DELETE FROM maker WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE maker SET name = ?, country = ? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Maker> makers;

    public MakerDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.makers = new HashMap<Long, Maker>();
    }


    private RowMapper<Maker> makerRowMapper = (resultSet, rowNumber) -> {
        Long currentMakerId = resultSet.getLong(1);
        if (makers.get(currentMakerId) == null) {
            makers.put(currentMakerId, Maker.builder()
                    .id(currentMakerId)
                    .name(resultSet.getString(2))
                    .country(resultSet.getString(3))
                    .build());
        }
        return makers.get(currentMakerId);
    };

    public void save(Maker model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, model.getCountry());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Maker find(Long id) {
        Maker result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, makerRowMapper).get(0);
        makers.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Maker model) {
        template.update(SQL_UPDATE, model.getName(), model.getCountry(), id);

    }

    @Override
    public List<Maker> findAll() {
        template.query(SQL_SELECT_ALL, makerRowMapper);
        List<Maker> result = Lists.newArrayList(makers.values());
        makers.clear();
        return result;
    }

}
