package DaoImpl;

import Dao.RamDao;
import com.google.common.collect.Lists;
import models.Ram;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RamDaoTemplateImpl implements RamDao {
    private final static String SQL_INSERT = "INSERT INTO ram(model,memory,memory_type,energy,maker_id) values (?,?,?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM ram WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM ram";
    private static final String SQL_DELETE = "DELETE FROM ram WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE ram SET model = ?, memory = ?, memory_type = ?, energy = ? , maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Ram> rams;

    public RamDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.rams = new HashMap<Long, Ram>();
    }


    private RowMapper<Ram> ramRowMapper = (resultSet, rowNumber) -> {
        Long currentRamId = resultSet.getLong(1);
        if (rams.get(currentRamId) == null) {
            rams.put(currentRamId, Ram.builder()
                    .id(currentRamId)
                    .model(resultSet.getString(2))
                    .memory(resultSet.getInt(3))
                    .memoryType(resultSet.getString(4))
                    .energy(resultSet.getInt(5))
                    .makerId(resultSet.getInt(6))
                    .build());
        }
        return rams.get(currentRamId);
    };

    public void save(Ram model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setInt(2, model.getMemory());
            preparedStatement.setString(3, model.getMemoryType());
            preparedStatement.setInt(4, model.getEnergy());
            preparedStatement.setLong(5, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Ram find(Long id) {
        Ram result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, ramRowMapper).get(0);
        rams.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Ram model) {
        template.update(SQL_UPDATE, model.getModel(), model.getMemory(), model.getMemoryType(), model.getEnergy(), model.getMakerId(), id);

    }

    @Override
    public List<Ram> findAll() {
        template.query(SQL_SELECT_ALL, ramRowMapper);
        List<Ram> result = Lists.newArrayList(rams.values());
        rams.clear();
        return result;
    }

}
