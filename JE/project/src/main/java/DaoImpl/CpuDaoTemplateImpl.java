package DaoImpl;

import Dao.CpuDao;
import com.google.common.collect.Lists;
import models.Cpu;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CpuDaoTemplateImpl implements CpuDao {
    private final static String SQL_INSERT = "INSERT INTO cpu(model,socket,speed,core_number,energy,maker_id) values (?,?,?,?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM cpu WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM cpu";
    private static final String SQL_DELETE = "DELETE FROM cpu WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE cpu SET model = ?, socket = ?, speed = ?, " +
            "core_number = ?, energy = ? , maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Cpu> cpus;

    public CpuDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.cpus = new HashMap<Long, Cpu>();
    }


    private RowMapper<Cpu> cpuRowMapper = (resultSet, rowNumber) -> {
        Long currentCpuId = resultSet.getLong(1);
        if (cpus.get(currentCpuId) == null) {
            cpus.put(currentCpuId, Cpu.builder()
                    .id(currentCpuId)
                    .model(resultSet.getString(2))
                    .socket(resultSet.getString(3))
                    .speed(resultSet.getDouble(4))
                    .coreNumber(resultSet.getInt(5))
                    .energy(resultSet.getInt(6))
                    .makerId(resultSet.getInt(7))
                    .build());
        }
        return cpus.get(currentCpuId);
    };

    public void save(Cpu model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setString(2, model.getSocket());
            preparedStatement.setDouble(3, model.getSpeed());
            preparedStatement.setInt(4, model.getCoreNumber());
            preparedStatement.setInt(5, model.getEnergy());
            preparedStatement.setLong(6, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Cpu find(Long id) {
        Cpu result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, cpuRowMapper).get(0);
        cpus.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Cpu model) {
        template.update(SQL_UPDATE, model.getModel(), model.getSocket(), model.getSpeed(), model.getCoreNumber(), model.getEnergy(), model.getMakerId(), id);

    }

    @Override
    public List<Cpu> findAll() {
        template.query(SQL_SELECT_ALL, cpuRowMapper);
        List<Cpu> result = Lists.newArrayList(cpus.values());
        cpus.clear();
        return result;
    }

}
