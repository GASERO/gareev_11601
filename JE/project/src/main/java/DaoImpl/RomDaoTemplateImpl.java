package DaoImpl;

import Dao.RomDao;
import com.google.common.collect.Lists;
import models.Rom;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RomDaoTemplateImpl implements RomDao {
    private final static String SQL_INSERT = "INSERT INTO rom(model,memory,type,energy,maker_id) values (?,?,?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM rom WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM rom";
    private static final String SQL_DELETE = "DELETE FROM rom WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE rom SET model = ?, memory = ?, type = ?, energy = ? , maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Rom> roms;

    public RomDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.roms = new HashMap<Long, Rom>();
    }


    private RowMapper<Rom> romRowMapper = (resultSet, rowNumber) -> {
        Long currentRomId = resultSet.getLong(1);
        if (roms.get(currentRomId) == null) {
            roms.put(currentRomId, Rom.builder()
                    .id(currentRomId)
                    .model(resultSet.getString(2))
                    .memory(resultSet.getInt(3))
                    .type(resultSet.getString(4))
                    .energy(resultSet.getInt(5))
                    .build());
        }
        return roms.get(currentRomId);
    };

    public void save(Rom model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setInt(2, model.getMemory());
            preparedStatement.setString(3, model.getType());
            preparedStatement.setInt(4, model.getEnergy());
            preparedStatement.setLong(5, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Rom find(Long id) {
        Rom result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, romRowMapper).get(0);
        roms.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Rom model) {
        template.update(SQL_UPDATE, model.getModel(), model.getMemory(), model.getType(), model.getEnergy(), model.getMakerId(), id);

    }

    @Override
    public List<Rom> findAll() {
        template.query(SQL_SELECT_ALL, romRowMapper);
        List<Rom> result = Lists.newArrayList(roms.values());
        roms.clear();
        return result;
    }

}
