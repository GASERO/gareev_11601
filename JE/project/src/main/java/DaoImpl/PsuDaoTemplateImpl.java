package DaoImpl;

import Dao.PsuDao;
import com.google.common.collect.Lists;
import models.Psu;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PsuDaoTemplateImpl implements PsuDao {
    private final static String SQL_INSERT = "INSERT INTO psu(model,power,maker_id) values (?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM psu WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM psu";
    private static final String SQL_DELETE = "DELETE FROM psu WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE psu SET model = ?,power = ?, maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Psu> psus;

    public PsuDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.psus = new HashMap<>();
    }


    private RowMapper<Psu> psuRowMapper = (resultSet, rowNumber) -> {
        Long currentPsuId = resultSet.getLong(1);
        if (psus.get(currentPsuId) == null) {
            psus.put(currentPsuId, Psu.builder()
                    .id(currentPsuId)
                    .model(resultSet.getString(2))
                    .power(resultSet.getInt(3))
                    .makerId(resultSet.getInt(4))
                    .build());
        }
        return psus.get(currentPsuId);
    };

    public void save(Psu model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setInt(2, model.getPower());
            preparedStatement.setLong(3, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Psu find(Long id) {
        Psu result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, psuRowMapper).get(0);
        psus.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Psu model) {
        template.update(SQL_UPDATE, model.getModel(), model.getPower(), model.getMakerId(), id);

    }

    @Override
    public List<Psu> findAll() {
        template.query(SQL_SELECT_ALL, psuRowMapper);
        List<Psu> result = Lists.newArrayList(psus.values());
        psus.clear();
        return result;
    }

}
