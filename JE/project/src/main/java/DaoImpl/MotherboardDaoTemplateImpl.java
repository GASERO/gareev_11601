package DaoImpl;

import Dao.MotherboardDao;
import com.google.common.collect.Lists;
import models.Motherboard;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MotherboardDaoTemplateImpl implements MotherboardDao {
    private final static String SQL_INSERT = "INSERT INTO motherboard(model,socket,memory_type,igp,energy,maker_id) values (?,?,?,?,?,?)";
    private final static String SQL_SELECT_BY_ID = "SELECT * FROM motherboard WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT * FROM motherboard";
    private static final String SQL_DELETE = "DELETE FROM motherboard WHERE id = ?";
    private static final String SQL_UPDATE = "UPDATE motherboard SET model = ?, socket = ?, memory_type = ?, " +
            "igp = ?, energy = ? , maker_id =? WHERE id = ?";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterTemplate;
    private Map<Long, Motherboard> motherboards;

    public MotherboardDaoTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.motherboards = new HashMap<Long, Motherboard>();
    }


    private RowMapper<Motherboard> motherboardRowMapper = (resultSet, rowNumber) -> {
        Long currentMotherboardId = resultSet.getLong(1);
        if (motherboards.get(currentMotherboardId) == null) {
            motherboards.put(currentMotherboardId, Motherboard.builder()
                    .id(currentMotherboardId)
                    .model(resultSet.getString(2))
                    .socket(resultSet.getString(3))
                    .memoryType(resultSet.getString(4))
                    .igp(resultSet.getBoolean(5))
                    .energy(resultSet.getInt(6))
                    .makerId(resultSet.getInt(7))
                    .build());
        }
        return motherboards.get(currentMotherboardId);
    };

    public void save(Motherboard model) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            preparedStatement.setString(1, model.getModel());
            preparedStatement.setString(2, model.getSocket());
            preparedStatement.setString(3, model.getMemoryType());
            preparedStatement.setBoolean(4, model.isIgp());
            preparedStatement.setInt(5, model.getEnergy());
            preparedStatement.setLong(6, model.getMakerId());
            return preparedStatement;
        }, keyHolder);
        model.setId(keyHolder.getKey().longValue());

    }

    public Motherboard find(Long id) {
        Motherboard result = template.query(SQL_SELECT_BY_ID, new Long[]{id}, motherboardRowMapper).get(0);
        motherboards.clear();
        return result;
    }

    public void delete(Long id) {
        template.update(SQL_DELETE, id);

    }

    public void update(Long id, Motherboard model) {
        template.update(SQL_UPDATE, model.getModel(), model.getSocket(), model.getMemoryType(), model.isIgp(), model.getEnergy(), model.getMakerId(), id);

    }

    @Override
    public List<Motherboard> findAll() {
        template.query(SQL_SELECT_ALL, motherboardRowMapper);
        List<Motherboard> result = Lists.newArrayList(motherboards.values());
        motherboards.clear();
        return result;
    }

}
