package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Rom {
    private Long id;
    private String model;
    private Integer memory;
    private String type;
    private Integer energy;
    private long makerId;
}
