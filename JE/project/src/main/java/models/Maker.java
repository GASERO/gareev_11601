package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Maker {
    private String name;
    private String country;
    private Long id;
}
