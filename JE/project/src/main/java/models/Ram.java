package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Ram {
    private Long id;
    private String model;
    private Integer memory;
    private String memoryType;
    private Integer energy;
    private long makerId;
}
