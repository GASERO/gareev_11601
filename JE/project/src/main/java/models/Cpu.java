package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Cpu {
    private String model;
    private String socket;
    private Double speed;
    private Integer coreNumber;
    private Long id;
    private int energy;
    private long makerId;
}
