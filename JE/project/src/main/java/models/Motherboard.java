package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
public class Motherboard {
    private String model;
    private String socket;
    private Long id;
    private String memoryType;
    private boolean igp;
    private int energy;
    private long makerId;
}
