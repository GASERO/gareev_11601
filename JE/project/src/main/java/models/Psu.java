package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Psu {
    private Long id;
    private String model;
    private Integer power;
    private long makerId;
}

