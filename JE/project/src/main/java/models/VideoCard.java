package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class VideoCard {
    private Long id;
    private String model;
    private Integer memory;
    private Double speed;
    private String memoryType;
    private int energy;
    private long makerId;
}
