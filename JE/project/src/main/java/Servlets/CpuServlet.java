package Servlets;

import Dao.CpuDao;
import DaoImpl.CpuDaoTemplateImpl;
import models.Cpu;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CpuServlet extends HttpServlet {
    private CpuDao cpuDao;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Cpu> list = cpuDao.findAll();
        /*PrintWriter pw = resp.getWriter();
        pw.write("<table>");
        for (Cpu x :
                list) {
            pw.write("< tr >");
            pw.write("<td>"+ x.toString() + "</td>");
            pw.write("</tr");
        }
        pw.write("/table");
        pw.flush();
        pw.close();*/
        req.setAttribute("owners", list);
        req.getRequestDispatcher("/WEB-INF/jsp/cpus.jsp").forward(req, resp);
    }

    @Override
    public void init() throws ServletException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/hardware");
        dataSource.setUsername("postgres");
        dataSource.setPassword("gasero");
        cpuDao = new CpuDaoTemplateImpl(dataSource);
    }
}
