package ru.kpfu.itis.app.services;

import com.vk.api.sdk.actions.Groups;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import org.springframework.stereotype.Service;
/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 09.07.2018
 */
@Service
public class VkService {

    public static final Integer APP_ID = 6627444;
    public static final String CLIENT_SECRET = "SlyOCXOZK34iCzBNhMy6";
    public static final String REDIRECT_URI = "https://8107c935.ngrok.io/code";
    public static final String GROUP_ID = "safronofpub";

    private VkApiClient vk;

    public VkService() {
        TransportClient transportClient = HttpTransportClient.getInstance();
        vk = new VkApiClient(transportClient);
    }

    public boolean checkSub(UserAuthResponse authResponse) throws ClientException, ApiException {
        Groups groups =  vk.groups();
        return 1 == groups.isMember(new UserActor(authResponse.getUserId(),authResponse.getAccessToken()),GROUP_ID).execute().getValue();
    }
    public UserAuthResponse getOuthResponse(String code) throws ClientException, ApiException {
        return vk.oauth()
                .userAuthorizationCodeFlow(APP_ID, CLIENT_SECRET, REDIRECT_URI, code)
                .execute();
    }
}
