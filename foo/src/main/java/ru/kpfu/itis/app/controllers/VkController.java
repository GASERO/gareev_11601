package ru.kpfu.itis.app.controllers;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.UserAuthResponse;
import org.apache.catalina.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.app.services.VkService;

import javax.servlet.http.HttpSession;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 09.07.2018
 */
@Controller
public class VkController {

    private VkService vkService;

    public VkController(VkService vkService) {
        this.vkService = vkService;
    }

    @GetMapping("/auth")
    public String getMainPage(){
        return "vk_auth";
    }

    @GetMapping("/code")
    public String receiveCode(HttpSession session,@RequestParam("code") String code) {
        session.setAttribute("code",code);
        return "redirect:/main";
    }
    @GetMapping("/main")
    public String main(HttpSession session, @ModelAttribute("model") ModelMap model) throws ClientException, ApiException {
        model.addAttribute("uri",VkService.REDIRECT_URI);
        String code = (String) session.getAttribute("code");
        UserAuthResponse authResponse = (UserAuthResponse) session.getAttribute("auth");
        if (code == null || "".equals(code)) {
            model.addAttribute("flag",false);
            return "main";
        }
        if (authResponse == null) {
            authResponse = vkService.getOuthResponse(code);
            session.setAttribute("auth", authResponse);
        }
        model.addAttribute("code",code);
        model.addAttribute("flag", vkService.checkSub(authResponse));
        return "main";
    }
}
