<!DOCTYPE html>
<html lang="en">
<head>
    <title>:)</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
    <#if !model.code??>
        <a href="https://oauth.vk.com/authorize?client_id=6627444&display=popup&redirect_uri=${model.uri}&scope=groups&response_type=code&v=5.80">
            Войти в VK</a>
    <#else>
        <#if model.flag>
            <h1> Вы уже подписаны на группу <a href="https://vk.com/safronofpub">sufron pub</a></h1>
        <#else>
            <h1> Вы еще не подписаны на группу <a href="https://vk.com/safronofpub">sufron pub</a> :(</h1>
        </#if>
    </#if>
</div>
</body>
</html>

