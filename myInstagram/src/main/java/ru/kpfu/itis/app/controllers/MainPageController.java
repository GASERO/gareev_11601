package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.app.services.PublicationService;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 21.04.2018
 */
@Controller
@RequestMapping("/main")
public class MainPageController {

    @Autowired
    private PublicationService publicationService;

    @GetMapping("")
    public String getMainPage(Authentication authentication, @ModelAttribute("model") ModelMap model){
        model.addAttribute("publications", publicationService.getAll());
        return "main";
    }
}
