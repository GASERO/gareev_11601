package ru.kpfu.itis.app.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.kpfu.itis.app.forms.PublicationAddingForm;
import ru.kpfu.itis.app.repositories.PublicationRepository;
import ru.kpfu.itis.app.repositories.UserDataRepository;

import java.util.Optional;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
@Component
public class PublicationAddingFormValidator implements Validator {

    @Autowired
    private PublicationRepository publicationRepository;



    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(PublicationAddingForm.class.getName());
    }

    @Transactional
    @Override
    public void validate(Object target, Errors errors) {

    }
}