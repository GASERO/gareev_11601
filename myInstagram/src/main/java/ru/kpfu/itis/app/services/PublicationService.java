package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.model.Publication;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.forms.PublicationAddingForm;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
public interface PublicationService {
    List<Publication> getAll();
    void add(PublicationAddingForm publicationAddingForm);
    Publication getById(Long id);
    void delete(Long id);
}
