package ru.kpfu.itis.app.services;

import ru.kpfu.itis.app.model.Comment;
import ru.kpfu.itis.app.model.UserData;
import ru.kpfu.itis.app.forms.CommentAddingForm;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
public interface CommentService {
    List<Comment> getAll();
    void add(CommentAddingForm commentAddingForm);
    Comment getById(Long id);
    void delete(Long id);
}
