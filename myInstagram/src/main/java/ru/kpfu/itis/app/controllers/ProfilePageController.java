package ru.kpfu.itis.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.app.services.UserDatasService;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
@Controller
@RequestMapping("profile")
public class ProfilePageController {
    @Autowired
    private UserDatasService userDatasService;

    @GetMapping("/{user-id}")
    public String getProfile(Authentication authentication, @ModelAttribute("model") ModelMap model, @PathVariable("user-id")Long userId){
        model.addAttribute("userData", userDatasService.getById(userId));
        return "profile";
    }
}
