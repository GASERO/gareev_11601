package ru.kpfu.itis.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.PublicationAddingForm;
import ru.kpfu.itis.app.model.Publication;
import ru.kpfu.itis.app.repositories.PublicationRepository;
import ru.kpfu.itis.app.services.PublicationService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
@Service
public class PublicationServiceImpl implements PublicationService {
    @Autowired
    private PublicationRepository publicationRepository;


    @Override
    public List<Publication> getAll() {
        return publicationRepository.findAll();
    }

    @Override
    public void add(PublicationAddingForm publicationAddingForm) {
    }


    @Override
    public Publication getById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
