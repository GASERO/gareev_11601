package ru.kpfu.itis.app.services.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.app.forms.CommentAddingForm;
import ru.kpfu.itis.app.model.Comment;
import ru.kpfu.itis.app.repositories.CommentRepository;
import ru.kpfu.itis.app.services.CommentService;

import java.util.List;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
@Service
public class CommentServiceImpl implements CommentService {
    private CommentRepository roleInProjectRepository;


    @Override
    public List<Comment> getAll() {
        return roleInProjectRepository.findAll();
    }

    @Override
    public void add(CommentAddingForm roleInProjectAddingForm) {
        roleInProjectRepository.save(Comment.builder()
                .build());
    }

    @Override
    public Comment getById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
