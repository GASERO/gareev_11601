package ru.kpfu.itis.app.forms;

import lombok.*;

import java.sql.Date;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 14.05.2018
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CommentAddingForm {
    private Long id;
    private String name;
    private Date dateOfCreation;
}
