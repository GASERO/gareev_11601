${model.userData.user.name}
<#list model.userData.user.publications as publ>
    <div class="sl-item">
        <div class="sl-left"> <img src="/storage/${publ.photo.storageFileName}" alt="user" class="img-circle" style="width: 30px" /> </div>
        <div class="sl-right">
            <div><a href="#" class="link">Michael Qin</a> <span class="sl-date">5 minutes ago</span>
                <p> ${publ.date}</p>
                <div class="row">
                    <img src="/storage/${publ.photo.storageFileName}" alt="user" class="img-circle" style="width: 400px"/>
                </div>
                <div class="like-comm"> <a href="javascript:void(0)" class="link m-r-10">${publ.comments?size} comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
            </div>
        </div>
    </div>

</#list>