﻿create table genre (
	id serial primary key,
	name varchar(50)
)
create table studio (
	id serial primary key,
	name varchar(50)
)
create table actor (
	id serial primary key,
	name varchar(50)
)
create table moovie (
	id serial primary key,
	name varchar(50),
	year integer ,
	genre_id integer references genre(id),
	studio_id integer references studio(id),
	cost integer		
)

create table actor_moovie (
	actor_id integer not null references actor(id) ,
	moovie_id integer not null references moovie(id),
	actor_money integer
)
create table cinema(
	id serial primary key,
	name varchar(50)
)	 
create table timetable(
	moovie_id integer references moovie(id),
	cinema_id integer references cinema(id),
	"date" date not null,
	price integer not null
	 
)
