
CREATE VIEW numberOfConfirmedUsers 
AS select count(state) from users where state = 'CONFIRMED';

CREATE VIEW theBestCpus 
as select model 
from cpu where 
core_number = (select max(core_number) from cpu)
 and speed = (select max(speed) from cpu where core_number =(select max(core_number) from cpu) );

CREATE VIEW countriesAndNumberMakers as select country, count(name) from maker group by country;
