
CREATE OR REPLACE FUNCTION fill()
    RETURNS TABLE(a text,b text) AS
$$
BEGIN

    DROP TABLE IF EXISTS tab;
    CREATE TABLE tab AS
        SELECT cpu.model as cpu,motherboard.model as motherboard FROM cpu JOIN motherboard ON cpu.socket = motherboard.socket;

END;
$$ LANGUAGE plpgsql;

select fill();