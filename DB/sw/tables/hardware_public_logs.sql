CREATE TABLE public.logs
(
    id integer DEFAULT nextval('logs_id_seq'::regclass) PRIMARY KEY NOT NULL,
    login text NOT NULL,
    date date NOT NULL,
    action text NOT NULL
);
CREATE UNIQUE INDEX logs_id_uindex ON public.logs (id);
INSERT INTO public.logs (id, login, date, action) VALUES (1, 'gasero', '2017-11-29', 'entering');