CREATE TABLE public.video_card
(
    id integer DEFAULT nextval('video_card_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(15),
    memory integer,
    speed integer,
    memory_type varchar(10),
    energy integer,
    maker_id integer,
    CONSTRAINT fkipho04prjrdkqnndxn84cd8k0 FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.video_card (id, model, memory, speed, memory_type, energy, maker_id) VALUES (1, '560ti', 2048, 2, 'ddr3', 250, 1);
INSERT INTO public.video_card (id, model, memory, speed, memory_type, energy, maker_id) VALUES (2, '560', 2048, 1, 'ddr3', 225, 1);
INSERT INTO public.video_card (id, model, memory, speed, memory_type, energy, maker_id) VALUES (3, 'r430', 2048, 2, 'ddr3', 230, 2);
INSERT INTO public.video_card (id, model, memory, speed, memory_type, energy, maker_id) VALUES (4, 'r430m', 1024, 1, 'ddr3', 200, 2);