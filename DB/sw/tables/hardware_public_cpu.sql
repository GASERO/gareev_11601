CREATE TABLE public.cpu
(
    id integer DEFAULT nextval('cpu_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(15),
    socket varchar(20),
    speed real,
    core_number integer,
    energy integer,
    maker_id integer,
    CONSTRAINT fkjllrvde80wfd5uva1kkbbffwi FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (8, 'A10-6790K', 'FM2', 4, 4, 250, 2);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (9, 'A8-7600', 'FM2+', 3.1, 4, 210, 2);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (10, 'A4-6300', 'FM2', 3.7, 2, 205, 2);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (17, 'azaza', 'gfdgdf', 1, 2, 124, 1);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (3, 'Core i5-6400', 'LGA1151', 2.7, 4, 150, 1);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (6, 'Core i7-7700', 'LGA1366', 2.667, 4, 170, 1);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (7, 'Pentium G3260', 'LGA1150', 3.3, 2, 200, 1);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (4, 'Core i5-7600', 'LGA1151', 3.5, 4, 190, 1);
INSERT INTO public.cpu (id, model, socket, speed, core_number, energy, maker_id) VALUES (5, 'Core i7-7700', 'LGA1151', 3.6, 4, 210, 1);