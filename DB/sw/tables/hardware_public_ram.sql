CREATE TABLE public.ram
(
    id integer DEFAULT nextval('ram_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(35),
    memory integer,
    memory_type varchar(10),
    energy integer,
    maker_id integer,
    CONSTRAINT fkq4m2mlkkbdlecfw1awck91i0n FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.ram (id, model, memory, memory_type, energy, maker_id) VALUES (2, 'ram2', 1024, 'ddr3', 225, 1);
INSERT INTO public.ram (id, model, memory, memory_type, energy, maker_id) VALUES (4, 'ram4', 2048, 'ddr3', 225, 2);
INSERT INTO public.ram (id, model, memory, memory_type, energy, maker_id) VALUES (3, 'ram3', 2048, 'ddr3', 275, 2);
INSERT INTO public.ram (id, model, memory, memory_type, energy, maker_id) VALUES (1, 'ram1', 2048, 'ddr3', 250, 1);