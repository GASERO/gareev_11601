CREATE TABLE public.rom
(
    id integer DEFAULT nextval('rom_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(15),
    memory integer,
    type varchar(10),
    energy integer,
    maker_id integer,
    CONSTRAINT fkp55k02jvhs84ibeex44irai87 FOREIGN KEY (maker_id) REFERENCES maker (id),
    CONSTRAINT rom_maker_id_fkey FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.rom (id, model, memory, type, energy, maker_id) VALUES (1, 'model1', 100, 'type1', 25, 1);
INSERT INTO public.rom (id, model, memory, type, energy, maker_id) VALUES (2, 'model2', 250, 'type1', 25, 1);
INSERT INTO public.rom (id, model, memory, type, energy, maker_id) VALUES (3, 'model3', 500, 'type1', 30, 2);
INSERT INTO public.rom (id, model, memory, type, energy, maker_id) VALUES (4, 'model4', 1000, 'type2', 40, 6);
INSERT INTO public.rom (id, model, memory, type, energy, maker_id) VALUES (5, 'model5', 1000, 'typ2', 40, 7);