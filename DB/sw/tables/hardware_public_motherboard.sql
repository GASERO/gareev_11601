CREATE TABLE public.motherboard
(
    id integer DEFAULT nextval('motherboard_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(45),
    socket varchar(10),
    memory_type varchar(10),
    igp boolean,
    energy integer,
    maker_id integer,
    CONSTRAINT fk7yhucy6locorj9tq43r2vra4k FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (1, 'Micro ATX H110M-S2H GSM', 'LGA 1151', 'ddr4', true, 250, 7);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (2, 'ATX GA-Z170XP-SLI', 'LGA 1151', 'ddr4', true, 275, 7);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (3, 'ROG MAXIMUS VIII HERO Z170 ATX', 'LGA 1151', 'ddr4', true, 290, 6);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (4, 'Sabertooth X58', 'LGA 1366', 'ddr3', false, 245, 6);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (5, 'B85M-E', 'LGA 1150', 'ddr3', false, 250, 6);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (6, 'FM2-A85XA-G65', 'FM2', 'ddr3', false, 215, 5);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (7, 'FM2-A75IA-E53', 'FM2', 'ddr3', true, 220, 5);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (8, 'FM2-A75MA-P31', 'FM2', 'ddr3', true, 210, 5);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (9, 'Crosshair V Formula-Z', 'FM2+', 'ddr3', false, 270, 6);
INSERT INTO public.motherboard (id, model, socket, memory_type, igp, energy, maker_id) VALUES (10, 'M5A99FX Pro R2.0', 'FM2+', 'ddr3', true, 250, 6);