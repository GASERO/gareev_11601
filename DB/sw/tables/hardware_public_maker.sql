CREATE TABLE public.maker
(
    id integer DEFAULT nextval('maker_id_seq'::regclass) PRIMARY KEY NOT NULL,
    name varchar(45),
    country varchar(20)
);
INSERT INTO public.maker (id, name, country) VALUES (1, 'Intel', 'USA');
INSERT INTO public.maker (id, name, country) VALUES (2, 'AMD', 'USA');
INSERT INTO public.maker (id, name, country) VALUES (5, 'MSI', 'Taiwan');
INSERT INTO public.maker (id, name, country) VALUES (6, 'ASUS', 'Taiwan');
INSERT INTO public.maker (id, name, country) VALUES (7, 'Gigabyte Technology', 'Taiwan');