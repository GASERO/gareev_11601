CREATE TABLE public.psu
(
    id integer DEFAULT nextval('psu_id_seq'::regclass) PRIMARY KEY NOT NULL,
    model varchar(15),
    power integer,
    maker_id integer,
    CONSTRAINT fkr3qa19jy5vaur3nxr2cj7vwo4 FOREIGN KEY (maker_id) REFERENCES maker (id)
);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (4, '1', 2, 7);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (5, '2', 250, 1);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (6, 'model3', 500, 1);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (7, 'model4', 500, 2);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (8, 'model5', 750, 7);
INSERT INTO public.psu (id, model, power, maker_id) VALUES (9, 'model6 ', 450, 6);