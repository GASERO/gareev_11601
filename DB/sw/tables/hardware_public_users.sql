CREATE TABLE public.users
(
    role text NOT NULL,
    email text,
    id bigint DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    login text NOT NULL,
    hash_password varchar(255),
    hash_temp_password varchar(255),
    name varchar(255),
    state varchar(255),
    uuid varchar(255)
);
CREATE UNIQUE INDEX user_id_uindex ON public.users (id);
CREATE UNIQUE INDEX user_login_uindex ON public.users (login);
CREATE UNIQUE INDEX uk_6km2m9i3vjuy36rnvkgj1l61s ON public.users (uuid);
INSERT INTO public.users (role, email, id, login, hash_password, hash_temp_password, name, state, uuid) VALUES ('ADMIN', 'r.s.gareev@gmail.com', 1, 'admin', '$2a$10$3NH7OGRDOyg767yZoMcAZO4tAbueH7kSIt2WmgPX0ZEUbEFahVftG', null, 'Администратор', null, null);
INSERT INTO public.users (role, email, id, login, hash_password, hash_temp_password, name, state, uuid) VALUES ('USER', null, 2, '12', '$2a$10$fl7Q9uNId8.GcoVWxOaYwO337fdl05nRKmlhzidmO2zTr8tlLv4R6', null, null, null, null);
INSERT INTO public.users (role, email, id, login, hash_password, hash_temp_password, name, state, uuid) VALUES ('USER', 'r.s.gareev@gmail.com', 5, 'g', '$2a$10$E0r1eGxd/t4NGphjOh00pu6yVfXDXjHc3FRc0CyCk9T2vWubBCHr6', null, null, 'CONFIRMED', null);
INSERT INTO public.users (role, email, id, login, hash_password, hash_temp_password, name, state, uuid) VALUES ('USER', 'r.s.gareev@gmail.com', 4, 'gasero', '$2a$10$VIH.yBC1i7b37AAB0DeNkukd9T9jIxxNb5u4q6cDtlCC0RyIdR3fa', '$2a$10$GGJFVryJKAlOP7N8oiFgG.bEa9uuaNnjqWwMAw5qUIIUCODcBYHYC', null, 'CONFIRMED', '272154c2-fede-4432-9b5e-11ae10824f9c');