

CREATE OR REPLACE FUNCTION add_to_log() RETURNS TRIGGER AS $$
DECLARE
BEGIN
    IF OLD.hash_temp_password = NULL THEN
    INSERT INTO logs(login,date,action) VALUES (OLD.login,now(),'req password');
    RETURN NEW;
    ELSE
        INSERT INTO logs(login,date,action) VALUES (OLD.login,now(),'entering');
        RETURN NEW;
    END IF;


END;
$$ LANGUAGE plpgsql;



CREATE  TRIGGER t_user
AFTER UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE add_to_log ();





