import javax.servlet.ServletException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GorodaServlet extends javax.servlet.http.HttpServlet {

    private Map<String,List<String>> gameSessions;
    private List<String> allWords;

    @Override
    public void init() throws ServletException {
        gameSessions = new HashMap<>();
        allWords = new ArrayList<>();
        try {
            Files.lines(Paths.get("C:\\GR\\exam\\practice15\\server\\src\\words.txt")).forEach(x->{
                allWords.add(x);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)  {
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws IOException {
        String uuid = request.getParameter("uuid");
        List<String> usedWords;
        if (gameSessions.containsKey(uuid)){
            usedWords = gameSessions.get(uuid);
        }else{
            usedWords = new ArrayList<>();
            gameSessions.put(uuid,usedWords);
        }
        String  gorod = request.getParameter("gorod");
        System.out.println(gorod);
        System.out.println(uuid);
        usedWords.add(gorod);
        for (String s :
                allWords) {
            if (s.charAt(0) == gorod.charAt(gorod.length()-1) && !usedWords.contains(s)){
                usedWords.add(s);
                response.getWriter().print(s);
            }
        }
        response.getWriter().close();
    }
}
