import java.util.UUID;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 16.06.2018
 */
public class UuidSingletone {
    private static String uuid;

    public static String getUuid(){
        if (uuid == null){
            uuid = UUID.randomUUID().toString();
        }
        return uuid;
    }
}
