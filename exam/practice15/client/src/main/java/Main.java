import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Robert Gareev
 * 11-601 ITIS KPFU
 * 16.06.2018
 */
public class Main {
    private static List<String> goroda;
    public static void main(String[] args) throws IOException {
        List<String> used = new ArrayList<>();
        initGoroda();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input = sc.nextLine().toLowerCase();
            if (goroda.contains(input)){
                if(!used.contains(input)){
                    sendWord(input);
                    used.add(input);
                }else{
                    System.out.println("Already used");
                }
            }else{
                System.out.println("Takogo goroda net");
            }
        }

    }
    private static void sendWord(String word) throws IOException {
        String url = "https://localhost:8080/goroda?gorod="+word+"&uuid" + UuidSingletone.getUuid();
        System.out.println(url);
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println("server said:" + response.toString());
    }
    private static void initGoroda() throws IOException {
        goroda = new ArrayList<>();
        Files.lines(Paths.get("words.txt")).forEach(x->{
            goroda.add(x);
        });
    }
}
